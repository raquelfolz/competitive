#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>

using namespace std;
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

vector<ll> primos = {2, 3, 5, 7};

ll n;
int cnt;
vector<int> divp;

int main () {
  ios::sync_with_stdio(0);
  cin.tie(0);

  for (int i = 9; i < 1000100; i += 2) {
    for (auto p: primos) {
      if (i % p == 0) break;
      if (p*p > i) {
        primos.pb(i);
        break;
      }
    }
  }
  
  cin >> n;
  for (auto p: primos) {
    if (n % p == 0) {
      cnt = 0;
      while (n%p == 0) {n/=p; cnt++;}
      divp.pb(cnt);
    }
  }
  if (n != 1) divp.pb(1);
  
  if (divp.size() == 1 and divp[0] % 2 == 1) { cout << "Y\n"; return 0; }
  if (divp.size() == 2 and divp[0] == 1 and divp[1] == 1) { cout << "Y\n"; return 0; }
  cout << "N\n"; 

  return 0;
}
