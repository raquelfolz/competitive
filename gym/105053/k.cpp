#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>

using namespace std;
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

string vogais="AEIOUY";

int n;
string s[1000100];
int val[1000100], qtd[1000100]; //tam min do acronimo terminando em uma vogal naquela palavra
                                //qtd de consoantes que eu precisei pegar daquela palavra

int ans;
bool b;

bool isvowel(char c) {
  return (vogais.find(c) != string::npos);
}

int main () {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 1; i <= n; i++) cin >> s[i];
  s[n+1] = "A";

  for (int i = 1; i <= n+1; i++) {
    int vogal = 0;
    for (auto c: s[i]) {
      if (isvowel(c)) break;
      vogal++;
    }
    if (vogal == s[i].length()) vogal = 10;
    qtd[i] = vogal;
  }
  
  for (int i = 1; i <= n+1; i++) {
    int melhor = INF;
    //se tiver 2 consoates, preciso pegar i-1
    //se tiver 1, posso pegar i-2
    //com 0, posso pegar i-3
    int ant = i-3 + qtd[i];
    for (int j = ant; j < i; j++) {
      //somo uma letra pra cada palavra depois da j...
      //e mais a quantidade de consoates da i
      melhor = min(melhor, val[j] + i-j + qtd[i]);
    }
    val[i] = min(melhor, INF);
  }

  /*
  for (int i = n-1; i >= 0; i--) {
    if (b) {
      int vogal=0;
      for (auto c: s[i]) {
        if (isvowel(c)) break;
        vogal++;
      }
      if (vogal <= 2) ans += vogal;
    }
    
    ans++;
    if (isvowel(s[i][0])) b = false;
    else b = true;
  }*/
  
  if (val[n+1] == INF) cout << "*\n";
  else cout << val[n+1]-1 << '\n';

  return 0;
}
