r, c, k = map(int, input().split())

for i in range(r):
    a, b = input().split()
    if a != '*' * c and b.count('*') > 0:
        print('N')
        break
else:
    print('Y')
