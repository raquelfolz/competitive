p, s = map(int, input().split())
l = []

for i in range(s):
    u, v = map(int, input().split())
    l.append((u, False))
    l.append((v, True))

l.sort()

ini = -1
cnt = 0

for k in l:
    if k[1] is False:
        cnt += 1
        if cnt == 1:
            ini = k[0]
    else:
        cnt -= 1
        if cnt == 0:
            print(ini, k[0])
