#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

using namespace std;

int l[110];
int tam[110];

int find(int x) {
  if (l[x] == x) return x;
  return l[x] = find(l[x]);
}

void unite(int a, int b) {
  a = find(a);
  b = find(b);
  if (a == b) return;
  if (tam[a] < tam[b]) swap(a, b);
  tam[a] += tam[b];
  l[b] = a;
}

struct aresta {
  int a, b;
  int p;
};

vector<aresta> g;

bool aresta_cmp(aresta a, aresta b) {
  return a.p < b.p;
}

int kruskal() {
  int t = 0;
  sort(g.begin(), g.end(), aresta_cmp);
  for (auto e: g) {
    if (find(e.a) == find(e.b)) continue;
    t += e.p;
    unite(e.a, e.b);
    cout << e.a << " " << e.b << '\n';
  }
  return t;
}

int n, m;
int teste;
aresta a;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  while (true) {
    cin >> n >> m;
    if (n == 0) break;

    cout << "Teste " << ++teste << '\n';

    for (int i = 1; i <= n; i++) {
      l[i] = i;
      tam[i] = 1;
    }
    g.clear();

    for (int i = 0; i < m; i++) {
      cin >> a.a >> a.b >> a.p;
      g.pb(a);
    }

    //for (int i = 0; i <= 6; i++) cerr << l[i] << ' ';
    //cerr << '\n';

    kruskal();

    cout << '\n';
  }

  return 0;
}
