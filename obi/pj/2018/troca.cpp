#include <bits/stdc++.h>

using namespace std;


int n, t;
pair<int, int> carta[100100];
int i, j;
int cnt[100100];
bool ps[100100];

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> t;

  for (int k = 1; k <= n; k++) cin >> carta[k].first;
  for (int k = 1; k <= n; k++) cin >> carta[k].second;

  for (int k = 0; k < t; k++) {
    cin >> i >> j;
    cnt[i]++;
    cnt[j+1]++;
  }

  ps[1] = cnt[1] % 2;
  for (int k = 2; k <= n; k++) ps[k] = (ps[k-1] != (bool) (cnt[k]%2));


  if (ps[1]) cout << carta[1].second;
  else cout << carta[1].first;
  for (int k = 2; k <= n; k++) {
    if (ps[k]) cout << ' ' << carta[k].second;
    else cout << ' ' << carta[k].first;
  }
  cout << '\n';


  return 0;

}
