p, r = [int(x) for x in input().split()]

# se P estiver no 0, a bolinha vai automaticamente para C
# caso contrario, a bolinha vai passar por R
#   nesse caso se R estiver em 0, a bolinha vai para B
#   caso contrario, a bolinha vai para A

if p == 0:
    print("C")
else:
    if r == 0:
        print("B")
    else:
        print("A")
