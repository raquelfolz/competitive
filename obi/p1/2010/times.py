import heapq as hq

n, t = map(int, input().split())
a = [(lambda x: (-int(x[1]), x[0]))(input().split()) for i in range(n)]

hq.heapify(a)

l = [[] for i in range(t)]
cur = 0

for i in range(n):
    l[cur].append(hq.heappop(a)[1])
    cur += 1
    if cur == t:
        cur = 0

for i in range(t):
    print("Time", i+1)
    print(*sorted(l[i]), sep="\n")
    print()
