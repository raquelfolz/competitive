n = int(input())

# assumo que numero e primo
primo = True

# testo todos os potenciais divisores
for i in range(2, min(int(n**0.5)+1, n)):
    # se achei um divisor, nao eh primo
    if n % i == 0:
        primo = False
        break

if primo:
    print("N")
else:
    print("S")
