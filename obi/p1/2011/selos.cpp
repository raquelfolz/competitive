#include <bits/stdc++.h>

using namespace std;

long long n;
bool primo;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;

  // assumo que numero e primo
  primo = true;

  // testo todos os potenciais divisores
  for (long long i = 2; i <= min((long long) sqrt(n), n-1); i++) {
    // se achei um divisor, nao eh primo
    if (n % i == 0) {
      primo = false;
      break;
    }
  }


  if (primo) cout << "N\n";
  else cout << "S\n";

  return 0;

}
