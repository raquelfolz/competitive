#include <bits/stdc++.h>

using namespace std;

struct pais {
  int o=0, p=0, b=0;
  int id;
};

bool cmp(pais a, pais b) {
  if (a.o != b.o) return a.o > b.o;
  if (a.p != b.p) return a.p > b.p;
  if (a.b != b.b) return a.b > b.b;
  return a.id < b.id;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n, m;
  pais v[110];
  int o, p, b;

  cin >> n >> m;
  for (int i=1; i<=n; i++) v[i].id = i;

  for (int i=0; i<m; i++) {
    cin >> o >> p >> b;
    v[o].o++;
    v[p].p++;
    v[b].b++;
  }

  sort(v+1, v+n+1, cmp);

  for (int i=1; i<n; i++) cout << v[i].id << ' ';
  cout << v[n].id << '\n';

  return 0;

}
