#include <bits/stdc++.h>

using namespace std;

int ia, ib, fa, fb;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> ia >> ib >> fa >> fb;

  // B so troca de estado com interruptor C2, entao verificamos
  // B primeiro
  if (ib != fb) {
    // estado de A tambem trocou, entao se ja estava certo tem
    // que contar mais 1
    if (ia == fa) cout << 2 << '\n';
    else cout << 1 << '\n';
  }
  else {
    // estado de A nao trocou, entao so conta se estivesse errado
    if (ia == fa) cout << 0 << '\n';
    else cout << 1 << '\n';
  }

  return 0;

}
