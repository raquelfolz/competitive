ia, ib, fa, fb = [int(x) for x in input().split()]

# B so troca de estado com interruptor C2, entao verificamos
# B primeiro
if ib != fb:
    # estado de A tambem trocou, entao se ja estava certo tem
    # que contar mais 1
    if ia == fa:
        print(2)
    else:
        print(1)

else:
    # estado de A nao trocou, entao so conta se estivesse errado
    if ia == fa:
        print(0)
    else:
        print(1)
