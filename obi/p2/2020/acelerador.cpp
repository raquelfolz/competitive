#include <bits/stdc++.h>

using namespace std;

int d;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> d;

  // particula percorre 5km fora do circulo, independente de onde atinja
  d -= 5;

  // 1 volta completa = 8km
  // resposta = quanto percorre no circulo depois da ultima volta = mod 8
  d %= 8;

  cout << d << '\n';

  return 0;

}
