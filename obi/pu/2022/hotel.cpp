#include <bits/stdc++.h>

using namespace std;

int d, a, n;

int diaria;
int dias;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> d >> a >> n;

  diaria = d + a * (min(n, 15)-1);
  dias = 32-n;

  cout << diaria * dias << '\n';

  return 0;
}


