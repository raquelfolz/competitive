#include <bits/stdc++.h>

using namespace std;

int soma_digitos(int x) {
  int ans = 0;
  while (x) {
    ans += x % 10;
    x /= 10;
  }
  return ans;
}

int n, m, s;
int ans = -1;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m >> s;

  for (int i = m; i >= n; i--) {
    if (soma_digitos(i) == s) {
      ans = i;
      break;
    }
  }

  cout << ans << '\n';
 
  return 0;
}


