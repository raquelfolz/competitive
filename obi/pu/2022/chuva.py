n = int(input())
s = int(input())
x = [int(x) for x in input().split()]

i = 0 #primeiro numero do intervalo
j = 0 #numero depois do fim do intervalo
iz = -1 #numero depois do ultimo zero no inicio do intervalo (se existir)
jz = -1 #primeiro numero com zero no fim do intervalo (se existir)

soma = 0
cnt = 0

while i < n and j <= n:
    # se intervalo for inválido, movo j pra próxima posição válida
    if j <= i:
        j = i+1
        soma = x[i]
        continue
    
    # se a soma não for suficiente, aumento j
    if soma < s:
        # se j tiver grande demais, pode terminar
        if j >= n:
            break
        soma += x[j]
        j += 1
    
    # se a soma estiver grande demais, aumento i
    elif soma > s:
        soma -= x[i]
        i += 1
    
    # se a soma estiver certa, preciso contar os zeros no início e no fim
    else:
        iz = i
        while iz < n and x[iz] == 0:
            iz += 1
        jz = j
        while j < n and x[j] == 0:
            j += 1

        # 000abcd0000  -> sequencia
        # |  |   |   |  
        # i  iz  jz  j -> variaveis
        # 012345678911 -> indices
        # ----------01
        #
        # -> 4 opcoes para comecar (3 zeros mais a)
        #    5 opcoes para terminar (4 zeros mais d)
        #
        # iz-i e j-jz dao a quantidade de zeros
        # possibilidades: (iz-i+1) * (j-jz+1)

        # caso especial: a soma eh 0
        #
        # no inicio, i e j apontam para o primeiro e segundo na sequencia
        #
        # 000000
        # ||
        # ij
        #
        # depois, jz aponta para o segundo, iz e j movem para o final
        #
        # 000000
        # ||    |
        # ijz   iz/j
        #
        # iz - i vai dar a quantidade k de 0 na sequencia
        #
        # se a sequencia comeca no primeiro 0, tem k zeros onde pode terminar
        # se a sequencia comeca no segundo 0, tem k-1 zeros onde pode terminar
        # ...
        # se a sequencia comeca no penultimo 0, tem 2 zeros onde pode terminar
        # se a sequencia comeca no ultimo 0, tem 1 zero onde pode terminar
        #
        # -> progressao aritmetica, k termos variando de 1 a k
        # -> no fim sao k(k+1)/2 possibilidades

        if s == 0:
            cnt += ((iz-i)*(iz-i+1)) // 2
        else:
            cnt += (iz-i+1) * (j-jz+1)

        # agora precisa incrementar o i
        # iz está apontando para o primeiro caso com i != 0
        # se a soma fosse 0, iz não poderia ser primeiro termo de uma sequencia
        # se a soma fosse != 0, iz era o primeiro termo real
        # -> pode passar o i para iz+1
        i = iz+1
        
        # e subtrai da soma o valor de x[iz]
        # se sequencia fosse soma 0, i vai passar de j e vai resetar a soma
        # se sequencia nao fosse soma 0, vai ter tirado o valor corretamente
        soma -= x[iz]


print(cnt)
