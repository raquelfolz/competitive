l = int(input())
c = int(input())

# lado ocupado pelo t2 -> lado -0.5 em cima -0.5 em baixo
#                      -> lado-1 t2 em cada lado
t2 = (l-1)*2 + (c-1)*2


# divida o t1 em dois grupos
# grupo 1: linhas e colunas das que tocam a borda com os cantos
#          -> tantas por linha/coluna quanto cabem
#             (l de largura -> l em cada coluna)
#          -> l*c lajotas total
# grupo 2: linhas e colunas correspondentes as de tipo 2
#          -> tantas lajotas por linha/coluna quanto de t2
#          -> (l-1)*(c-1) lajotas total

# alternativa: cada lajota de t1 tem area 0.5
#                             t2 tem area 0.25
#                             t3 tem area 0.125
# area de t2 + t3: 0.25 * (l-1 + c-1)*2 + 0.125*4
#                  (l+c)*2/4 - 2*2/4 + 0.5
#                  (l+c)/2 - 0.5
# area total para t1: l*c - (l+c)/2 + 0.5
# quantidade de t1 para cobrir area: (l*c - (l+c)/2 + 0.5)*2
#                                    2*l*c - l+c + 1
#                                    l*c + l*c-l + c+1
#                                    l*c + l(c-1) - (c-1)
#                                    l*c + (l-1)(c-1)

t1 = l*c + (l-1)*(c-1)

print(t1)
print(t2)
