k, s = input().split()
k = int(k)

if k == 2:
    a = 0
    b = 0
    for i in range(len(s)):
        if s[i] == '1':
            if i % 2 == 0:
                a += 1
            else:
                b += 1
        else:
            if i % 2 == 0:
                b += 1
            else:
                a += 1
    if a <= b:
        print(a, '01'* (len(s)//2) + ('0' if len(s) %2 == 1 else ''))
    else:
        print(b, '10'* (len(s)//2) + ('1' if len(s) %2 == 1 else ''))
    quit()

def foo(x):
    if x == '1':
        return '0'
    return '1'

t = ['2']
cnt = 0
ans = 0
s += '2'
for i in range(len(s)-1):
    if s[i] != t[-1]:
        cnt = 1
        t.append(s[i])
    else:
        cnt += 1
        if cnt == k:
            if s[i+1] != s[i]:
                t[-1] = foo(t[-1])
                t.append(s[i])
                cnt = 1
                ans += 1
            else:
                t.append(foo(s[i]))
                cnt = 1
                ans += 1
        else:
            t.append(s[i])

print(ans, "".join(t[1:]))

