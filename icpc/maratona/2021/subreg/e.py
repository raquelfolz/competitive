n = int(input())

a = []
b = []

for i in range(n):
    t, d = map(int, input().split())
    if d == 0:
        a.append(t)
    else:
        b.append(t)

i = 0
j = 0
t = 0

while i < len(a) and j < len(b):
    if a[i] < b[j]:
        t = max(a[i], t) + 10
        i += 1
        while i < len(a) and a[i] < t:
            t = max(t, a[i] + 10)
            i += 1
    else:
        t = max(b[j], t) + 10
        j += 1
        while j < len(b) and b[j] < t:
            t = max(t, b[j] + 10)
            j += 1

while i < len(a):
    t = max(a[i], t) + 10
    i += 1
    while i < len(a) and a[i] < t:
        t = max(t, a[i] + 10)
        i += 1

while j < len(b):
    t = max(b[j], t) + 10
    j += 1
    while j < len(b) and b[j] < t:
        t = max(t, b[j] + 10)
        j += 1

print(t)
