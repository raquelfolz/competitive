#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

using namespace std;

int n, k;
pair<int, int> a[100100], b[100100];

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> k;
  for (int i = 0; i < n; i++) {
    cin >> a[i].f >> a[i].s;
    b[i] = a[i];
  }

  sort(a, a+n);

  for (int i = 0; i < n; i++) {
    if (a[i].s != b[i].s) {
      cout << "N\n";
      return 0;
    }
  }

  cout << "Y\n";

  return 0;
}
