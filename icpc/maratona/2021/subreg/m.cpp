#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

using namespace std;

int q;
int n;
int t, x;

bool morto[100100]; //true se a pessoa ja morreu
int pai[100100];
set<int> arv[100100]; //filhos com descendentes vivos

void cut(int x) {
  if (morto[x] == false) return;

  int p = pai[x];

  if (arv[x].size() == 0) {
    arv[p].erase(x);
  }
  else if (arv[p].size() == 1) {
    //cerr << "UPDATE " << p << '\n';
    for (auto k: arv[x]) {
      pai[k] = p;
    }
    arv[p] = arv[x];
  }

  cut(p);
}

int find(int x) {
  if (morto[x] == false) return x;
  //cerr << "Find " << x << ' ' << *arv[x].begin() << '\n';
  return find(*arv[x].begin());
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  n = 1;

  cin >> q;

  for (int i = 0; i < q; i++) {
    cin >> t >> x;

    if (t == 1) {
      pai[++n] = x;
      arv[x].insert(n);
    }
    else{
      morto[x] = true;
      cut(x);
      cout << find(1) << '\n';
    }
  }

  return 0;
}
