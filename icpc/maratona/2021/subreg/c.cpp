#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

using namespace std;

int b, l;
int d[200100];
long long resto;

int x;
int u=-1, v=-1;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> b >> l;

  for (int i = 0; i < l; i++) cin >> d[i];

  x = l % 2;

  for (int i = 0; i < l; i++) {
    if (i % 2 != x) resto += d[i];
    else resto -= d[i];
  }

  resto %= b+1;
  if (resto < 0) resto += b+1;

  for (int i = 0; i < l; i++) {
    if (i % 2 != x) {
      if (d[i] < resto) continue;
      u = i+1;
      v = d[i] - resto;
      break;
    }
    else {
      if (d[i] < b+1-resto) continue;
      u = i+1;
      v = d[i] + resto - b-1;
      break;
    }
  }

  if (resto == 0) {
    u = 0; v = 0;
  }

  cout << u << ' ' << v << '\n';

  return 0;
}
