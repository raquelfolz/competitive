#include <bits/stdc++.h>

using namespace std;

int n;
string dict[1010];
int g;
string guess[15];

int ans[15];
int temp[15];

int testa(int i, int j, int k) {
    string pal = dict[i];
    string test = dict[k];
    string fb = guess[j];
    //cerr << pal << ' ' << test << ' ' << fb << '\n';
    bool ok;
    for (int a = 0; a < 5; a++) {
        if (fb[a] == '*' and pal[a] != test[a]) {//cerr << "A\n";
        return 0;}
        else if (fb[a] == '!') {
            if (pal[a] == test[a]) {//cerr << "B1\n";
            return 0;}
            ok = false;
            for (int b = 0; b < 5; b++) {
                if (test[a] == pal[b]) {
                    ok = true;
                    break;
                }
            }
            if (not ok) {//cerr << "B2\n";
            return 0;}
        }
        else if (fb[a] == 'X') {
            for (int b = 0; b < 5; b++) {
                if (test[a] == pal[b]) {
                    {//cerr << test[a] << pal[b] <<"C\n";
                    return 0;}
                }
            }
        }
    }
    return 1;
}

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);

    cin >> n;
    for (int i = 0; i < n; i++) {
       cin >> dict[i];
    }

    cin >> g;
    for (int i = 0; i < g; i++) {
        cin >> guess[i];
    }

    for (int j =0; j < g; j++) {
        for (int k = 0; k < n; k++) {
            ans[j] += testa(0, j, k);
        }
        if (ans[j] == 0) break;
    }

    for (int j = 0; j < g; j++) cout << ans[j] << '\n';

    return 0;
}