n = int(input())
l = [int(x) for x in input().split()]
l.sort()

for i in range(n):
     if l[3*i] != l[3*i+1] or l[3*i] != l[3*i+2]:
        print("Y")
        break
else:
    print("N")