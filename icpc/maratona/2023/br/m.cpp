#include <bits/stdc++.h>
#define f first
#define s second

using namespace std;

long long n, m;
long long p, g;
long long u, v, d;

long long INF = 1e16;

vector<pair<long long, long long>> grafo[100100];
long long dist[100100];
bool passa[100100];
bool proc[100100];
priority_queue<pair<long long, pair<long long, int>>> q;

void djikstra() {
    for (long long i = 1; i <= n; i++) {
        dist[i] = INF;
    }
    dist[p] = 0;
    q.push({0, {p, 1}});
    while (! q.empty()) {
        long long a = q.top().s.f;
        long long aa = -q.top().f;
        int bb = q.top().s.s;
        //cerr << aa << ' ' << a << ' ' << bb << '\n';
        q.pop();
        if (proc[a]) {
            if (bb == 1 and dist[a]==aa) passa[a] = false;   
            continue;
        }
        proc[a] = true;
        if (bb == -1) passa[a] = true;
        if (a == g) bb = -1;
        for (auto u: grafo[a]) {
            long long b = u.f, w = u.s;
            if (dist[a] + w < dist[b] or (bb == 1 and dist[a] + w == dist[b])) {
                dist[b] = dist[a] + w;
                q.push({-dist[b], {b, bb}});
            }
        }
    }
}

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);

    cin >> n >> m;
    cin >> p >> g;
    for (long long i = 0; i < m; i++) {
        cin >> u >> v >> d;
        grafo[u].push_back({v, d});
        grafo[v].push_back({u, d});
    }
    //cerr << "a\n";

    djikstra();


    long long cnt=0;
    for (long long i = 1; i <= n; i++) {
        if (passa[i] and dist[i] == 2*dist[g]) {cnt += 1; cout << i << ' ';}
    }
    if (not cnt) cout << '*';
    cout << '\n';

    return 0;
}