notas = [0]*12
escala = [int(x) for x in "101011010101"]
nomes = ["do", "do#", "re", "re#", "mi", "fa", "fa#",
  "sol", "sol#", "la", "la#", "si"]

n = int(input())

for i in range(n):
    x = int(input()) - 1
    notas[x%12] = 1


notas = notas+notas
ans = None

for ini in range(12):
    l = notas[ini:ini+12]

    ok = True
    for i in range(12):
        if escala[i] < l[i]:
            ok = False
            break

    if ok:
        ans = ini
        break

if ans == None:
    print("desafinado")
else:
    print(nomes[ans])
