#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

#define pcount 78498

using namespace std;

int n, l;
int c;
int primos[80100] = {2, 3, 5, 7};

int mul;
unordered_map<int, int> pl;
int aux, ans;

unordered_map<int, int> b;

bool ok;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  //calcula primos
  aux = 4;
  for (int i = 11; i < 1000000; i+=2) {
    ok = true;
    for (int j = 0; j < aux; j++) {
      if (i / primos[j] < primos[j]) break;
      if (i % primos[j] == 0) {
        ok = false;
        break;
      }
    }
    if (ok) primos[aux++] = i;
  }

  cin >> n >> l;

  //encontra fatores
  for (int i = 0; i < n; i++) {
    cin >> c;

    for (int j = 0; j < pcount; j++) {
      if (c < primos[j]) break;

      aux = 0;
      while (c % primos[j] == 0) {
        aux++;
        c /= primos[j];
      }

      if (aux > 0) pl[primos[j]] = max(pl[primos[j]], aux);
    }
  }

  //mmc
  mul = 1;
  for (auto x: pl) {
    mul *= pow(x.f, x.s);
  }

  //valor para multiplicar e chegar perto de l
  aux = l / mul;
  //fatora valor
  for (int j = 0; j < pcount; j++) {
    if (aux < primos[j]) break;
    while (aux % primos[j] == 0) {
      //cerr << aux << '\n';
      aux /= primos[j];
      b[primos[j]]++;
    }
  }

  //calcula valor real com fatores em comum
  ans = 1;
  for (auto x: b) {
    ans *= pow(x.f, x.s + pl[x.f]);
    //cerr << x.f << ' ' << x.s << ' ' << pl[x.f] << '\n';
    //cerr << ans << ' ' << pow(x.f, x.s + pl[x.f]) << '\n';
  }

  //cerr << mul << '\n';
  cout << ans << '\n';

  return 0;
}
