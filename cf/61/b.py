def foo(s):
    l = [c for c in s if c not in "-;_"]
    return "".join(l)

a = foo(input().lower())
b = foo(input().lower())
c = foo(input().lower())

ans = [a+b+c, a+c+b, b+a+c, b+c+a, c+a+b, c+b+a]

n = int(input())

for i in range(n):
    s = foo(input().lower())
    if s in ans:
        print("ACC")
    else:
        print("WA")
