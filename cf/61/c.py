a, b = input().split()
c = int(input(), int(a))

if b != 'R':
    b = int(b)
    alf = [str(i) for i in range(10)] + [chr(c) for c in range(ord('A'), ord('Z')+1)]
    ans = ""
    while c > 0:
        ans += alf[c%b]
        c //= b
    if len(ans) == 0:
        print(0)
    else:
        print(ans[::-1])
else:
    # print(c)
    ans = ""
    while c >= 1000:
        ans += "M"
        c -= 1000
    if c >= 900:
        ans += "CM"
        c -= 900
    if c >= 500:
        ans += "D"
        c -= 500
    if c >= 400:
        ans += "CD"
        c -= 400
    while c >= 100:
        ans += "C"
        c -= 100
    if c >= 90:
        ans += "XC"
        c -= 90
    if c >= 50:
        ans += "L"
        c -= 50
    if c >= 40:
        ans += "XL"
        c -= 40
    while c >= 10:
        ans += "X"
        c -= 10
    if c == 9:
        ans += "IX"
        c = 0
    if c >= 5:
        ans += "V"
        c -= 5
    if c >= 4:
        ans += "IV"
        c -= 4
    while c >= 1:
        ans += "I"
        c -= 1
    print(ans)
