#include<bits/stdc++.h>

#include <ext/pb_ds/assoc_container.hpp>

using namespace std;
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
          tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll) 1e18) + 9;


ll n, m, k;
ll a[300100];
ll ans, b, c;

void solve() {
  cin >> n >> m >> k;
  for (int i = 0; i < n; i++) cin >> a[i];
  sort(a, a+n);
  ans = 0; b = 0;
  for (int i = 0; i < n; i++) {
    if (k == 0) break;
    c = min(m, k);
    ans += (a[i] + b)*c;
    b += c;
    k -= c;
  }
  cout << ans << '\n';
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int t;
  cin >> t;
  while (t--) solve();

  return 0;
}
