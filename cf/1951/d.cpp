#include<bits/stdc++.h>

#include <ext/pb_ds/assoc_container.hpp>

using namespace std;
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
          tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll) 1e18) + 9;

ll n, k;

void solve() {
  cin >> n >> k;
  if (k > (n+1)/2 and n != k) {
    cout << "NO\n";
    return;
  }
  
  cout << "YES\n";
  if (k == (n+1)/2) {
    cout << "2\n";
    cout << 2 << ' ' << 1 << '\n';
  } else {
    //quantidade de aneis eh estritamente menor que a metade do dinheiro
    cout << "2\n";
    cout << n-k+1 << ' ' << 1 << '\n';
  }
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int t;
  cin >> t;
  while (t--) solve();

  return 0;
}
