t = int(input())

for _ in range(t):
    n = int(input())
    s = input()
    l = s.count('1')
    
    if l % 2 == 1:
        print("NO")
    elif l == 2 and "11" in s:
        print("NO")
    else:
        print("YES")
