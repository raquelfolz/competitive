#include<bits/stdc++.h>

#include <ext/pb_ds/assoc_container.hpp>

using namespace std;
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
          tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll) 1e18) + 9;

int n, k;
int a[100100];
int fm;

int x, y;

void solve() {
  cin >> n >> k;
  for (int i = 0; i < n; i++) cin >> a[i];
  
  k--;
  //primeira vaca mais forte que eu
  for (fm = 0; fm < n; fm++) {
    if (a[fm] > a[k]) break;
  }

  //caso 1, troco com a primeira vaca dentre todas
  //perco da primeira vaca que eh mais forte...
  x = fm-1; //se a vaca ta na posicao 3(3 vacas antes dela)
            //entao so ganho 2 batalhas

  //caso 2, troco com a primeira vaca forte :D (mas so se estiver antes de mim!)
  y = 0;
  if (fm > k) goto fim;
  swap(a[fm], a[k]);
  if (fm > 0) y = 1; //ganho de quem eu enfrentar que tivesse antes de mim
  for (int i = fm+1; i < n; i++) {
    if (a[i] > a[fm]) break;
    y++;
  }
fim:
  //cerr << x << ' ' << y << '\n';
  cout << max(x, y) << '\n';
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int t;
  cin >> t;
  while (t--) solve();

  return 0;
}
