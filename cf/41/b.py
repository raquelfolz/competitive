n, b = map(int, input().split())
a = [int(x) for x in input().split()]

compra = 0
venda = 0
melhor = 0

for i in range(n):
    if a[i] < a[compra]:
        compra = i
        venda = i
    if a[i] >= a[venda]:
        venda = i
        melhor = max(melhor, (b//a[compra])*a[venda]+b%a[compra])

print(melhor)
