for _ in range(int(input())):
    n = int(input())
    a = [int(x) for x in input().split()]
    a.append(0)
    cnt = 0
    b = False
    for x in a:
        if x == 0:
            if b:
                cnt += 1
                b = False
        else:
            b = True
    print(min(2, cnt))
