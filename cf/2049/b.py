for _ in range(int(input())):
    n = int(input())
    s = input()
    ok = True
    for i in range(n):
        for j in range(i+1, n):
            if s[i] == 'p' and s[j] == 's':
                ok = False
                break
            elif s[i] == 's' and s[j] == 'p':
                #tem que ter j+1 caras no inicio
                #e n-i caras no final
                #o espaco de interseccao entre eles eh j-i+1
                #esse espaco tem que conter todo mundo de um deles
                if i != 0 and j != n-1:
                    ok = False
                    break
        if not ok:
            break
    print("YES" if ok else "NO")
                    
