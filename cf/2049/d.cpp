#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
ll k;
ll a[205][205];
ll dp[205][205][205]; //numero exato de shifts naquela linha
ll b;

void solve() {
  cin >> n >> m >> k;
  /*cerr << n << ' ' << m << ' ' << k << '\n';*/
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) cin >> a[i][j];
  }
  
  for (int s = 0; s < m; s++) {
    dp[0][0][s] = a[0][s]+k*s;
    for (int j = 1; j < m; j++) dp[0][j][s] = dp[0][j-1][s] + a[0][(j+s)%m];
  }

  for (int i = 1; i < n; i++) {
    for (int s = 0; s < m; s++) {
      dp[i][0][s] = LINF;
      for (int x = 0; x < m; x++) dp[i][0][s] = min(dp[i][0][s], dp[i-1][0][x]);
      dp[i][0][s] += a[i][s] + k*s;

      for (int j = 1; j < m; j++) {
        //vindo de cima
        dp[i][j][s] = LINF;
        for (int x = 0; x < m; x++) dp[i][j][s] = min(dp[i][j][s], dp[i-1][j][x]);
        dp[i][j][s] += a[i][(j+s)%m] + k*s;
        //vindo do lado
        dp[i][j][s] = min(dp[i][j][s], dp[i][j-1][s] + a[i][(j+s)%m]);
      }
    }
  }
  /*for (int i = 0; i < n; i++) {*/
  /*  for (int s = 0; s < m; s++) {*/
  /*    for (int j = 0; j < m; j++) cerr << dp[i][j][s] << ' ';*/
  /*    cerr << '\n';*/
  /*  }*/
  /*  cerr << '\n';*/
  /*}*/
  b = LINF;
  for (int s = 0; s < m; s++) b = min(b, dp[n-1][m-1][s]);
  cout << b << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

