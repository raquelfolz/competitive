for _ in range(int(input())):
    n, x, y = map(int, input().split())
    a = [i % 2 for i in range(n)]
    if len(a) % 2 == 1:
        a[-1] = 2
    x -= 1
    y -= 1
    if a[x] == 0 and a[y] == 0:
        l = [a[(y-1)%n], a[(y+1)%n]]
        if 1 not in l:
            a[y] = 1
        else:
            a[y] = 2
        #nao tem como nao ter 2, pq o ultimo 0 nao eh vizinho de 2
        if y == n-3 and n%2 == 1:
            a[n-1] = 1
            a[n-2] = 0
    if a[x] == 1 and a[y] == 1:
        a[x] = 2
        #o primeiro x nunca tem 2 como vizinho

    print(*a)
