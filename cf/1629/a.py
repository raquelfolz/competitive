t = int(input())

for _ in range(t):
    n, k = map(int, input().split())
    a = [int(x) for x in input().split()]
    b = [int(x) for x in input().split()]

    c = sorted([[a[i], b[i]] for i in range(n)])

    for x in c:
        if x[0] <= k:
            k += x[1]
        else:
            break

    print(k)
