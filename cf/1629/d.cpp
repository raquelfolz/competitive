#include <bits/stdc++.h>

using namespace std;

int t;
int n;
string s;

bool ok;
unordered_set<string> st;

bool checktriple(string x) {
  string y = {x[2], x[1], x[0]};
  if (st.count(y)) return true;

  // inverso -1 (obrigatoriamente a esquerda do atual)
  y = {x[2], x[1]};
  if (st.count(y)) return true;

  return false;
}

bool checkdouble(string x) {
  string y;

  //inverso direto
  y = {x[1], x[0]};
  if (st.count(y)) return true;

  //1 so
  y = x[0];
  if (st.count(y)) return true;
  y = x[1];
  if (st.count(y)) return true;

  //inverso + 1 (obrigatoriamente a esquerda do atual)
  for (char c = 'a'; c <= 'z'; c++) {
    y = {x[1], x[0], c};
    if (st.count(y)) return true;
  }

  return false;
}

bool check(string x) {
  if (x.size() == 1) return true;
  if (x.size() == 2) return checkdouble(x);
  return checktriple(x);
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {

    ok = false;
    st.clear();

    cin >> n;

    for (int i = 0; i < n; i++) {
      cin >> s;
      if (ok) continue;
      st.insert(s);
      ok = check(s);
    }

    if (ok) cout << "YES\n";
    else cout << "NO\n";

  }

  return 0;
}
