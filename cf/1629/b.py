# se eu conseguir fazer com outro numero, eu consigo fazer com 2\
#
# cada operacao reduz qtd em 1, e no fim devem ter exatamente
# tantos quanto os multiplos de 2
# -> aplicar cada troca com 1 nao multiplo e 1 multiplo e valido
#
# l=1, r=1 -> impossivel
# l=r != 1 -> automaticamente ok
# l%2 = 0, r%2 = 0 [0,1,2,3,4] -> precisa de floor((r-l+1)/2) trocas
# l%2 = 1, r%2 = 0 [1,2,3,4] -> precisa de (r-l+1)/2 trocas
# l%2 = 0, r%2 = 1 [0,1,2,3] -> precisa de (r-l+1)/2 trocas
# l%2 = 1, r%2 = 1 [1,2,3,4,5] -> precisa de ceil((r-l+1)/2) trocas


t = int(input())

for _ in range(t):
    l, r, k = map(int, input().split())

    if l == r == 1:
        print("NO")
    elif l == r:
        print("YES")
    elif l%2 != 1 and r%2 != 1:
        print("YES" if k >= (r-l+1)//2 else "NO")
    else:
        print("YES" if k >= (r-l+2)//2 else "NO")
