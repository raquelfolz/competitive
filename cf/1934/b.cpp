#include<bits/stdc++.h>

using namespace std;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 1e9+7;
const long long LINF = ((long long)1e18) + 9;

int n, ans;
int dp[5100]; //calcular qtd por dp pra ate 15*20=300 e divide os outros todos
int aux[5100];
vector<int> m = {1, 3, 6, 10, 15};

void solve() {
  cin >> n;
  ans = (n-300)/300;
  n -= ans*300;
  cout << ans*20 + dp[n] << '\n';
}

int test(int k) {
  ans = (k-300)/300;
  k -= ans*300;
  return ans*20 + dp[k];
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  
  for (int i = 1; i <= 5000; i++) {
    dp[i] = INF;
    for (auto x: m) {
      if (x <= i and dp[i] > dp[i-x]+1) {
        dp[i] = dp[i-x]+1;
        aux[i] = x;
      }
    }
  }

  /*for (int i = 1; i <= 5000; i++) {
    if (test(i) != dp[i]) {
      cerr << i << '\n';
      break;
    }
  }*/

  cin >> t;
  while (t--) solve();

  return 0;

}
