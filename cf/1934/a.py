t = int(input())

for _ in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    l.sort()
    print((l[-1] + l[-2] - l[0] - l[1])*2)
