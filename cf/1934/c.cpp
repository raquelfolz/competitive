#include<bits/stdc++.h>

using namespace std;

#define x first
#define y second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 1e9+7;
const long long LINF = ((long long)1e18) + 9;


int n, m;
int a, b, c, d;
pii p[10];

void solve() {
  cin >> n >> m;

  //primeiro chute, canto superior esquerdo
  cout << "? 1 1" << endl;
  cin >> a;
  if (a == 0) { cout << "! 1 1" << endl; return; }
  p[0].x = p[0].y = 1;

  //segundo chute, canto inferior direito
  cout << "? " << n << " " << m << endl;
  cin >> b;
  if (b == 0) { cout << "! " << n << " " << m << endl; return; }
  p[1].x = n; p[1].y = m;
  
  //terceiro chute, canto superior direito
  p[2].x = 1; p[2].y = m;
  cout << "? " << p[2].x << ' ' << p[2].y << endl;
  cin >> c;
  if (c == 0) { cout << "! " << p[2].x << ' ' << p[2].y << endl; return; }

  //quarto chute, se 1 e 3 intersectam
  if (a % 2 != (1 + m + c) % 2) goto fim;
  p[3].x = (p[0].x + p[0].y + a + p[2].x - p[2].y + c)/2;
  p[3].y = p[0].x + p[0].y + a - p[3].x;
  if (1 <= p[3].x and p[3].x <= n and 1 <= p[3].y and p[3].y <= m) {
    cout << "? " << p[3].x << ' ' << p[3].y << endl;
    cin >> d;
    if (d == 0) { cout << "! " << p[3].x << ' ' << p[3].y << endl; return; }
  }
  
fim:
  //2 e 3 intersectam
  p[4].x = (p[1].x + p[1].y - b + p[2].x - p[2].y + c)/2;
  p[4].y = p[1].x + p[1].y - b - p[4].x;
  cout << "! " << p[4].x << ' ' << p[4].y << endl;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while (t--) solve();

  return 0;

}
