#include <bits/stdc++.h>

using namespace std;

#define f first
#define s second
#define pb push_back

typedef long long ll;

const int INF = 1e9+7;

void solve() {
  int n, k;
  cin >> n >> k;
  if (k >= n-1) cout << 1 << "\n";
  else cout << n << "\n";
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int t;
  cin >> t;
  while (t--) solve();
  
  return 0;
}
