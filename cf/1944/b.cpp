#include <bits/stdc++.h>

using namespace std;

#define f first
#define s second
#define pb push_back

typedef long long ll;

const int INF = 1e9+7;

int n, k;
int a[100100];
int pos[100100][2];

vector<int> sep, esq, dir;
vector<int> f, s;

void solve() {
  //pra cada par de elementos:
  //- se tiverem na mesma metade do array, pega os dois pro mesmo lado
  //- se tiverem em metades separadas, pega um pra cada lado
  
  memset(pos, -1, sizeof(pos));
  cin >> n >> k;
  for (int i = 0; i < 2*n; i++) {
    cin >> a[i];
    if (pos[a[i]][0] == -1) pos[a[i]][0] = i;
    else pos[a[i]][1] = i;
  }
  
  sep.clear();
  esq.clear();
  dir.clear();

  for (int i = 1; i <= n; i++) {
    if (pos[i][0] >= n) dir.pb(i);
    else if (pos[i][1] >= n) sep.pb(i);
    else esq.pb(i);
  }
  
  int aux = min({(int) esq.size(), (int) dir.size(), k});
  f.clear();
  s.clear();

  for (int i = 0; i < aux; i++) {
    f.pb(esq[i]);
    f.pb(esq[i]);
    s.pb(dir[i]);
    s.pb(dir[i]);
  }
  for (int i = 0; f.size() < 2*k; i++) {
    f.pb(sep[i]);
    s.pb(sep[i]);
  }

  for (int i = 0; i < f.size()-1; i++) cout << f[i] << ' ';
  cout << f[f.size()-1] << '\n';

  for (int i = 0; i < s.size()-1; i++) cout << s[i] << ' ';
  cout << s[s.size()-1] << '\n';
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int t;
  cin >> t;
  while (t--) solve();
  
  return 0;
}
