#include <bits/stdc++.h>

using namespace std;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;


const int INF = 1e9+7;

int n;
int a[200100];
int freq[200100];


void solve() {
  //alice so pode pegar um numero de freq 1,
  //mas pode pegar qualquerr numero de freq 2+
  
  memset(freq, 0, sizeof(freq));

  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    freq[a[i]]++;
  }

  int ctrl = 0;
  for (int i = 0; i <= n; i++) {
    if (freq[i] <= ctrl) {
      cout << i << '\n';
      return;
    }
    if (freq[i] == 1) ctrl = 1;
  }

}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int t;
  cin >> t;
  while (t--) solve();
  
  return 0;
}
