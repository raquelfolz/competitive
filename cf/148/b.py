p = int(input())
d = int(input())
t = int(input())
f = int(input())
c = int(input())

if d <= p:
    print(0)
    quit()

delta = d-p
#corrida inicial da princesa
a = p*t
b = 0
cnt = 0

#tempo ate alcancar princesa
x = a / delta
a += x*p

while a < c:
    cnt += 1
    #tempo ate voltar pra caverna
    y = a / d
    a += y*p
    #tempo arrumando as coisas
    a += f*p
    #tempo ate alcancar princesa
    x = a / delta
    a += x*p

print(cnt)
