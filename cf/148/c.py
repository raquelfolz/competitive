n, a, b = map(int,input().split())

c = n - a - b

if a == n-1 and n > 1:
    print(-1)
    quit()

l = [1]
s = 1
m = 1
for i in range(b):
    l.append(s+1)
    m = s+1
    s += s+1
l += [1] * (c-1)
for i in range(a):
    l.append(m+1)
    m += 1

if m > 50000:
    print(-1)
else:
    print(*l)
