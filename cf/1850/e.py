t = int(input())

def foo(k, l):
    s = 0
    for x in l:
        s += (x+k)**2
    return s

for teste in range(t):
    n, c = map(int, input().split())

    s = [int(x) for x in input().split()]

    maior = int(((c/n)**0.5) /2) + 1
    menor = 0

    while menor <= maior:
        meio = (maior + menor)//2
        a = foo(meio*2, s)

        #print(meio, a)
        
        if a < c:
            #borda pequena demais!
            menor = meio+1
        elif a > c:
            #borda grande demais!
            maior = meio-1
        else:
            print(meio)
            break
    else:
        raise RuntimeError("busca binaria falhou :D")
