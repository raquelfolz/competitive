#include <bits/stdc++.h>

#define f first
#define s second

using namespace std;

typedef pair<int, int> pii;

int t;
int n, m;
int a, b, d;
map<int, int> c[200100]; //i-k=v;

long long pos[200100];
bool vis[200100];

bool ok;

void dfs(int v) {
  if (not ok) return;
  for (auto x: c[v]) {
    //cerr << x.f << '\n';
    if (not vis[x.f]) {
      //cerr << x.f << '\n';
      vis[x.f] = true;
      pos[x.f] = pos[v]-x.s;
      dfs(x.f);
    }
    else if (pos[v]-x.s != pos[x.f]) {
      //cerr << "a " << v << ' ' << x.f << ' ' << x.s << "\n";
      ok = false;
      return;
    }
  }
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {
    cin >> n >> m;
    for (int i = 0; i <= n; i++) c[i].clear();
    ok = true;
    for (int i = 0; i < m; i++) {
      cin >> a >> b >> d;
      if (c[a].count(b) != 0 and c[a][b] != d) {
        ok = false;
      }
      //cerr << a << ' ' << b << ' ' << d << '\n';
      c[a][b] = d;
      c[b][a] = -d;
    }
    if (not ok) {
      cout << "NO\n";
      continue;
    }

    memset(pos, 0, sizeof(pos));
    memset(vis, 0, sizeof(vis));

    for (int i = 1; i <= n; i++) {
      if (not vis[i]) {
        vis[i] = true;
        dfs(i);
        if (not ok) break;
      }
    }

    if (not ok) cout << "NO\n";
    else cout << "YES\n";
    //for (int i = 1; i<= n; i++) cerr << pos[i] << ' ';
    //cerr << endl;

    
  }

  return 0;

}
