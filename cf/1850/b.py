t = int(input())

for teste in range(t):
    n = int(input())
    l = [[int(x) for x in input().split()] + [i] for i in range(n)]

    ans = max(l, key=lambda x: -1 if x[0]>10 else x[1])

    print(ans[2] + 1)

