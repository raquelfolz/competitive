#include <bits/stdc++.h>

using namespace std;

int t;
int n;
int a, b, c;
map<int, int> s;

int m;
int cnt[200100];

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {
    cin >> n;
    s.clear();

    for (int i = 0; i < n; i++) {
      cin >> a;
      cnt[i+1] = 0;
      if (a > n) continue;
      if (s.count(a) != 0) s[a]+=1;
      else s[a]=1;
    }
    
    for (auto x: s) {
      a = x.first;
      b = x.second;
      for (int i = a; i <= n; i+= a) cnt[i] += b;
    }
    m = 0;
    for (int i = 1; i <= n; i++) m = max(m, cnt[i]);
    cout << m << '\n';
  }

  return 0;

}
