#include <bits/stdc++.h>

using namespace std;

int t;
long long n, k;
long long a[200100];

int cnt, m;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {
    cin >> n >> k;
    for (int i = 0; i < n; i++) cin >> a[i];

    sort(a, a+n);
      
    cnt = m = 1;
    for (int i = 1; i < n; i++) {
      if (a[i-1] + k < a[i]) {
        m = max(m, cnt);
        cnt = 1; //cnt reinicia no i
      }
      else {
        cnt++;
        //cerr << i << ' ' << cnt << "\n";
      }
    }

    m = max(m, cnt);
    cout << n-m << '\n';
  }

  return 0;

}
