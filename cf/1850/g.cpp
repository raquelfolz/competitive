#include <bits/stdc++.h>

#define F first
#define S second

using namespace std;

typedef pair<int, int> pii;

int t;
int n;
pii p[200100];

long long a;
long long cnt;

bool sa(pii a, pii b) { //norte sul
  return a.F < b.F;
}

bool sb(pii a, pii b) { //leste oeste
  return a.S < b.S;
}

bool sc(pii a, pii b) { //nordeste sudoeste
  return a.F-a.S < b.F-b.S;
}

bool sd(pii a, pii b) { //noroeste sudeste
  return a.F+a.S < b.F+b.S;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {
    cin >> n;
    for (int i = 0; i < n; i++) cin >> p[i].F >> p[i].S;
    
    cnt = 0;

    //norte sul
    sort(p, p+n, sa);
    a = 1;
    for(int i = 1; i < n; i++) {
      if (p[i].F == p[i-1].F) a++;
      else {
        cnt += a*(a-1);
        a=1;
      }
    }
    cnt += a*(a-1);

    //leste oeste
    sort(p, p+n, sb);
    a = 1;
    for(int i = 1; i < n; i++) {
      if (p[i].S == p[i-1].S) a++;
      else {
        cnt += a*(a-1);
        a=1;
      }
    }
    cnt += a*(a-1);

    //nordeste sudoeste
    sort(p, p+n, sc);
    a = 1;
    for(int i = 1; i < n; i++) {
      if (p[i].F-p[i].S == p[i-1].F-p[i-1].S) a++;
      else {
        cnt += a*(a-1);
        a=1;
      }
    }
    cnt += a*(a-1);

    //noroeste sudeste
    sort(p, p+n, sd);
    a = 1;
    for(int i = 1; i < n; i++) {
      if (p[i].F+p[i].S == p[i-1].F+p[i-1].S) a++;
      else {
        cnt += a*(a-1);
        a=1;
      }
    }
    cnt += a*(a-1);

    cout << cnt << '\n';

  }
  return 0;

}
