t = int(input())

for i in range(t):
    l = [int(x) for x in input().split()]
    s = sum(l) - min(l)
    if s >= 10:
        print("YES")
    else:
        print("NO")

