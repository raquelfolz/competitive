t = int(input())

for _ in range(t):

    n = int(input())
    a = [int(x) for x in input().split()]

    b = [False] * 60

    for x in a:
        while x > 0:
            if x <= n and b[x] == False:
                b[x] = True
                break
            x //= 2

    for i in range(1, n+1):
        if not b[i]:
            print("NO")
            break
    else:
        print("YES")
