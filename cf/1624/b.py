t = int(input())

for _ in range(t):
    a, b, c = map(int, input().split())

    x = b-a
    y = b+x
    if y > 0 and y % c == 0:
        print("YES")
        continue

    x = c-b
    y = b-x
    if y > 0 and y % a == 0:
        print("YES")
        continue

    x = (c-a)//2
    y = a+x
    if y > 0 and (c-a) % 2 == 0 and y % b == 0:
        print("YES")
        continue

    print("NO")
