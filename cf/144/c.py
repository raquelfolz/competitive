s = input() + '?'
p = input()

a = [0]*26
b = [0]*26

for c in p:
    a[ord(c)-ord('a')] += 1

for c in s[:len(p)]:
    if c != '?':
        b[ord(c)-ord('a')] += 1

cnt = 0
for i in range(len(s)-len(p)):
    for x in range(26):
        if b[x] > a[x]:
            break
    else:
        cnt += 1

    if s[i] != '?':
        b[ord(s[i])-ord('a')] -= 1
    if s[i+len(p)] != '?':
        b[ord(s[i+len(p)])-ord('a')] += 1

print(cnt)
