n = int(input())
a = [int(x) for x in input().split()]


b = 0
c = 0
for i in range(len(a)):
    if a[i] > a[b]:
        b = i
    if a[i] <= a[c]:
        c = i

ans = b + n-1 - c
if b > c:
    ans -= 1

print(ans)
