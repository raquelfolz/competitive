def distsq(a, b):
    return (a[0]-b[0])**2+(a[1]-b[1])**2

xa, ya, xb, yb = map(int, input().split())
n = int(input())
h = [[int(x) for x in input().split()] for i in range(n)]

if xa > xb:
    xa, xb = xb, xa
if ya > yb:
    ya, yb = yb, ya

cnt = 0
for r in h:
    # print(xa, ya, r, distsq((xa, ya), r))
    if distsq((xa, ya), r) <= r[2]**2:
        # print("ok")
        cnt += 1
        break
for r in h:
    # print(xa, yb, r, distsq((xa, yb), r))
    if distsq((xa, yb), r) <= r[2]**2:
        # print("ok")
        cnt += 1
        break
for r in h:
    # print(xb, ya, r, distsq((xb, ya), r))
    if distsq((xb, ya), r) <= r[2]**2:
        # print("ok")
        cnt += 1
        break
for r in h:
    # print(xb, yb, r, distsq((xa, yb), r))
    if distsq((xb, yb), r) <= r[2]**2:
        # print("ok")
        cnt += 1
        break

for x in range(xa+1, xb):
    for r in h:
        # print(x, ya, r, distsq((x, ya), r))
        if distsq((x, ya), r) <= r[2]**2:
            # print("ok")
            cnt += 1
            break
for x in range(xa+1, xb):
    for r in h:
        # print(x, yb, r, distsq((x, yb), r))
        if distsq((x, yb), r) <= r[2]**2:
            # print("ok")
            cnt += 1
            break
for y in range(ya+1, yb):
    for r in h:
        # print(xa, y, r, distsq((xa, y), r))
        if distsq((xa, y), r) <= r[2]**2:
            # print("ok")
            cnt += 1
            break
for y in range(ya+1, yb):
    for r in h:
        # print(xb, y, r, distsq((xb, y), r))
        if distsq((xb, y), r) <= r[2]**2:
            # print("ok")
            cnt += 1
            break

print((abs(xa-xb)+abs(ya-yb))*2-cnt)
