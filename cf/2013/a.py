t = int(input())

for _ in range(t):
    n = int(input())
    x, y = map(int, input().split())
    a = min(x, y)
    print((n+a-1)//a)
