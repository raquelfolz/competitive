#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;


int n;
int ans;
string s;
int cnt;

void solve() {
  cin >> n;
  s = "";
  cnt = 0;
  for (int i = 0; i < n; i++) {
    cout << "? " << s << '0' << endl;
    cnt++;
    cin >> ans;
    if (ans == 1) {s += "0"; continue;}
    cout << "? " << s << '1' << endl;
    cnt++;
    cin >> ans;
    if (ans == 1) {s += "1"; continue;}
    break;
  }
  //chegou ate aqui, fez no maximo 2*(min(s.size()+1, n)) tentativas
  //mas pior caso na verdade seria tudo 1, sem nenhum 0
  //o que terminaria a string
  //entao ou string acabou ou tem pelo menos 1 tentativa restante por caractere
  //
  //pior caso incompleto: 111011111111111 -> 1[0]+2*(n-ini)[1] -> 2*n - 2*ini +1
  //o que significa que tem sobrando pelo menos suficiente pra completar s

  //string s ate agora eh obrigatoriamente um sufixo
  //entao so preciso testar adicionar gente na frente a um custo de 1 por char
  for (int i = s.size(); i < n; i++) {
    cout << "? " << '0' << s << endl;
    cnt++;
    cin >> ans;
    if (ans == 1) s = "0"+s;
    else s = "1"+s;
  }

  cout << "! " << s << endl;
  cerr << "tentativas: " << cnt << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

