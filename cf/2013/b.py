t = int(input())

for _ in range(t):
    n = int(input())
    a = [int(x) for x in input().split()]
    for i in range(n-2):
        a[-2] -= a[i]
    print(a[-1] - a[-2])
