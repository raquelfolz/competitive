#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int u, v;
vector<int> g[200100];
int tipo[200100];
ll nfolhas, nviz, nnorm;
map<pii, int> m;
bool vis[200100];

int dfs(int x) {
  int cnt=0;
  for (auto y: g[x]) {
    if (m.count({x, y}) == 0) {
      m[{y, x}] = nnorm; m[{x, y}] = 0;
      m[{x, y}] = dfs(y);
      if (tipo[y] == 0) m[{x, y}]++;
      cnt += m[{x, y}];
      m[{y, x}] -= m[{x, y}];
    }
  }
  /*for (int i = 0; i < g[x].size(); i++) {*/
  /*  if (! vis[g[x][i].f]) {*/
  /*    vis[g[x][i].f] = true;*/
  /*    g[x][i].s = dfs(g[x][i].f);*/
  /*    if (tipo[g[x][i].f] == 0) g[x][i].s += 1;*/
  /*    cnt += g[x][i].s;*/
  /*  }*/
  /*}*/
  return cnt;
}

ll ans;

void solve() {
  m.clear();
  cin >> n;
  for (int i = 1; i <= n; i++) { g[i].clear(); tipo[i] = 0;}
  for (int i = 0; i < n-1; i++) {
    cin >> u >> v;
    g[u].pb(v);
    g[v].pb(u);
  }
  for (int i = 1; i <= n; i++) {
    if (g[i].size() == 1) {
      tipo[i] = 1;
      for (auto x: g[i]) if (tipo[x] != 1) tipo[x] = 2;
    }
  }
  nfolhas = 0; nviz = 0;
  for (int i = 1; i <= n; i++) {
    if (tipo[i] == 1) nfolhas++;
    else if (tipo[i] == 2) nviz++;
  }
  nnorm = n - nfolhas - nviz;
  
  //caso 1: q eh folha desde sempre
  ans = nfolhas * (n-nfolhas);
  //caso 2: q depois da alice vai ser vizinho de uma folha
  //preciso de um vertice que tenha pelo menos dois vizinhos que nao sao folhas!
  //um vizinho vai ser o bob
  //alice vai estar na subarvore do outro vizinho
  //=> pra cada aresta preciso saber o numero de caras daquela subarvore que nao sao folhas ou vizinhos de folhas
  //que vao ser onde alice pode estar no inicio
  //
  //eu assumo que q vai comecar em um vertice que nao eh folha
  //pra cada vizinho que for vizinho de alguma folha
  //eu olho a quantidade de posicoes possiveis da alice se eu pegar aquela aresta
  memset(vis, 0, sizeof(vis));
  dfs(1);

  /*for (int i = 1; i <= n; i++) cout << tipo[i];*/
  /*cout << '\n';*/

  /*cerr << "C1 " << ans << '\n';*/
  /*cerr << "C2\n";*/
  for (int x = 1; x <= n; x++) {
    if (tipo[x] == 1) continue;
    for (auto y: g[x]) {
      if (tipo[y] != 2) continue; //nao pode ser uma folha tbm pq alice nao poderia estar ali
      /*cerr << x << ' ' << y << ' ' << m[{x, y}] <<'\n';*/
      ans += m[{x, y}];
    }
  }

  cout << ans << '\n';
  /*cerr << '\n';*/
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

