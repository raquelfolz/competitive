#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, k;

pair<ll, ll> foo(int x) {
  if (x < k) return {0, 0};
  pair<ll, ll> aux;
  aux = foo(x/2);
  ll ans = aux.f*2 + ((x+1)/2) * (aux.s + x%2);
  return {ans, aux.s*2 + x%2};
}

void solve() {
  cin >> n >> k;
  cout << foo(n).f << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

