#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
pii a[200100];
int occ[400100];
int ps[400100];

void solve() {
  memset(occ, 0, sizeof(occ));
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a[i].f >> a[i].s;
    if (a[i].f == a[i].s) occ[a[i].f] += 1;
  }
  for (int i = 1; i <= 2*n; i++) ps[i] = ps[i-1] + min(1, occ[i]);

  /*for (int i = 0; i <= 2*n; i++) cerr << occ[i];*/
  /*cerr << '\n';*/
  /*for (int i = 0; i <= 2*n; i++) cerr << ps[i] << ' ';*/
  /*cerr << '\n';*/

  for (int i = 0; i < n; i++) {
    /*cerr << a[i].f << ' ' << a[i].s << ' ';*/
    if (a[i].f == a[i].s) {
      if (occ[a[i].f] < 2) cout << '1';  
      else cout << '0';
    }
    else if (ps[a[i].s] - ps[a[i].f-1] != a[i].s - a[i].f + 1) cout << '1';
    else cout << '0';
    /*cerr << '\n';*/
  }
  cout << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

