for _ in range(int(input())):
    n = int(input())
    a = [int(x) for x in input().split()]
    for i in range(n-1):
        if 2*min(a[i], a[i+1]) > max(a[i], a[i+1]):
            print("YES")
            break
    else:
        print("NO")
