#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int a[200100], b[200100];
int m;
int d[200100];

multiset<int> s;
bool ok;

void solve() {
  //requerimentos:
  //d[m-1] tem que estar entre os elementos de b
  //cada elemento de b diferente do de a na mesma posicao deve vir de d

  s.clear();
  ok = false;

  cin >> n;
  for (int i = 0; i < n; i++) cin >> a[i];
  for (int i = 0; i < n; i++) cin >> b[i];
  cin >> m;
  for (int i = 0; i < m; i++) {
    cin >> d[i];
    s.insert(d[i]);
  }

  for (int i = 0; i < n; i++) {
    if (b[i] == d[m-1]) ok = true;
    if (a[i] != b[i]) {
      auto it = s.find(b[i]);
      if (it != s.end()) s.erase(it);
      else goto fim;
    }
  }

  if (ok) {
    cout << "YES\n";
    return;
  }
  //else cerr << "a";

fim:
  cout << "NO\n";
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

