t = int(input())

for _ in range(t):
    n, m = map(int, input().split())
    s = input()
    k = 0
    for l in "ABCDEFG":
        k += max(0, m-s.count(l))
    print(k)
