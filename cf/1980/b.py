t = int(input())

for _ in range(t):
    n, f, k = map(int, input().split())
    a = [int(x) for x in input().split()]

    v = a[f-1]
    a.sort(reverse=True)

    if a[k-1] > v:
        print("NO")
    elif k == n or a[k] < v:
        print("YES")
    else:
        print("MAYBE")

