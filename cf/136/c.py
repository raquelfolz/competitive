n = int(input())
a = [int(x) for x in input().split()]
a.sort()
if a[-1] == 1:
    print(*a[:-1], 2)
else:
    print(1, *a[:-1])
