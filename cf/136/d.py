
def dist_sq(a, b):
    return (a[0]-b[0])**2 + (a[1]-b[1])**2
def is_right_angle(a, b, c):
    return dist_sq(a, b) + dist_sq(b, c) == dist_sq(a, c)
def is_rectangle(a, b, c, d):
    return (is_right_angle(a, b, c) and
            is_right_angle(b, c, d) and
            is_right_angle(c, d, a) and
            dist_sq(a,b) == dist_sq(c,d))
def is_square(a, b, c, d):
    return is_rectangle(a, b, c, d) and dist_sq(a, b) == dist_sq(b, c)

l = [[int(x) for x in input().split()] for i in range(8)]
p = []
c = [False]*10

def search():
    if len(p) == 8:
        if is_rectangle(*[l[x] for x in p[4:]]):
            print("YES")
            print(*[x+1 for x in p[:4]])
            print(*[x+1 for x in p[4:]])
            quit()
        return
    if len(p) == 4 and not is_square(*[l[x] for x in p]):
        return
    for i in range(8):
        if not c[i]:
            c[i] = True
            p.append(i)
            search()
            c[i] = False
            p.pop()

search()
print("NO")
