s = input()
a = 0
b = 0
c = 0

la = -1
lb = -1
lc = -1
ld = -1
i = 0
for x in s:
    if x == '0':
        a += 1
        la = i
    elif x == '1':
        b += 1
        lb = i
    else:
        c += 1
        if lc != -1:
            ld = lc
        lc = i
    i += 1

# print(a, b, c)
# print(la, lb, lc)

ans = set()
#0 eh apagado pelo bob (len(s)-2)//2 vezes
if a+c >= (len(s)-2)//2+2:
    ans.add("00")
#1 eh apagado pela alice (len(s)-1)//2 vezes
if b+c >= (len(s)-1)//2+2:
    ans.add("11")
#se vai sobrar exatamente um 0 (e um 1 por consequencia)
if a == (len(s)-2)//2+1:
    #0s tao fixos, nao da pra mudar
    if la < max(lb, lc):
        ans.add("01")
    else:
        ans.add("10")
elif b == (len(s)-1)//2+1:
    #1s tao fixos, nao da pra mudar
    if max(la, lc) < lb:
        ans.add("01")
    else:
        ans.add("10")
elif a+c >= (len(s)-2)//2+1 and b+c >= (len(s)-1)//2+1:
    x = (len(s)-2)//2+1 - a
    y = (len(s)-1)//2+1 - b
    if x > 0 and y > 0 and s[-2:] == '??':
        ans.add('01')
        ans.add('10')
    #tentar 01
    pri = False
    for i in range(len(s)-1, -1, -1):
        if not pri:
            if s[i] == '1':
                pri = True
            elif s[i] == '?' and y > 0:
                pri = True
            else:
                break
        else:
            if s[i] == '0' or (s[i] == '?' and x > 0):
                ans.add('01')
                break
    #tentar 10
    pri = False
    for i in range(len(s)-1, -1, -1):
        if not pri:
            if s[i] == '0':
                pri = True
            elif s[i] == '?' and x > 0:
                pri = True
            else:
                break
        else:
            if s[i] == '1' or (s[i] == '?' and y > 0):
                ans.add('10')
                break

print(*sorted(ans), sep='\n')

