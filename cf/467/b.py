n, m, k = map(int, input().split())
l = [int(input()) for x in range(m+1)]

ans = 0
#print(f"{l[-1]:b}")
for i in range(m):
    cnt = 0
    x = l[i] ^ l[-1]
    #print(f"{l[i]:b} {x:b}")
    for j in range(n):
        if x & (1 << j):
            cnt += 1
    #print(k, cnt)
    if cnt <= k:
        ans += 1

print(ans)
