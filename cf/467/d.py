import sys
sys.setrecursionlimit(10**5+500)

idd = dict()
d = []

m = int(input())
e = [x.lower() for x in input().split()]
for i in range(m):
    if e[i] not in idd:
        idd[e[i]] = len(idd)
        d.append((e[i].count('r'), len(e[i])))
    e[i] = idd[e[i]]

n = int(input())
g = dict()

for i in range(n):
    x, y = input().lower().split()
    if x not in idd:
        idd[x] = len(idd)
        d.append((x.count('r'), len(x)))
    if y not in idd:
        idd[y] = len(idd)
        d.append((y.count('r'), len(y)))
    iy = idd[y]
    if iy not in g:
        g[iy] = []
    g[iy].append(idd[x])

r = dict()
def f(x):
    return x if not x in r else r[x]
def kf(w):
    return d[f(w)]

def dfs(w, v):
    if v not in g:
        return
    for u in g[v]:
        if u in vis or kf(w) >= kf(u):
            continue
        #print(w, kf(w), u, kf(u), v)
        r[u] = w
        vis.add(u)
        dfs(w, u)

for v in g:
    if v in r:
        continue;
    vis = set()
    vis.add(v)
    dfs(v, v)

#for x in g:
#    print(x, *g[x])

#print(r)


for i in range(m):
    e[i] = f(e[i])

#print(*(f(w) for w in e))
print(sum(d[w][0] for w in e), sum(d[w][1] for w in e))

