n, m, k = map(int, input().split())
p = [int(x) for x in input().split()]

ps = [p[0]]
for i in range(1, n):
    ps.append(ps[-1] + p[i])

tab = [[0]*(n) for x in range(k+1)] #soma maxima com i sequencias todas terminando ate j
for i in range(1,k+1):
    tab[i][m*i-1] = ps[m*i-1]
    for j in range(m*i,n):
        #ou eu pego a mesma sequencia que terminava ate j-1
        #ou eu pego uma sequencia terminando em j com i-1 caras terminando ate j-m
        #print(i, j)
        tab[i][j] = max(tab[i][j-1], tab[i-1][j-m] + ps[j] - ps[j-m])

#print(ps)
#print(tab)
print(tab[k][n-1])
