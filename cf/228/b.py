na, ma = map(int, input().split())
a = [[x=='1' for x in input()] for y in range(na)]
nb, mb = map(int, input().split())
b = [[x=='1' for x in input()] for y in range(nb)]

melhor = [0, 0, 0]
for x in range(-na+1, nb):
    for y in range(-ma+1, mb):
        r = 0
        for i in range(max(0, -x), min(na, nb-x)):
            for j in range(max(0, -y), min(ma, mb-y)):
                if a[i][j] and b[i+x][j+y]:
                    r += 1
        if r > melhor[2]:
            melhor = [x, y, r]

print(melhor[0], melhor[1])
