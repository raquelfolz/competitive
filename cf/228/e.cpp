#include <bits/stdc++.h>

using namespace std;

#define pb push_back
#define f first
#define s second


vector<int> g[210], r[210];
stack<int> out;
set<int> ans;

int n, m;
int qtd;
int comp[210];
bool vis[210];

bool dfs(int v) {
  if (vis[v]) return false;
  vis[v] = true;
  comp[v] = qtd;
  for (auto x: g[v]) dfs(x);
  //out.push(v);
  return true;
}

bool kosaraju(int v) {
  if (comp[v] != -1) return false;
  comp[v] = qtd;
  for (auto x: r[v]) kosaraju(x);
  return true;
}

void asf(int v) {
  if (vis[(v>>1)]) return;
  vis[(v>>1)] = true;
  if (v&1) ans.insert((v>>1));
  for (auto x: g[v]) asf(x);
}

int u, v, w;

void solve() {

  cin >> n >> m;
  for (int i = 0; i < 210; i++) {
    g[i].clear();
    r[i].clear();
  }

  qtd = 0;
  memset(comp, -1, sizeof(comp));
  memset(vis, 0, sizeof(vis));

  for (int i = 0; i < m; i++) {
    cin >> u >> v >> w;
    
    if (w) {
      //ja esta asfaltada, asfalta sse
      g[(u<<1)].pb((v<<1));
      g[(u<<1)|1].pb((v<<1)|1);
      g[(v<<1)].pb((u<<1));
      g[(v<<1)|1].pb((u<<1)|1);
    }

    else {
      //nao esta asfaltada, tem que escolher exatamente 1
      g[(u<<1)|1].pb((v<<1));
      g[(u<<1)].pb((v<<1)|1);
      g[(v<<1)|1].pb((u<<1));
      g[(v<<1)].pb((u<<1)|1);
    }
  }
  //cerr << "A\n";

  for (int i = 1; i <= n; i++) {
    if (dfs((i<<1))) qtd++;
    if (dfs((i<<1)|1)) qtd++;
    if (comp[(i<<1)] == comp[(i<<1)|1]) {
      cout << "Impossible" << '\n';
      return;
    }
  }
  //cerr << "B\n";

  /*while (! out.empty()) {
    if (kosaraju(out.top())) qtd++;
    out.pop();
  }

  for (int i = 1; i <= n; i++) {
    if (comp[(i<<1)] == comp[(i<<1)|1]) {
      cout << "NO\n";
      return;
    }
  }*/

  memset(vis, 0, sizeof(vis));

  for (int i = 1; i <= n; i++) {
    asf((i<<1));
    asf((i<<1)|1);
  }

  cout << ans.size() << '\n';
  
  auto it = ans.begin();
  cout << *it;
  it++;
  for (; it != ans.end(); it++) cout << ' ' << *it;
  cout << '\n';
}

int main () {
  ios::sync_with_stdio(0);
  cin.tie(0);

   solve();

  return 0;
}
