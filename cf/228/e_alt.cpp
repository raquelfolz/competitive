#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b, c;
bool e[10100];
vector<pii> g[110];

int p[110];
int t, s;
int ans;

bool dfs(int v) {
  //cerr << v << ' ' << p[v] << '\n';
  for (auto u: g[v]) {
    //cerr << u.f << ' ' << u.s << ' ' << p[u.f] << ' ' << e[u.s] << '\n';
    if (p[u.f] == 0) {
      if (e[u.s]) p[u.f] = p[v];
      else p[u.f] = p[v]==1 ? -1 : 1;
      t++;
      if (p[u.f] == 1) s++;
      if (not dfs(u.f)) return false;
    } else if (e[u.s]) {
      if (p[u.f] != p[v]) return false;
    } else {
      if (p[u.f] == p[v]) return false;
    }
  }
  return true;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    e[i] = c;
    g[a].pb({b, i});
    g[b].pb({a, i});
  }

  for (int i = 1; i <= n; i++) {
    if (p[i] != 0) continue;
    p[i] = 1;
    t = 1; s = 1;
    if (not dfs(i)) {
      cout << "Impossible\n";
      return 0;
    }
    ans += s;
  }

  cout << ans << '\n';
  for (int i = 1; i <= n; i++) {
    if (p[i] == 1) cout << i << ' ';
  }
  cout << '\n';

  return 0;
}

