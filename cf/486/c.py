n, p = map(int, input().split())
s = input()

p -=1
q = n-p-1

a = ord(min(s[p], s[q]))
b = ord(max(s[p], s[q]))
cnt = min(b-a, a-b+26)

meio = 0
d = 0
for i in range(min(q, p)+1, n//2):
    a = ord(min(s[i], s[n-i-1]))
    b = ord(max(s[i], s[n-i-1]))
    if a == b:
        d += 1
    else:
        d = 0
    meio += min(b-a, a-b+26)
    # print(i, min(b-a, a-b+26))

if meio != 0:
    cnt += meio
    cnt += n//2 - min(q, p)-1-d
# print(n//2 - min(q, p)-1-d)

c = 0
ponta = 0
for i in range(min(q, p)-1, -1, -1):
    a = ord(min(s[i], s[n-i-1]))
    b = ord(max(s[i], s[n-i-1]))
    if a == b:
        c += 1
    else:
        c = 0
    ponta += min(b-a, a-b+26)
    # print(i, min(b-a, a-b+26))

if ponta != 0:
    cnt += ponta
    cnt += min(q, p) - c
# print(min(q, p) - c)

#se os dois forem verdade eu posso
if ponta != 0 and meio != 0:
    #1 voltar o meio
    l = [n//2 - min(q, p)-1-d]
    #2 voltar a ponta
    l.append(min(q, p) - c)
    #nao vale a pena avancar a ponta ou o meio nunca
    cnt += min(l)


print(cnt)
