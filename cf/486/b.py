m, n = map(int, input().split())

b = [[int(x) for x in input().split()] for i in range(m)]

linhas = [i for i in range(m) if sum(b[i]) == n]
colunas = [j for j in range(n) if sum([b[i][j] for i in range(m)]) == m]

ok = True
if sorted([(1 if sum(linhas) > 0 else 0), (1 if sum(colunas) > 0 else 0)]) == [0, 1]:
    ok = False

a = [[0]*n for i in range(m)]
for l in linhas:
    for c in colunas:
        a[l][c] = 1

cl = [1 if sum(a[i]) > 0 else 0 for i in range(m)]
cc = [1 if sum([a[i][j] for i in range(m)]) > 0 else 0 for j in range(n)]

c = [[1 if cl[i]+cc[j]>0 else 0 for j in range(n)] for i in range(m)]

if b == c:
    print("YES")
    for l in a:
        print(*l)
else:
    print("NO")
