n = int(input())
lc, rc = [], []
for i in range(n):
    l, r = map(int, input().split())
    lc.append(l)
    rc.append(r)
m = int(input())
lp, rp = [], []
for i in range(m):
    l, r = map(int, input().split())
    lp.append(l)
    rp.append(r)

print(max([max(lp)-min(rc), max(lc)-min(rp), 0]))
