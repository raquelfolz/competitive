n, m = map(int, input().split())

#primeiros m dias nao acontece nada
#depois, pro dia i, quantidade de graos diminui em i-m
#nos dias [m+1, m+k], quantidade diminui em (k+1)*k/2
#preciso de (k+1)*k/2 = n-m (-m pq no ultimo dia nao recarrega)
#k^2 + k - 2(n-m) = 0
#solucao minima para k > 0

if m >= n:
    print(min(n, m))
    quit()

b = 1
c = -2*(n-m)
delta = b*b - 4*c
k = int((-b + delta**0.5)//2)

y = (k+1)*k//2+m
# print(k, (k+1)*k//2)
# print(k+1, (k+2)*(k+1)//2+m)

if y >= n:
    print(k+m)
else:
    print(k+1+m)

"""
b = n
for i in range(1, 11):
    a = min(n, b+m)
    b = max(0, a-i)
    print(i, a, b)
"""
