l = [4, 6, 8, 12, 20]
t = ["Tetrahedron", "Cube", "Octahedron", "Dodecahedron", "Icosahedron"]
n = int(input())
print(sum([l[t.index(input())] for x in range(n)]))
