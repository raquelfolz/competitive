#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;


ll n, s;
ll a[200100];
void solve() {

  cin >> n >> s;
  for (int i = 0; i < n; i++) cin >> a[i];
  
  pii ans = {0, -1};
  int l = 0, r = 0;
  s += a[0];

  while(r < n) {
    if (r < l) s += a[++r];
    else if (s >= 0) {
      if (r - l > ans.s - ans.f) ans = {l, r};
      s += a[++r];
    }
    else {
      s -= a[l++];
    }
  }
  if (ans.s == -1) cout << "-1\n";
  else cout << ans.f+1 << ' ' << ans.s+1 << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

