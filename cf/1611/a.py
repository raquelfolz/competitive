def paridade(c):
    return (int(c)+1) % 2

t = int(input())
for _ in range(t):
    s = input()
    if paridade(s[-1]):
        print(0)
    elif paridade(s[0]):
        print(1)
    elif sum([paridade(x) for x in s]) > 0:
        print(2)
        #print([paridade(x) for x in s])
    else:
        print(-1)

