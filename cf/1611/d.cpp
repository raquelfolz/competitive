#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;


int n;
int b[200100], p[200100];
int w[200100], d[200100];

void solve() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> b[i];
  for (int i = 0; i < n; i++) cin >> p[i];
  
  //primeiro cara da permutacao tem que ser a raiz
  if (b[p[0]] != p[0]) {
    cout << "-1\n";
    return;
  }

  memset(w, -1, sizeof(w));
  w[p[0]] = 0;
  d[p[0]] = 0;
  
  for (int i = 1; i < n; i++) {
    //se o pai nao tem distancia, o pai ta depois -> falha!
    if (w[b[p[i]]] == -1) {
      cout << "-1\n";
      return;
    }
    //se nao, ate agora da certo
    w[p[i]] = i - d[b[p[i]]];
    d[p[i]] = i;
  }

  for (int i = 1; i < n; i++) {
    cout << w[i] << ' ';
  }
  cout << w[n] << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

