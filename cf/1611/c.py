t = int(input())

for _ in range(t):
    n = int(input())
    a = [int(x) for x in input().split()]

    if a[0] == n:
        print(a[0], *a[-1:0:-1])
    elif a[-1] == n:
        print(a[-1], *a[-2::-1])
    else:
        print(-1)
