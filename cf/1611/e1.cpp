#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, k;
int x[200100];
int u, v;
vector<int> g[200100];
int p[200100], l[200100];//, out[200100], l[200100];
int oc[200100];

int aux;
void dfs1(int u) {
  //cerr << u << ' ';
  for (auto v: g[u]) {
    if (p[v] != -1) continue;
    p[v] = u;
    l[v] = l[u] + 1;
    dfs1(v);
  }
}

queue<int> q;
bool dfs2() {
  while (! q.empty()) {
    u = q.front();
    q.pop();
    for (auto v: g[u]) {
      //podo se tiver ocupado ou for pai
      if (oc[v] || v == p[u]) continue;
      //se encontrei caminho, retorno
      if (g[v].size() == 1) return true;
      //se nao, coloco na pilha
      q.push(v);
    }
  }
  //cerr << u << ' ' << in[u] << ' ' << out[u] << '\n';
  /*if (u != 1 and g[u].size() == 1) return true;
  for (auto v: g[u]) {
    //podo se o cara ta ocupado ou eh pai
    if (oc[v] || v <= p[u]) continue;
    //podo se encontrei um caminho
    if (dfs2(v)) return true; 
  }*/
  return false;
}

bool cmp(int a, int b) {
  return l[a] < l[b];
}

void solve() {
  cin >> n >> k;
  for (int i = 0; i <= n; i++) g[i].clear();
  for (int i = 0; i < k; i++) cin >> x[i];
  for (int i = 0; i < n-1; i++) {
    cin >> u >> v;
    g[u].pb(v);
    g[v].pb(u);
  }

  /*for (int i = 0; i <= n; i++) {
    for (auto x: g[i]) cerr << x << ' ';
    cerr << '\n';
  }*/

  memset(p, -1, sizeof(p));
  p[1] = 0;
  l[1] = 0;
  dfs1(1);
  //cerr << '\n';

  memset(oc, 0, sizeof(oc));
  //ordeno amigos por nivel
  sort(x, x+k, cmp);
  //para cada amigo, subo ele ate o nivel que eh o teto da divisao por 2 do nivel original
  for (int i = 0; i < k; i++) {
    aux = l[x[i]];
    while (l[x[i]] > (aux+1)/2 and !oc[x[i]]) {
      oc[x[i]] = 1;
      x[i] = p[x[i]];
    }
    oc[x[i]] = 1;
    //cerr << x[i] << '\n';
  }
  
  while (q.size() != 0) q.pop();
  q.push(1);

  if (dfs2()) cout << "YES\n";
  else cout << "NO\n";

}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

