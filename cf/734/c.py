n, m, k = map(int, input().split())
x, s = map(int, input().split())

a = input().split()
b = input().split()
u = [(x, 0)] + [(int(a[i]), int(b[i])) for i in range(m) if int(b[i]) <= s]

c = input().split()
d = input().split()
v = [(int(c[i]), int(d[i])) for i in range(k) if int(d[i]) <= s]

def busca(val):
    ini = 0
    fim = len(v)
    ans = -1
    while fim > ini:
        meio = (ini+fim)//2
        if v[meio][1] > val:
            fim = meio
            continue
        else:
            ans = meio
            ini = ans+1
    return ans

melhor = x*n
for p in u:
    q = busca(s-p[1])
    #print(p, q)
    if q == -1:
        melhor = min(melhor, p[0]*n)
    else:
        melhor = min(melhor, p[0]*(max(0, n-v[q][0])))

print(melhor)

