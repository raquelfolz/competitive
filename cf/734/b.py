a, b, c, d = map(int, input().split())

x = min(a, c, d)
y = min(b, a-x)

print(256*x + 32*y)
