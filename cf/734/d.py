n = int(input())

x, y = map(int, input().split())

p = [(int(4e9+5), 'P')]*8

for i in range(n):
    c, a, b = input().split()
    a, b = map(int, (a, b))
    if a == x and b < y:
        d = y-b
        if d < p[0][0]:
            p[0] = (d, c)
    if a == x and b > y:
        d = b-y
        if d < p[1][0]:
            p[1] = (d, c)
    if a < x and b == y:
        d = x-a
        if d < p[2][0]:
            p[2] = (d, c)
    if a > x and b == y:
        d = a-x
        if d < p[3][0]:
            p[3] = (d, c)
    if a+b == x+y and a < x:
        d = x-a
        if d < p[4][0]:
            p[4] = (d, c)
    if a+b == x+y and a > x:
        d = a-x
        if d < p[5][0]:
            p[5] = (d, c)
    if a-b == x-y and a < x:
        d = x-a
        if d < p[6][0]:
            p[6] = (d, c)
    if a-b == x-y and a > x:
        d = a-x
        if d < p[7][0]:
            p[7] = (d, c)

check = False
for x in p[:4]:
    if x[1] in "RQ":
        check = True
for x in p[4:]:
    if x[1] in "BQ":
        check = True

print("YES" if check else "NO")

