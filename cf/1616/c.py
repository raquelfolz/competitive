def eq(a, b):
    return abs(a-b) <= 1e-7

t = int(input())

for _ in range(t):
    n = int(input())
    a = [int(x) for x in input().split()]

    if n < 3:
        print(0)
        continue

    ans = n

    for i in range(n-1):
        for j in range(i+1, n):
            x = n

            d = a[j] - a[i]
            e = j - i

            step = d/e
            ini = a[i] - i*step

            #print(i, j, step, ini)

            for k in range(n):
                if eq(a[k], ini+k*step):
                    x -= 1
                #else:
                    #print(k, ini+k*step + a[k])

            if x < ans:
                ans = x

    print(ans)
