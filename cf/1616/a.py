t = int(input())

for _ in range(t):
    n = int(input())
    a = [abs(int(x)) for x in input().split()]

    l = [0]*110
    for x in a:
        l[x] += 1

    r = 0 if l[0] == 0 else 1

    for x in l[1:]:
        if x == 1:
            r += 1
        elif x > 1:
            r += 2

    print(r)
