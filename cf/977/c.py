n, k = map(int, input().split())
a = [int(x) for x in input().split()]

a.sort()

a.append(a[-1]+1)

if k == 0:
    print(1 if a[0] > 1 else -1)
elif a[k] == a[k-1]:
    print(-1)
else:
    print(a[k]-1)
