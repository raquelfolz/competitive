#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;


int n, m;
int v, u;
vector<int> g[200100];
int ans;
bool vis[200100];

bool bfs(int v) {
  vis[v] = true;
  bool aux = g[v].size()==2;
  for (auto x: g[v]) {
    if (vis[x]) continue;
    aux = bfs(x) and aux;
  }
  return aux;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> u >> v;
    g[u].pb(v);
    g[v].pb(u);
  }

  for (int i = 1; i <= n; i++) {
    if (vis[i]) continue;
    if (bfs(i)) ans++;
  }

  cout << ans << '\n';

  return 0;
}

