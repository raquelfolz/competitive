n = int(input())
s = input()

d = dict()

for i in range(n-1):
    if s[i:i+2] in d:
        d[s[i:i+2]] += 1
    else:
        d[s[i:i+2]] = 1

print(max(d, key=lambda x: d.get(x)))
