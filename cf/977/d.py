n = int(input())
a = [int(x) for x in input().split()]

def foo(x):
    p = 0
    while x % 2 == 0:
        p += 1
        x /= 2
    return p

a.sort(key=lambda x:(foo(x), -x))

print(*a)
