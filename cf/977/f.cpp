#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int a[200100];
map<int, int> m;
int fim = 0;
vector<int> ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    m[a[i]] = max(m[a[i]], m[a[i]-1]+1);
    m.erase(a[i]-1);
  }

  for ( auto [k, v] : m) {
    if (v > m[fim]) fim = k;
  }

  for (int i = n-1; i >= 0; i--) {
    if (a[i] == fim) {
      ans.pb(i+1);
      fim--;
    }
  }

  cout << ans.size() << '\n';
  for (int i = ans.size()-1; i > 0; i--) cout << ans[i] << ' ';
  cout << ans[0] << '\n';

  return 0;
}

