t = int(input())

for _ in range(t):
    n, k = map(int, input().split())

    if k > (n+1)//2:
        print(-1)
        continue

    for i in range(n):
        l = ['.']*n
        if i % 2 == 0 and i//2 < k:
            l[i] = 'R'
        print(''.join(l))
