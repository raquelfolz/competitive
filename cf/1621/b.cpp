#include <bits/stdc++.h>

#define INF 1000000007

using namespace std;

int t;
int n;
long long l, r, val;

long long menor[3], maior[3]; // num, val, i
long long ss[4]; //l, r, val, i

long long curp;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int asdf = 0; asdf < t; asdf++) {
    cin >> n;

    menor[0] = menor[1] = maior[1] = ss[0] = ss[2] = INF;
    maior[0] = menor[2] = maior[2] = ss[1] = ss[3] = -1;

    for (int i = 0; i < n; i++) {
      cin >> l >> r >> val;

      // more numbers, or better price
      if (l < menor[0] or (l == menor[0] and val < menor[1])) {
        menor[0] = l;
        menor[1] = val;
        menor[2] = i;
      }
      if (r > maior[0] or (r == maior[0] and val < maior[1])) {
        maior[0] = r;
        maior[1] = val;
        maior[2] = i;
      }
      if (r-l > ss[1]-ss[0] or (r-l == ss[1]-ss[0] and val < ss[2])) {
        ss[0] = l;
        ss[1] = r;
        ss[2] = val;
        ss[3] = i;
      }

      // choose the most numbers, then best price
      if (maior[0]-menor[0] > ss[1]-ss[0])  curp = menor[1] + maior[1];
      else if (maior[0]-menor[0] < ss[1]-ss[0])  curp = ss[2];
      else curp = min(maior[1]+menor[1], ss[2]);

      //cerr << menor[0] << ' ' << menor[1] << ' ' << menor[2] << '\n';
      //cerr << maior[0] << ' ' << maior[1] << ' ' << maior[2] << '\n';
      //cerr << ss[0] << ' ' << ss[1] << ' ' << ss[2] << ' ' << ss[3] << '\n';
      cout << curp << '\n';
    }
  }

  return 0;
}
