#include <bits/stdc++.h>

#define INF 1000000007

using namespace std;

int t;
int n;

int p[10100];

int last, ans;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int asdf = 0; asdf < t; asdf++) {
    cin >> n;

    memset(p, -1, sizeof(p));

    for (int i = 1; i <= n; i++) {
      if (p[i] != -1) continue;

      // para iniciar, precisa saber o valor na posicao i em q
      cout << "? " << i << endl;
      cin >> last;

      // enquanto nao sei o valor do ultimo, saio do laco
      while (p[last] == -1) {

        // pergunto qual o proximo valor do ciclo
        cout << "? " << i << endl;
        cin >> ans;

        // atualizo o p[last] e last
        p[last] = ans;
        last = ans;
      }
    }

    // agora p tem a sequencia correta
    cout << "!";
    for (int i = 1; i <= n; i++) {
      cout << " " << p[i];
    }
    cout << endl;
  }

  return 0;
}
