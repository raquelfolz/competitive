for _ in range(int(input())):
    s = input()
    t = input()
    k = 0
    for i in range(min(len(s), len(t))):
        if s[i] == t[i]:
            k += 1
        else:
            break
    print(len(s) + len(t) - k + (1 if k > 0 else 0) )
