from queue import SimpleQueue as Queue

for _ in range(int(input())):
    n, k = map(int, input().split())
    a = [int(x) for x in input().split()]
    a.sort()
    cnt = 1
    m = 1
    l = k-1
    q = Queue()
    c = 1
    # print(a)
    for i in range(1, n):
        if a[i] == a[i-1]:
            cnt += 1
            c += 1
            m = max(m, cnt)
        elif l > 0 and a[i] == a[i-1]+1:
            q.put(c)
            # print("put", c)
            c = 1
            cnt += 1
            m = max(m, cnt)
            l -= 1
        elif a[i] == a[i-1]+1:
            q.put(c)
            # print("put", c)
            b = q.get()
            # print("get", b)
            cnt -= b-1
            c = 1
        else:
            q = Queue()
            c = 1
            cnt = 1
            l = k-1
        # print(i, cnt)
    print(m)
