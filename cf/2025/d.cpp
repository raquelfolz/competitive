#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
vector<int> in[5010]; //testes depois do i-esimo ponto (i>0)
vector<int> st[5010];

int dp[5010][5010]; //quantidade de testes que consigo passar tendo atribuido i pontos, sendo j pra int

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  int p=0, aux;
  for (int i = 0; i < n; i++) {
    cin >> aux;
    if (aux == 0) p++;
    else if (aux > 0) in[p].pb(aux);
    else st[p].pb(-aux);
  }
  for (int i = 0; i <= m; i++) {
    sort(in[i].begin(), in[i].end());
    sort(st[i].begin(), st[i].end());
  }
  
  int a, b;
  for (int i = 1; i <= m; i++) {
    //nao indo nenhum pra int, so consigo passar os de forca
    dp[i][0] = dp[i-1][0] + (int) (upper_bound(st[i].begin(), st[i].end(), i)-st[i].begin());
    //nao indo nenhum pra forca, so consigo passar os de in
    dp[i][i] = dp[i-1][i-1] + (int) (upper_bound(in[i].begin(), in[i].end(), i)-in[i].begin());
    for (int j = 1; j < i; j++) {
      //se eu joguei o ponto novo pra forca
      a = dp[i-1][j] + (int) (upper_bound(st[i].begin(), st[i].end(), i-j)-st[i].begin());
      a += (int) (upper_bound(in[i].begin(), in[i].end(), j)-in[i].begin());
      //se eu joguei o ponto novo pra int
      b = dp[i-1][j-1] + (int) (upper_bound(in[i].begin(), in[i].end(), j)-in[i].begin());
      b += (upper_bound(st[i].begin(), st[i].end(), i-j)-st[i].begin());
      
      dp[i][j] = max(a, b);
    }
  }

  int ans = 0;
  for (int i = 0; i <= m; i++) ans = max(ans, dp[m][i]);
  cout << ans << '\n';

  return 0;
}

