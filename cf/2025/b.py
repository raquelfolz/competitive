INF = int(1e9+7)

def fexp(x):
    if x == 0:
        return 1
    if x == 1:
        return 2
    y = fexp(x//2)
    y = (y*y) %INF
    if x % 2 == 1:
        y = (y*2) % INF
    return y


t = int(input())
a = [int(x) for x in input().split()]
b = [int(x) for x in input().split()]

for i in range(t):
    print(fexp(b[i] % a[i]))
