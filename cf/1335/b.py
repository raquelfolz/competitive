for _ in range(int(input())):
    n, a, b = map(int, input().split())
    s = ''
    for i in range(n):
        if i%a < b:
            s += chr(97+i%a)
        else:
            s += chr(97+b-1)
    print(s)
