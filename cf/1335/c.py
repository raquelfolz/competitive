def unique(l):
    cnt = 1
    for i in range(1, len(l)):
        if l[i] != l[i-1]:
            cnt += 1
    return cnt

def diff(l):
    cnt = 1
    m = 1
    for i in range(1, len(l)):
        if l[i] == l[i-1]:
            cnt += 1
            m = max(m, cnt)
        else:
            cnt = 1
    return m


for _ in range(int(input())):
    n = int(input())
    a = [int(x) for x in input().split()]
    a.sort()
    u = unique(a)
    d = diff(a)

    if u == d:
        print(u-1)
    else:
        print(min(u, d))
