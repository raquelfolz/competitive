from math import *

for _ in range(int(input())):
    n = int(input())
    ans = 1
    while n >= 4:
        n //= 4
        ans *= 2
    print(ans)
