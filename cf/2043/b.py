from math import *

fat = [1]
for i in range(1, 7):
    fat.append(fat[-1]*i)

for _ in range(int(input())):
    n, d = map(int, input().split())
    l = [1]
    if n >= 3 or d % 3 == 0:
        l.append(3)
    if d == 5:
        l.append(5)
    if n >= 3 or int(str(d)*n)%7 == 0:
        l.append(7)
    if n >= 6 or int(str(d)*fat[n])%9 == 0:
        l.append(9)
    print(*l)
    
