#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
ll a[200100];
int pos;
ll lmin, rmin;
ll lmax, rmax;
ll acc;
ll ps[200100];
ll x, y;
set<ll> s;

void solve() {
  cin >> n;
  pos = 0;
  for (int i = 1; i <= n; i++) {
    cin >> a[i];
    if (abs(a[i]) != 1) pos = i;
    ps[i] = ps[i-1] + a[i];
  }
  /*for (int i = 1; i <= n; i++) cerr << ps[i] << ' ';*/
  /*cerr << '\n';*/
  lmin = 0; lmax = 0;
  x = 0; y = 0;
  s.clear();
  for (int i = 1; i < pos; i++) {
    x = min(x, ps[i] - lmax);
    y = max(y, ps[i] - lmin);
    lmin = min(lmin, ps[i]);
    lmax = max(lmax, ps[i]);
  }
  /*cerr << x << ' ' << y << '\n';*/
  for (ll i = x; i <= y; i++) s.insert(i);
  x = 0; y = 0;
  rmin = ps[pos]; rmax = ps[pos];
  for (int i = pos+1; i <= n; i++) {
    x = min(x, ps[i] - rmax);
    y = max(y, ps[i] - rmin);
    rmin = min(rmin, ps[i]);
    rmax = max(rmax, ps[i]);
  }
  /*cerr << x << ' ' << y << '\n';*/
  for (ll i = x; i <= y; i++) s.insert(i);
  lmin = a[pos]; lmax = a[pos]; acc = a[pos];
  for (int i = pos-1; i > 0; i--) {
    acc += a[i];
    lmin = min(lmin, acc);
    lmax = max(lmax, acc);
  }
  rmin = a[pos]; rmax = a[pos]; acc = a[pos];
  for (int i = pos+1; i <= n; i++) {
    acc += a[i];
    rmin = min(rmin, acc);
    rmax = max(rmax, acc);
  }
  
  /*cerr << lmin << ' ' << rmax << "   " << rmin << ' ' << lmax << '\n';*/
  /*cerr << lmin - (a[pos]-rmin) << " " << lmax + (rmax-a[pos]) << '\n';*/
  for (ll i = lmin - (a[pos]-rmin); i <= lmax + (rmax-a[pos]); i++) s.insert(i);
  /*for (ll i = lmin; i <= rmax; i++) s.insert(i);*/
  /*for (ll i = rmin; i <= lmax; i++) s.insert(i);*/
  cout << s.size() << '\n';
  for (auto k: s) cout << k << ' ';
  cout << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  int t;
  cin >> t;
  while(t--) solve();

  return 0;
}

