n = int(input())

for i in range(10):
    if (4*i > n):
        print(-1)
        break
    if (n - 4*i)%7 == 0:
        print("4"*i + "7"* ((n-4*i)//7))
        break
else:
    print(-1)
