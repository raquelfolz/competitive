#include <bits/stdc++.h>

using namespace std;


int n;
string s;
deque<char> d;
vector<int> op;
int li, ri, k;
int cnt[2];

void solve() {
  cin >> n;
  cin >> s;

  d.clear();
  op.clear();
  
  cnt[0] = 0; cnt[1] = 0;
  for (auto c: s) {
    d.push_back(c);
    cnt[c-'0']++;
  }
  if (n % 2 == 1 or cnt[0] != cnt[1]) {
    cout << -1 << '\n';
    return;
  }
  
  li = 0;
  ri = n;
  k = 0;

  while (not d.empty()) {
    if (d.front() != d.back()) {
      li++; ri--;
      d.pop_front();
      d.pop_back();
      continue;
    }
    if (k > 300) {
      cout << -1 << '\n';
      return;
    }
    k++;
    if (d.front() == '1') {
      op.push_back(li);
      d.push_front('1');
      d.pop_back();
      li++; ri++;
    }
    else if (d.back() == '0') {
      op.push_back(ri);
      d.pop_front();
      d.push_back('0');
      li++; ri++;
    }
  }

  cout << k << '\n';
  if (k > 0) cout << op[0];
  for (int i = 1; i < k; i++) cout << ' ' << op[i];
  cout << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int t;
  cin >> t;
  for (int i = 0; i < t; i++) solve();
  return 0;
}
