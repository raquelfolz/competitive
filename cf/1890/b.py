t = int(input())

for _ in range(t):
    n, m = map(int, input().split())
    s = input()
    t = input()
    
    ok = True

    if t[0] != t[-1]:
        ok = False
    
    for i in range(m-1):
        if t[i] == t[i+1]:
            ok = False
            break
    
    for i in range(n-1):
        if s[i] == s[i+1] and (not ok or s[i] == t[0]):
            print("No")
            break
    else:
        print("Yes")
