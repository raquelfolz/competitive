#include<bits/stdc++.h>

#define f first
#define s second

using namespace std;

int n, m, k;
int l, r;
int cnt, dia, aux;
int melhor;
int qsecas;
pair<int, int> chuva[200100];
pair<int, pair<int, int>> c[200100]; //cidades quase secas por dia
                          //f -> primeira quase seca
                          //s.f -> ultima quase seca
                          //s.s -> numero de quase quase secas
set<pair<int, int>> s; //cidade para terminar a chuva
                       //dia da chuva

bool cmp (pair<int, pair<int, int>> a, pair<int, pair<int, int>> b) {
  if (a.f == b.f) {
    if (b.s.f == a.s.f) return a.s.s < b.s.s;
    return a.s.f > b.s.f;
  }
  return a.f < b.f;
}

bool cmp2 (pair<int, pair<int, int>> a, pair<int, pair<int, int>> b) {
  return a.s.s > b.s.s;
}


void solve() {
  cin >> n >> m >> k;

  s.clear();
  for (int i = 0; i < m; i ++) {
    c[i].f = -1;
    c[i].s.f = -1;
    c[i].s.s = 0;
    cin >> l >> r;
    chuva[i] = {l, r};
  }

  sort(chuva, chuva+m);

  cnt = chuva[0].f-1;
  dia = 0;
  qsecas = 0;
  for (int i = chuva[0].f; i <= n; i++) {
    //tiro chuvas velhas da lista
    while (not s.empty() and (*s.begin()).f < i) {
      s.erase(s.begin());
    }
    //e adiciono chuvas novas
    while (dia < m and chuva[dia].f == i) {
      //cerr << dia << ' ' << m << ' ' << chuva[dia].f << ' '<< i << '\n';
      s.insert({chuva[dia].s, dia});
      dia++;
    }
    //cerr << "d" << s.size() << "\n";
    //cerr << dia << ' ' << m << ' ' << chuva[dia].f << ' '<< i << '\n';

    if (s.size() == 0) cnt++;
    else if (s.size() == 1) {
      aux = (*s.begin()).s; //dia daquela unica chuva
      c[aux].s.s++;
    }
    else if (s.size() == 2) {
      for (auto x: s) {
        //cerr << "a" << x.s << ' ';
        //se nao tiver nada quase seco, configuro primeira cidade
        if (c[x.s].f == -1) {
          c[x.s].f = qsecas;
        }
        //ultima entra anyway
        c[x.s].s.f = qsecas;
      }
      //cerr << '\n';
      qsecas++;
    }
  }

  //cerr << "caso\n";
  //cerr << qsecas << '\n';
  //cerr << cnt << '\n';
  //for (int i = 0; i <= m; i++) cerr << c[i].f << ' ' << c[i].s.f << ' ' << c[i].s.s << '\n';
  
  //agora para processar :D
  //primeiro as que tem overlap... -> quase secas secam
  sort(c, c+m, cmp);
  dia = 0;
  melhor = 0;
  while (dia < m and c[dia].f == -1) dia++;
  for (int i = dia + 1; i < m; i++) {
    if (c[i].f > c[dia].s.f) {
      dia = i;
      continue;
    }
    aux = min(c[i].s.f, c[dia].s.f) - c[i].f + 1 + c[i].s.s + c[dia].s.s;
    melhor = max(aux, melhor);
    if (c[i].s.f > c[dia].s.f) dia = i;
  }
  //agora pares sem overlap -> duas melhores com quase quase secas
  sort(c, c+m, cmp2);
  melhor = max(c[0].s.s + c[1].s.s, melhor);

  cout << melhor + cnt<< '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int t;
  cin >> t;
  for (int i = 0; i < t; i++) solve();
  return 0;
}
