t = int(input())

for _ in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    d = {}
    for x in l:
        if not x in d:
            d[x] = 0
        else:
            d[x] += 1

    if len(d) > 2:
        print("No")
        continue
    if len(d) == 1:
        print("Yes")
        continue
    s = []
    for key, value in d.items():
        s.append(value)

    if abs(s[0] - s[1]) < 2:
        print("Yes")
    else:
        print("No")
