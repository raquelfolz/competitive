n = int(input())

m = {}

for i in range(n):
    s = input()
    if s in m:
        m[s] += 1
        print(s+str(m[s]))
    else:
        m[s] = 0
        print("OK")
