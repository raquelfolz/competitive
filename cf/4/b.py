d, s = map(int, input().split())
ans = []
t = []

for i in range(d):
    a, b = map(int, input().split())
    if a > s:
        print("NO")
        quit()
    ans.append(a)
    s -= a
    t.append(b-a)

if s > sum(t):
    print("NO")
    quit()

for i in range(d):
    x = min(s, t[i])
    ans[i] += x
    s -= x

print("YES")
print(*ans)
