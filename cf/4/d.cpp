#include<bits/stdc++.h>

using namespace std;

#define f first
#define s second
#define pb push_back

typedef pair<int, int> pii;

int n, w, h;
pii e[5010];
int s[5010];
pii dp[5010];

int len[5010];
int best;
vector<int> ans;

bool cmp(int a, int b) {
  if (e[a].f == e[b].f) return e[a].s < e[b].s;
  return e[a].f < e[b].f;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> w >> h;
  for (int i = 1; i <= n; i++) {
    cin >> e[i].f >> e[i].s;
    s[i] = i;
  }

  sort(s+1, s+n+1, cmp);

  for (int i = 1; i <= n; i++) {
    if (w >= e[s[i]].f or h >= e[s[i]].s) continue;
    for (int j = 1; j <= i; j++) {
      if (e[s[j]].f >= e[s[i]].f or e[s[j]].s >= e[s[i]].s) continue;
      if (dp[j].f > dp[i].f) {
        dp[i].f = dp[j].f;
        dp[i].s = j;
      }
    }
    dp[i].f++;
    len[i] = len[dp[i].s] + 1;
    if (len[i] > len[best]) best = i;
  }

  cout << len[best] << '\n';
  if (len[best] == 0) return 0;

  for (int i = best; i != 0; i = dp[i].s) ans.pb(s[i]);
  while (ans.size() > 0) {
    cout << ans.back() << ' ';
    ans.pop_back();
  }
  cout << '\n';

  return 0;

}
