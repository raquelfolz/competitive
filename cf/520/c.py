INF = 10**9+7

n = int(input())
s = input()
a = [0]*4

for c in s:
    if c == 'A':
        a[0] += 1
    if c == 'C':
        a[1] += 1
    if c == 'G':
        a[2] += 1
    if c == 'T':
        a[3] += 1

m = max(a)
k = a.count(m)

def fe(b, exp):
    if exp == 0:
        return 1
    if exp == 1:
        return b % INF
    x = fe(b, exp//2)
    if exp % 2 == 1:
        return (x * x * b) % INF
    return (x*x) % INF

#n posicoes, cada uma pode ser k caracteres
print(fe(k, n))
