from queue import Queue

n, m = map(int, input().split())

l = [-1]*(3*10**4)
l[n] = 0
l[0] = 10**5

q = Queue()
q.put(n)

while not q.empty():
    v = q.get()
    if v == m:
        break
    if l[v-1] == -1:
        l[v-1] = l[v]+1
        q.put(v-1)
    if v < 10**4 and l[v*2] == -1:
        l[v*2] = l[v]+1
        q.put(v*2)

print(l[m])
