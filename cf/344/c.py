a, b = map(int, input().split())
cnt = 0

if a > b:
    a, b = b, a
while a > 0:
    cnt += b // a
    a, b = b%a, a 

print(cnt)
