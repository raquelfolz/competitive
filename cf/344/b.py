a, b, c = map(int, input().split())

z = (a+c-b)//2
x = a - z
y = c - z

if (a+b+c)%2 == 0 and min(x, y, z) >= 0 and [x, y, z].count(0) < 2:
    print(x, y, z)
else:
    print("Impossible")
