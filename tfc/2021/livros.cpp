#include <bits/stdc++.h>

using namespace std;

struct ponto {
  long long t;
  bool f=false;
};

bool cmp(ponto a, ponto b) {
  if (a.t == b.t) return a.f;
  return a.t < b.t;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n;
  long long x, y;
  ponto v[200100];

  int cnt, m;

  cin >> n;

  for (int i = 0; i < n; i++) {
    cin >> x >> y;
    v[2*i].t = x;
    v[2*i+1].t = x+y;
    v[2*i+1].f = true;
  }

  sort(v, v+2*n, cmp);

  cnt = 0; m = 0;
  for (int i = 0; i < 2*n; i++) {
    if (v[i].f) cnt--;
    else {
      cnt++;
      if (cnt > m) m = cnt;
    }
  }

  cout << m << '\n';

  return 0;
}
