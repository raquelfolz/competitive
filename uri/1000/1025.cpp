//https://www.urionlinejudge.com.br/judge/pt/problems/view/1025

#include <bits/stdc++.h>

using namespace std;

int busca(int val, int n, int v[]) {
  int ini, fim, meio;
  int melhor = -1;

  ini = 0;
  fim = n-1;

  while (ini <= fim) {
    meio = (ini + fim)/2;
    if (v[meio] == val) melhor = meio;
    if (v[meio] < val) ini = meio + 1;
    else fim = meio - 1;
  }

  return melhor;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n, q;
  int v[10100];
  int pos, con;
  int caso = 0;

  while (true) {
    cin >> n >> q;
    if (n == 0 and q == 0) break;

    caso++;
    cout << "CASE# " << caso << ":\n";

    for (int i = 0; i < n; i++) cin >> v[i];
    sort(v, v+n);

    for (int i = 0; i < q; i++) {
      cin >> con;
      pos = busca(con, n, v);
      if (pos == -1) cout << con << " not found\n";
      else cout << con << " found at " << pos+1 << '\n';
    }
  }

  return 0;

}
