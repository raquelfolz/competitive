#include <bits/stdc++.h>

using namespace std;

unordered_set<int> g[10010]; // unordered set exclui elementos repetidos e
                             // espera tempo constante nas operacoes
bool vis[10010]; // como nao tem ciclo, nao preciso me preocupar com voltar
                 // para um vertice ja visitado que nao seja o "pai"

int dfs(int vertice) { //retorna quantidade de arestas
  //se vertice foi visitado, eh pai do meu vertice e eu ignoro
  if (vis[vertice]) return -1;

  int val = 0;
  int aux;
  //marco que vertice entrou na pilha
  vis[vertice] = true;

  for (auto x: g[vertice]) { //para cada vizinho do vertice, calculo os valores
    aux = dfs(x);
    if (aux == -1) continue; //se era o pai, eu ignoro
    val++; //uma aresta do atual para o filho
    val += aux; //todas as arestas na sub-arvore do filho
    val++; //uma aresta do filho para o atual
  }
  return val;
}

int t;
int n;
int v, a;
int x, y;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {

    for (int i = 0; i < 10010; i++) g[i].clear(); //limpa o grafo
    memset(vis, 0, sizeof(vis));

    cin >> n;
    cin >> v >> a;
    for (int i = 0; i < a; i++) {
      cin >> x >> y;
      g[x].insert(y);
      g[y].insert(x);
    }

    cout << dfs(n) << '\n';
  
  }

  return 0;

}

