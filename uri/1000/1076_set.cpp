#include <bits/stdc++.h>

using namespace std;

int t;
int n;
int v, a;
set<pair<int,int>> s; // exclui elementos repetidos,
                      // tempo log(tamanho) nas operacoes
                      //
                      // para usar unordered_set (tabela hash) teria
                      // que criar funcao de hash para pair ou usar
                      // operacoes bit a bit para usar long long como
                      // par de inteiros
int x, y;


int main() {
  
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste=0; teste < t; teste++) {
    s.clear(); //reinicia grafo
    
    cin >> n;
    cin >> v >> a;

    for (int i = 0; i < a; i++) {
      cin >> x >> y;
      if (x > y) swap(x, y); //garante que x eh menor ou igual a y
      s.insert({x, y});
    }

    //cada aresta possivel aparece exatamente uma vez em s
    //
    //para desenhar o labirinto, cada aresta deve
    //ser percorrida exatamente 2 vezes
    
    cout << 2 * s.size() << '\n'; 
  }

  return 0;
}
