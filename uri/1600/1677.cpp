//resolvida no contest do dia 2 da escola de verao 2024

#include <bits/stdc++.h>

using namespace std;

#define pb push_back

int n, m;
vector<int> g[5100], r[5100];
set <int> cond[5100], rcond[5100];

bool vis[5100];
int comp[5100];
int qtd;
stack<int> out;

void dfs(int v) {
  if (vis[v]) return;
  vis[v] = true;
  for (auto x: g[v]) dfs(x);
  out.push(v);
}

bool kosaraju(int v) {
  if (comp[v] != -1) return true;
  comp[v] = qtd;
  for (auto x: r[v]) kosaraju(x);
  return false;
}

int a, b;
unordered_set<int> sink;
vector<int> ans;

void solve() {

  for (int i = 0; i < 5010; i++) {
    g[i].clear();
    r[i].clear();
    cond[i].clear();
    rcond[i].clear();
  }

  memset(comp, -1, sizeof(comp));
  memset(vis, 0, sizeof(vis));
  qtd = 0;
  sink.clear();
  ans.clear();

  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    r[b].pb(a);
  }

  for (int i = 1; i <= n; i++) dfs(i);
  
  while (! out.empty()) {
    if (! kosaraju(out.top())) qtd++;
    out.pop();
  }

  for (int i = 1; i <= n; i++) {
    for (auto x: g[i]) {
      if (comp[x] != comp[i]) {
        cond[comp[i]].insert(comp[x]);
        rcond[comp[x]].insert(comp[i]);
      }
    }
  }

  for (int i = 0; i < qtd; i++) {
    if (cond[i].size() == 0) sink.insert(i);
  }

  for (int i = 1; i <= n; i++) {
    if (sink.count(comp[i]) == 1) ans.pb(i);
  }

  for (int i = 0; i < ans.size()-1; i++) cout << ans[i] << ' ';
  cout << ans[ans.size()-1] << '\n';

}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);
  
  while (true) {
    cin >> n;
    if (n == 0) return 0;
    cin >> m;
    solve();
  }
  
  return 0;

}
