#include <bits/stdc++.h>

using namespace std;

unordered_set<int> g[10010]; // unordered set exclui elementos repetidos e
                             // espera tempo constante nas operacoes
int vis[10010];

bool dfs(int vertice) { //retorna verdadeiro se encontrar ciclo
  //se vertice ja foi processado completamente, eu sei que nao esta em ciclo
  if (vis[vertice] == 2) return false;
  //se vertice ainda esta em 1, encontrei um ciclo
  if (vis[vertice] == 1) return true;

  //marco que vertice entrou na pilha
  vis[vertice] = 1;

  for (auto x: g[vertice]) { //para cada vizinho do vertice, busco um ciclo
    if (dfs(x)) return true; //se um dos vizinhos retornar ciclo,
                             //o grafo todo tem ciclo
  }
  //se nenhum dos vizinhos retornou um ciclo, nao tem ciclo com x
  vis[vertice] = 2;
  return false;
}

int t;
int n, m;
int a, b;
bool ciclo;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 0; teste < t; teste++) {

    for (int i = 0; i < 10010; i++) g[i].clear(); //limpa o grafo
    memset(vis, 0, sizeof(vis));

    cin >> n >> m;
    for (int i = 0; i < m; i++) {
      cin >> a >> b;
      g[a].insert(b); //a depende de b
    }
  
    ciclo = false;
    for (int i = 1; i <= n; i++) { //vertices foram inseridos indexados em 1
      if (dfs(i)) ciclo = true;
    }

    if (ciclo) cout << "SIM\n";
    else cout << "NAO\n";
  }

  return 0;

}
