//https://www.urionlinejudge.com.br/judge/pt/problems/view/2973

#include <bits/stdc++.h>

using namespace std;

bool funciona(long long tempo, int n, int c, int t, int v[]) {
  long long pip = t * tempo;
  int aux=0;
  c--;
  for (int i = 0; i < n; i++) {
    if (v[i] > pip) return false;
    if (aux + v[i] <= pip) {
      aux += v[i];
    } else {
      c--;
      aux = v[i];
    }
  }
  if (c < 0) return false;
  else return true;
}

int busca(int n, int c, int t, int v[]) {
  int ini, fim, meio;
  int melhor = -1;
  bool ok;

  ini = 1;
  fim = 1000000000;

  while (ini <= fim) {
    meio = (ini + fim)/2;
    //cerr << ini << " " << meio << " " << fim << '\n';
    ok = funciona(meio, n, c, t, v);
    if (ok) {
      melhor = meio;
      fim = meio - 1;
    }
    else ini = meio + 1;
  }

  return melhor;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n, c, t;
  int p[100100];
  int ans;

  cin >> n >> c >> t;
  for (int i = 0; i < n; i++) cin >> p[i];

  ans = busca(n, c, t, p);

  cout << ans << '\n';

  //cerr << funciona(650,n,c,t,p) << '\n';

  return 0;

}
