//https://olimpiada.ic.unicamp.br/pratique/pu/2014/f1/carteiro/
//https://olimpiada.ic.unicamp.br/pratique/pu/2014/f1/carteiro/

#include <bits/stdc++.h>

using namespace std;

int busca(int val, int n, int v[]) {
  int ini, fim, meio;

  ini = 0;
  fim = n-1;

  while (ini <= fim) {
    meio = (ini + fim)/2;
    if (v[meio] == val) return meio;
    if (v[meio] < val) ini = meio + 1;
    else fim = meio - 1;
  }

  return -1;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n, m;
  int casas[45010];
  int pos = 0;
  int carta, prox;
  int cnt = 0;

  cin >> n >> m;
  for (int i = 0; i < n; i++) cin >> casas[i];

  for (int i = 0; i < m; i++) {
    cin >> carta;
    prox = busca(carta, n, casas);
    cnt += abs(prox - pos);
    pos = prox;
  }

  cout << cnt << '\n';

  return 0;

}
