#include <bits/stdc++.h>

using namespace std;

bool cmp(int a, int b) {
  return a > b;
}

int n;
string s[1010]; //matriz da entrada
int casa[1010]; //casa de cada um
vector<int> c; //quantidade de pessoas por casa
bool ok;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  memset(casa, -1, sizeof(casa));
  ok = true;

  cin >> n;
  for (int i = 0; i < n; i++) cin >> s[i];

  for (int i = 0; i < n; i++) {
    // para cada pessoa, se nao tiver uma casa assumo que entra em casa nova
    // a casa daquela pessoa vai ser formada por todas as vizinhas a ela
    //
    // se tiver algum conflito (vizinho que ja tem casa) entao
    // tenho certeza de que informacao eh inconsistente
    if (casa[i] != -1) continue;
    casa[i] = c.size();
    c.push_back(1);
    for (int j = 0; j < n; j++) {
      if (s[i][j] == 'S' and i != j) {
        if (casa[j] != -1) {
          ok = false;
          goto end; // nao adianta fazer o resto das verificacoes;
        }
        casa[j] = casa[i]; // atribuo casa de j
        c[casa[j]]++; // e aumento a quantidade de pessoas naquela casa
      }
    }
  }

  // agora sei todas as casas e quantas pessoas existem nelas
  // mas ainda preciso verificar se a informacao eh consistente
  
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      // para cada par de pessoas, verifico se a info de casa condiz
      // com o relacionamento daquelas pessoas
      if (s[i][j] == 'S' and casa[i] == casa[j]) continue;
      if (s[i][j] != 'S' and casa[i] != casa[j]) continue;
      // se nao entrou em nenhuma das condicoes anteriores, entao
      // info eh inconsistente
      ok = false;
      goto end; // nao adianta fazer o resto das verificacoes;
    }
  }

  sort(c.begin(), c.end(), cmp);

  end:

  if (ok) {
    cout << c.size() << '\n';
    for (int i = 0; i < c.size()-1; i++) cout << c[i] << ' ';
    cout << c[c.size()-1] << '\n';
  }
  else {
    cout << "-1\n";
  }

  return 0;

}
