#include <bits/stdc++.h>

using namespace std;

bool vis[1010];
string s[1010];
int n;

void dfs(int vertice, vector<int> &v) {
  if (vis[vertice]) return; //vertice ja esta em uma componente
  vis[vertice] = true;
  v.push_back(vertice);
  for (int i = 0; i < n; i++) {
    //para todo vizinho da minha casa, faco busca a partir dele tambem;
    if (s[vertice][i] == 'S') dfs(i, v);
  }
}
// funcao auxiliar que cria a componente de um vertice
vector<int> componente(int vertice) {
  vector<int> v;
  dfs(vertice, v);
  return v;
}

bool cmp(vector<int> a, vector<int> b) { //para ordencao
  return a.size() > b.size();
}

bool verifica(vector<int> v) { //funcao para verificar se uma componente
                               //conexa eh vailida
  for (int i = 0; i < v.size(); i++) {
    for (int j = i+1; j < v.size(); j++) {
      //para todo par de vertices, verifico se realmente sao vizinhos
      if (s[v[i]][v[j]] != 'S') return false;
    }
  }
  //se chegou ate aqui, nao encontrou nenhum par invalido
  return true;
}

vector<vector<int>> componentes;
bool ok;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) cin >> s[i];
  
  for (int i = 0; i < n; i++) {
    //todo vertice foi visitado sse ja esta em uma componente
    if (vis[i]) continue;
    componentes.push_back(componente(i));
  }

  sort(componentes.begin(), componentes.end(), cmp);
  
  ok = true;
  for (auto c: componentes) {
    //para cada componente, verifico se eh valida
    if (not verifica(c)) ok = false;
  }

  if (ok) {
    cout << componentes.size() << '\n';
    for (int i = 0; i < componentes.size()-1; i++)
      cout << componentes[i].size() << ' ';
    cout << componentes[componentes.size()-1].size() << '\n';
  }
  else {
    cout << "-1\n";
  }
}
