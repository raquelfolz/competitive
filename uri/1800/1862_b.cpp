#include <bits/stdc++.h>

using namespace std;

bool cmp(int a, int b) {
  return a > b;
}

int n;
string s[1010];
bool vis[1010];
vector<int> c;
bool ok;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) cin >> s[i];
  ok = true;
  
  //quaisquer duas pessoas de uma mesma casa tem que ser descritas
  //pela mesma string
  //(porque os vizinhos daqueles vertices sao os mesmos)
  //--->podemos verificar para cada pessoa se tem a mesma string
  //    que os vizinhos dela
  
  for (int i = 0; i < n; i++) {
    if (vis[i]) continue; //se ja foi visitada, ja foi verificada

    vis[i] = true;
    c.push_back(0);

    for (int j = 0; j < n; j++) {
      if (s[i][j] != 'S') continue; //se nao sao vizinhos, nao importa
      vis[j] = true; //j esta na mesma componente que i
      c[c.size()-1]++;
      if (s[i] != s[j]) ok = false; //deveriam ser iguais se sao da mesma casa
    }
  }

  sort(c.begin(), c.end(), cmp);

  if (ok) {
    cout << c.size() << '\n';
    for (int i = 0; i < c.size()-1; i++) cout << c[i] << ' ';
    cout << c[c.size()-1] << '\n';
  }
  else {
    cout << "-1\n";
  }



  return 0;
}
