#include <bits/stdc++.h>

using namespace std;

struct deus {
  string nome;
  int p, k, m;
};

// true se a for melhor
// false se b for melhor
bool cmp(deus a, deus b) {
  if (a.p != b.p) return a.p > b.p;
  if (a.k != b.k) return a.k > b.k;
  if (a.m != b.m) return a.m < b.m;
  return a.nome < b.nome;
}

int main() {

  int n;
  deus v[110];
  int melhor;

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;

  for (int i = 0; i < n; i++) {
    cin >> v[i].nome >> v[i].p >> v[i].k >> v[i].m;
  }

  melhor = 0;

  for (int i = 1; i < n; i++) {
    // se o atual for melhor que meu melhor
    // mudo melhor para o atual
    if (cmp(v[i], v[melhor])) melhor = i;
  }

  cout << v[melhor].nome << '\n';

  return 0;
}
