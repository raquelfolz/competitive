#include<bits/stdc++.h>

using namespace std;

int m, n;
int x, y, z;
vector<pair<int, int>> g[200100];
bool vis[200100];
priority_queue<pair<int,int>> pq;
pair<int, int> p;

long long pesot, peso;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  while (true) {

    cin >> m >> n;
    if (m == 0) break;
    
    memset(vis, 0, sizeof(vis));
    for(int i = 0; i < n; i++) g[i].clear();
    pesot = peso = 0;

    for (int i = 0; i < n; i++) {
      cin >> x >> y >> z;
      g[x].push_back({-z, y});
      g[y].push_back({-z, x});
      pesot += z;
    }
    
    //spg, suponho que a arvore comeca no vertice 0
    vis[0] = true;
    for (auto k: g[0]) pq.push(k);

    while (not pq.empty()) {
      p = pq.top();
      pq.pop();
      if (vis[p.second]) continue; //se vertice esta na arvore, continuo
      
      //vertice nao esta na arvore, adiciono e colo vizinhos na fila
      
      x = p.second;
      vis[x] = true;
      for (auto k: g[x]) pq.push(k);

      z = -p.first;
      peso += z;

      
    }


    cout << pesot - peso << '\n';

  }

  return 0;

}
