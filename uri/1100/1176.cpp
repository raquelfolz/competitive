#include <bits/stdc++.h>

using namespace std;

int main() {

  int t, n;
  vector <long long> fib;
  int x;

  ios::sync_with_stdio(0);
  cin.tie(0);

  // primeiors dois valores de fibonacci
  fib.push_back(0);
  fib.push_back(1);

  cin >> t;

  for (int k = 0; k < t; k++) {

    cin >> n;

    // se o tamanho do vector for maior que n
    // então o número já foi computado
    // caso contrário, adiciono mais números no vector

    while (fib.size() <= n) {
      x = fib.size();
      fib.push_back(fib[x - 1] + fib[x - 2]);
    }

    cout << "Fib(" << n << ") = " << fib[n] << '\n';
  }

  return 0;

}
