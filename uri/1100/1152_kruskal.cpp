#include<bits/stdc++.h>

using namespace std;

struct edge {
  int x, y, z;
};

bool cmp(edge a, edge b) {
  return a.z < b.z;
}

int comp[200100], tam[200100];

int find(int x) {
  if (x == comp[x]) return x;
  return comp[x] = find(comp[x]);
}

bool same(int a, int b) {
  return find(a) == find(b);
}

void unite(int a, int b) {
  a = find(a);
  b = find(b);
  if (tam[a] < tam[b]) swap(a, b);
  tam[a] += tam[b];
  comp[b] = a;
}

int m, n;
int x, y, z;
vector<edge> g;

long long pesot;
long long peso;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  while (true) {

    cin >> m >> n;
    if (m == 0) break;

    g.clear();
    pesot = peso = 0;

    for (int i = 0; i < n; i++) {
      cin >> x >> y >> z;
      g.push_back({x, y, z});
      pesot += z;
    }

    sort(g.begin(), g.end(), cmp);

    for (int i = 0; i <= m; i++) { //tudo indexado em 0
      comp[i] = i;
      tam[i] = 1;
    }

    for (auto e: g) {
      if (same(e.x, e.y)) continue;
      peso += e.z;
      unite(e.x, e.y);
    }

    cout << pesot - peso << '\n';

  }

  return 0;

}
