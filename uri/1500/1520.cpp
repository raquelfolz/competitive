#include <bits/stdc++.h>

using namespace std;

int n;
int x, y;
int num;

int v[110];
int s, b;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  while (cin >> n) {

    memset(v, 0, sizeof(v));

    for (int i = 0; i < n; i++) {
      cin >> x >> y;
      for (int j = x; j <= y; j++) v[j]++;
    }

    cin >> num;

    s = 0;
    for (int i = 0; i < num; i++) s += v[i];

    if (v[num] == 0) cout << num << " not found\n";
    else cout << num << " found from " << s << " to " << s + v[num] - 1 << '\n';

  }

  return 0;

}
