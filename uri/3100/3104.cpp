#include <bits/stdc++.h>

using namespace std;

string a;
long long r, b;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> a >> b;

  for (auto c: a) {
    r = 10*r + (c - '0');
    r %= b;
  }

  cout << r << '\n';

  return 0;
}

