#include <bits/stdc++.h>

using namespace std;

#define pb push_back

int n;
int g[1010][1010];

set<int> p[2]; //as piscinas
bool vis[1010];

//faco dfs alternando as piscinas
//(se for bipartido, cada piscina ninguem vai ter aresta em comum dps)
void dfs(int v, int k) {
  for (int i = 0; i < n; i++) {
    if (! vis[i] and ! g[v][i]) {
      vis[i] = true;
      p[k].insert(i);
      dfs(i, !k);
    }
  }
}

bool verifica(int k) {
  for (auto u: p[k]) {
    for (auto v: p[k]) {
      if (! g[u][v]) return false;
    }
  }
  return true;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cin >> g[i][j];
    }
  }
  
  for (int i = 0; i < n; i++) {
    //cerr << i << ' ' << vis[i] << '\n';;
    if (!vis[i]) {
      vis[i] = true;
      p[0].insert(i);
      dfs(i, 1);
    }
  }
  
  /*
  for (auto x: p[0]) cerr << x << ' ';
  cerr << '\n';
  for (auto x: p[1]) cerr << x << ' ';
  cerr << '\n';
  */
  
  if (verifica(0) and verifica(1)) cout << "Bazinga!\n";
  else cout << "Fail!\n";

  return 0;

}
