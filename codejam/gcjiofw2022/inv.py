t = int(input())

for teste in range(t):
    n = int(input())
    m = [[0 if c == 'O' else 1 for c in input()] for i in range(2*n)]

    a = sum([sum(m[i][:n]) for i in range(n)])
    b = sum([sum(m[i][:n]) for i in range(n, 2*n)])
    c = sum([sum(m[i][n:]) for i in range(n)])
    d = sum([sum(m[i][n:]) for i in range(n, 2*n)])

    ans = abs(a-d) + abs(b-c)

    print(f"Case #{teste+1}: {ans}")

