#include <bits/stdc++.h>

#define PB push_back
#define F first
#define S second
#define INF 1000000007

using namespace std;

int t;
int n;
int a, b;
long long c;

vector<pair<int, long long>> g[1010];
long long tot;
long long dist[1010];
int v, u, aux;
queue<int> q;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 1; teste <= t; teste++) {

    cin >> n;

    for (int i = 1; i <= n; i++) g[i].clear();
    tot = 0;
    
    for (int i = 1; i < n; i++) {
      cin >> a >> b >> c;
      g[a].PB({b, c});
      g[b].PB({a, c});
      tot += 2*c;
    }

    memset(dist, -1, sizeof(dist));

    v = 1;
    u = v;
    dist[v] = 0;
    q.push(v);

    while (not q.empty()) {
      aux = q.front();
      q.pop();

      for (auto x: g[aux]) { 
        if (dist[x.F] != -1) continue;
        dist[x.F] = dist[aux] + x.S;
        q.push(x.F);
        if (dist[x.F] > dist[u]) u = x.F;
      }
    }

    //for (int i = 1; i <= n; i++) cerr << dist[i] << ' ';
    //cerr << '\n';

    memset(dist, -1, sizeof(dist));

    v = u;
    dist[v] = 0;
    q.push(v);

    while (not q.empty()) {
      aux = q.front();
      q.pop();

      for (auto x: g[aux]) { 
        if (dist[x.F] != -1) continue;
        dist[x.F] = dist[aux] + x.S;
        q.push(x.F);
        if (dist[x.F] > dist[u]) u = x.F;
      }
    }

    //for (int i = 1; i <= n; i++) cerr << dist[i] << ' ';
    //cerr << '\n';

    cout << "Case #" << teste << ": " << tot - dist[u] << '\n';
    //cerr << u << ' ' << v << '\n';

  }

  return 0;

}
