#include <bits/stdc++.h>

#define F first
#define S second
#define INF 1000000007

using namespace std;

struct basil {
  int m, l, e;
};

int t;
int d, n, u;
basil b;
int o;

queue<basil> chega;
queue<int> ordem;
priority_queue<pair<int, int>> folhas;
int aux, cnt;
pair<int, int> p;


int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;

  for (int teste = 1; teste <= t; teste++) {
    
    while (not chega.empty()) chega.pop();
    while (not ordem.empty()) ordem.pop();
    while (not folhas.empty()) folhas.pop();

    cin >> d >> n >> u;
    for (int i = 0; i < d; i++) {
      cin >> b.m >> b.l >> b.e;
      chega.push(b);
    }
    for (int i = 0; i < n; i++) {
      cin >> o;
      ordem.push(o);
    }

    b.m = b.l = b.e = INF;
    chega.push(b);
    cnt = 0;

    while (not ordem.empty()) {
      o = ordem.front();
      b = chega.front();
      
      // se proximo evento eh folha, pega as folhas e continua
      if (b.m <= o) {
        folhas.push({-(b.m+b.e), b.l});
        chega.pop();
        continue;
      }

      // se alguma folha expirou antes da ordem, retira elas da pq
      while (not folhas.empty() and -folhas.top().F <= o) folhas.pop();
      
      // pega folhas ate ter o suficiente
      aux = u;
      while (not folhas.empty() and aux > 0) {
        p = folhas.top();
        folhas.pop();
        if (p.S <= aux) aux -= p.S;
        else {
          p.S -= aux;
          aux = 0;
          folhas.push(p);
        }
      }
      
      // se chegou aqui, entao ou aux eh 0 ou as folhas acabaram

      if (aux != 0) break;

      cnt++;
      ordem.pop();
    }

    cout << "Case #" << teste << ": " << cnt << '\n';
  }

  return 0;

}
