t = int(input())

for test in range(t):
    x, y = map(int, input().split())
    sq = max(x, y)-1
    v = sq**2

    if sq%2 == 0:
        x, y = y, x

    v += x
    if x == sq+1:
        v += sq-y+1

    print(v)
