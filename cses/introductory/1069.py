s = input() + 'x'

m = 1
a = 1
for i in range(1, len(s)):
    if s[i] == s[i-1]:
        a+=1
    else:
        m = max(m, a)
        a = 1

print(m)
