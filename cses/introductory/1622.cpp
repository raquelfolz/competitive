#include<bits/stdc++.h>

using namespace std;

string s;
set<string> per;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> s;
  sort(s.begin(), s.end());

  do {
    per.insert(s);
  } while(next_permutation(s.begin(), s.end()));

  cout << per.size() << '\n';
  for (auto x: per) cout << x << '\n';

  return 0;
}

