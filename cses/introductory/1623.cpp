#include<bits/stdc++.h>
#define INF 1e9+7

using namespace std;

int n;
int p[22];
long long s, a, m=INF;
vector<int>sub;

void search(int k) {
  if (k == n) {
    //processa
    a = 0;
    for (auto x: sub) a += x;
    m = min(m, abs(s-2*a));
  } else {
    //inclui k-esimo
    sub.push_back(p[k]);
    search(k+1);
    //nao inclui k-esimo
    sub.pop_back();
    search(k+1);
  }
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> p[i];
    s += p[i];
  }
  
  search(0);
  
  cout << m << '\n';

  return 0;
}

