n = int(input())

l = ['0', '1']

j = 1
c = 1
for i in range(2, 1<<n):
    l.append('1' + '0'*(c-len(l[j])) + l[j])
    j -= 1
    if j < 0:
        j = i
        c += 1

for x in l:
    print('0'*(n-len(x)) + x)
