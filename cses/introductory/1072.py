canto = 2 #3x3 -> 4 num tab
borda1 = 3 #4x3 -> 8 num tab (dist 1 do canto)
borda2 = 4 #5x3 -> muitos num tab (dist 2+ do canto)

atk = [0, 0, 0, 8] #quantidades de formas de atacarem ixi

n = int(input())

for i in range(4, n+1):
    b = 3*canto + 4*borda1-2 + (2*i-8)*borda2
    atk.append(b + atk[-1])

for i in range(1, n+1):
    print((i**2*(i**2-1)//2) - atk[i])
