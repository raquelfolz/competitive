n = int(input())
l = map(int, input().split())

last = 0
tot = 0
for x in l:
    if x < last:
        tot += last-x
    else:
        last = x

print(tot)
