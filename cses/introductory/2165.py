n = int(input())

t = [list(range(n, 0, -1)), [], []]
m = 0
if n%2 == 0:
    k = 1
else:
    k = 2
    
print((1<<n) -1)

for i in range((1<<n) -1):
    if i%2 == 0:
        p = (m+k)%3
        t[m].pop()
        t[p].append(1)
        print(m+1, p+1)
        m = p
    else:
        a = (m+1)%3
        b = (m+2)%3
        if len(t[a]) == 0 or len(t[b]) > 0 and t[a][-1] > t[b][-1]:
            a, b = b, a
        x = t[a][-1]
        t[a].pop()
        t[b].append(x)
        print(a+1, b+1)
