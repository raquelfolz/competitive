n = int(input())

if n in (2, 3):
    print("NO SOLUTION")
else:
    a = [x for x in range(1, n+1) if x%2 == 0]
    b = [x for x in range(1, n+1) if x%2 == 1]

    print(*a, *b)
