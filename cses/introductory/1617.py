m = int(1e9)+7

def foo(x):
    if x == 1:
        return 2
    a = False
    if x % 2 == 1:
        a = True
    b = foo(x//2)
    b *= b
    if a:
        b <<= 1
    b %= m
    return b

print(foo(int(input())))
