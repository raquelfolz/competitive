t = int(input())

for teste in range(t):
    a, b = map(int, input().split())
    if b > a:
        a, b = b, a

    x = a - b
    a = b - x
    if a < 0 or a % 3 != 0:
        print("NO")
    else:
        print("YES")
