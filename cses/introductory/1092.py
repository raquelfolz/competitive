n = int(input())
a = []
b = []

for i in range(n):
    if i % 4 in [0, 3]:
        a.append(n-i)
    else:
        b.append(n-i)

if (n*(n+1)//2) %2 != 0:
    print("NO")
else:
    print("YES")

    print(len(a))
    print(*a)

    print(len(b))
    print(*b)

