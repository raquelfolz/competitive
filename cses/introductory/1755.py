s = input()
l = [0]*26

for c in s:
    l[ord(c)-ord('A')] += 1

ans = ""
meio = None
for i in range(26):
    if l[i]%2 == 0:
        ans += chr(i + ord('A')) * (l[i]//2)
    elif meio is None:
        ans += chr(i + ord('A')) * (l[i]//2)
        meio = chr(i + ord('A'))
    else:
        print("NO SOLUTION")
        break
else:
    if meio:
        print(ans, meio, ans[::-1], sep="")
    else:
        print(ans, ans[::-1], sep="")
