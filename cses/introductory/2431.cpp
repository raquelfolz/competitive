#include<bits/stdc++.h>
#define INF 1e9+7

using namespace std;

int q;
long long k;
long long siz, cnt;
long long pos, num;
long long x;

void solve() {
  siz = 1;
  cnt = 9;

  while ((k+siz-1) / siz > cnt) {
    k -= cnt*siz++;
    cnt*= 10;
  }

  //posicao esquerda pra direita
  pos = (k-1)%siz; //indexado em 0
  //numero dentre os com aquele tamanho
  num = (k-1)/siz; //indexado em 0
  //o numero que ta procurando
  cnt /= 9;
  x = num + cnt;

  //cerr << k << ' ' << pos << ' ' << num << ' ' << cnt << ' ' << x << '\n';

  while (pos-- > 0) {
    x %= cnt;
    cnt /= 10;
  }
  cout << x /cnt << '\n';
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> q;

  for (int i = 0; i < q; i++) {
    cin >> k;
    solve();
  }

  return 0;
}

