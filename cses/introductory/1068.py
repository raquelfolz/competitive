def foo(n):
    yield n
    while n != 1:
        n = 3*n+1 if n%2 == 1 else n//2
        yield n

n = int(input())
print(*foo(n))

