n = int(input())
l = map(int, input().split())

exp = n+1 if n%2 == 1 else 0
s = 0
for x in l:
    s += x+1 if x%2 == 1 else -x

diff = abs(s - exp)

print(diff-1 if s < exp else diff)

