#include<bits/stdc++.h>
#define INF 1e9+7

using namespace std;

string t[8];
bool c[8], a[20], b[20];
long long cnt;

void search(int k) {
  if (k == 8) {
    cnt++;
    return;
  }
  for (int i = 0; i < 8; i++) {
    if (t[k][i] == '*') continue;
    if (c[i] or a[i+k] or b[8+i-k]) continue;
    c[i] = a[i+k] = b[8+i-k] = true;
    search(k+1);
    c[i] = a[i+k] = b[8+i-k] = false;
  }
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  for (int i = 0; i < 8; i++) cin >> t[i];
  
  search(0);

  cout << cnt << '\n';

  return 0;
}

