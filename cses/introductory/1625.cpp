#include <bits/stdc++.h>
using namespace std;
 
#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
 
#define f first
#define s second
#define pb push_back
 
typedef long long ll;
typedef pair<int, int> pii;
 
typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;
 
const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;
 
string s;
int cnt;
vector<int> p;
char c;
bool tab[10][10];
int x, y, k;
int fx[10], fy[10];
//ll aux, auxs;
 
bool vis[10][10];
queue<pair<int, int>> q;

bool wall(int v) {
 if (v == 0) { //D
   if (tab[x][y+1]) return false;
 }
 else if (v == 1) { //U
   if (tab[x][y-1]) return false;
 }
 else if (v == 2) { //R
   if (tab[x+1][y]) return false;
 }
 else if (v == 3) { //L
   if (tab[x-1][y]) return false;
 }

 return true;
}

void desempilha() {
  k = p.back();
  fx[x] -= 1;
  fy[y] -= 1;
  if (k == 0) { //D
    tab[x][y--] = 0;
  }
  else if (k == 1) { //U
    tab[x][y++] = 0;
  }
  else if (k == 2) { //R
    tab[x--][y] = 0;
  }
  else if (k == 3) { //L
    tab[x++][y] = 0;
  }
  p.pop_back();
}
 
bool empilha(int v) {
  
  if (v == 0) { //D
    if (tab[x][y+1]) return false;
    tab[x][++y] = 1;
  }
  else if (v == 1) { //U
    if (tab[x][y-1]) return false;
    tab[x][--y] = 1;
  }
  else if (v == 2) { //R
    if (tab[x+1][y]) return false;
    tab[++x][y] = 1;
  }
  else if (v == 3) { //L
    if (tab[x-1][y]) return false;
    tab[--x][y] = 1;
  }
  else return false;
  
  p.pb(v);
  fx[x] += 1;
  fy[y] += 1;
    
  int kb = k;
  k = v;

  if (p.size() == 48) {
    if (x == 1 and y == 7) cnt++;
    desempilha();
    k = kb;
    return false;
  }

  //se chegou no final, esta errado
  //se uma linha tiver preenchida e as vizinhas nao,
  //vai ser impossivel preencher
  //ou se bateu em parede
  if ( (x == 1 and y == 7)
      or (fx[x] == 7 and fx[x-1] < 7 and fx[x+1] < 7)
      or (fy[y] == 7 and fy[y-1] < 7 and fy[y+1] < 7)
      or (not wall(k) and wall(k^2) and wall(k^3))
      or (tab[x][y] == tab[x+1][y+1] and tab[x][y] != tab[x+1][y] and tab[x][y] != tab[x][y+1])
      or (tab[x][y] == tab[x+1][y-1] and tab[x][y] != tab[x+1][y] and tab[x][y] != tab[x][y-1])
      or (tab[x][y] == tab[x-1][y+1] and tab[x][y] != tab[x-1][y] and tab[x][y] != tab[x][y+1])
      or (tab[x][y] == tab[x-1][y-1] and tab[x][y] != tab[x-1][y] and tab[x][y] != tab[x][y-1])
      ) {
    desempilha();
    k = kb;
    return false;
  }
  return true;
}
 
 
 
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> s;
 
  for (int i = 0; i <= 8; i++) {
    tab[i][0] = 1;
    tab[i][8] = 1;
    tab[0][i] = 1;
    tab[8][i] = 1;
  }
  tab[1][1] = 1;
  x = 1; y = 1;
  
  fx[0] = 7;
  fx[8] = 7;
  fy[0] = 7;
  fy[8] = 7;
  fx[1] = 1;
  fy[1] = 1;
 
  //D = 0
  //U = 1
  //R = 2
  //L = 3
  
  if (s[0] == '?' or s[0] == 'D') empilha(0);
  else if (s[0] == 'R') empilha(2);
  
  while (p.size() > 0) {
    
    if ((s[p.size()] == '?' or s[p.size()] == 'D') and empilha(0)) continue;
    if ((s[p.size()] == '?' or s[p.size()] == 'U') and empilha(1)) continue;
    if ((s[p.size()] == '?' or s[p.size()] == 'R') and empilha(2)) continue;
    if ((s[p.size()] == '?' or s[p.size()] == 'L') and empilha(3)) continue;
 
    //se nao conseguiu empilhar ninguem... significa que o caminho nao funciona
    while (true) {
      //se a pilha esvaziou, nem vale a pena continuar
      if (p.size() == 0) goto fim;
      desempilha();
      //se eu conseguir trocar o cara que eu tirei,
      //tento o caminho dele
      if (s[p.size()] == '?') {
        if (empilha(k+1)) break;
        if (empilha(k+2)) break;
        if (empilha(k+3)) break;
      }
    }
  }
 
fim:
 
  cout << cnt << '\n';
 
  return 0;
}
