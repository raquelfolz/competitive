#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
ll x[5010];
ll s[5010];
ll dp[5010][5010];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> x[i];
    s[i] = s[i-1] + x[i];
    dp[i][i] = x[i];
  }

  for (int k = 1; k < n; k++) {
    for (int i = 1; i + k <= n; i++) {
      dp[i][i+k] = s[i+k]-s[i-1] - min(dp[i+1][i+k], dp[i][i+k-1]);
    }
  }
  
  cout << dp[1][n] << '\n';

  return 0;
}

