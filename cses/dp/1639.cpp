#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
string s, t;
int dp[5010][5010];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> s >> t;
  n = s.size();
  m = t.size();

  for (int i = 1; i <= n; i++) dp[i][0] = i;
  for (int j = 1; j <= m; j++) dp[0][j] = j;

  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      dp[i][j] = INF;
      if (s[i-1] == t[j-1]) dp[i][j] = dp[i-1][j-1];
      dp[i][j] = min(dp[i][j], dp[i-1][j-1]+1); //replace
      dp[i][j] = min(dp[i][j], dp[i][j-1]+1); //add
      dp[i][j] = min(dp[i][j], dp[i-1][j]+1); //remove 
    }
  }

  cout << dp[n][m] << '\n';


  return 0;
}

