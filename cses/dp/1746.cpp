#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
ll x[100100];
ll dp[110][2];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;

  cin >> x[0];
  if (x[0] != 0) dp[x[0]][0] = 1;
  if (x[0] == 0) {
    for (int j = 1; j <= m; j++) dp[j][0] = 1;
  } 
  for (int i = 1; i < n; i++) {
    cin >> x[i];
    if (x[i] != 0) {
      for (int j = 1; j <= m; j++) dp[j][i%2] = 0;
      dp[x[i]][i%2] = dp[x[i]-1][(i-1)%2];
      dp[x[i]][i%2] += dp[x[i]][(i-1)%2];
      dp[x[i]][i%2] += dp[x[i]+1][(i-1)%2];
      dp[x[i]][i%2] %= INF;
      continue;
    }
    for (int j = 1; j <= m; j++) {
      dp[j][i%2] = dp[j-1][(i-1)%2];
      dp[j][i%2] += dp[j][(i-1)%2];
      dp[j][i%2] += dp[j+1][(i-1)%2];
      dp[j][i%2] %= INF;
    }
  }
  
  ll ans=0;
  for (int j = 1; j <= m; j++) ans += dp[j][(n-1)%2];
  ans %= INF;
  cout << ans << '\n';

  return 0;
}

