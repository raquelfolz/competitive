#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

string a, b;
ll dp[10][11][20];

int auxa, auxb;

ll ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> a >> b;
  
  char aux=0;
  for (int c = a.size()-1; c >= 0; c--) {
    if (a[c] == aux) break;
    aux = a[c];
    auxa++;
  }
  aux=0;
  for (int c = b.size(); c >= 0; c--) {
    if (b[c] == aux) break;
    aux = b[c];
    auxb++;
  }
  
  dp[0][10][0] = 1;

  for (int i = 1; i <= 18; i++) {
    for (int d = 0; d < 10; d++) {
      dp[0][d][i] = dp[0][10][i-1] - dp[0][d][i-1];
      dp[0][10][i] += dp[0][d][i];
      //final estritamente menor que o de a
      if (i <= a.size()) {
        if (d < (a[a.size()-i] - '0')) {
          dp[1][d][i] = dp[0][10][i-1] - dp[0][d][i-1];
       } else if (d == (a[a.size()-i] - '0')) {
          dp[1][d][i] = dp[1][10][i-1] - dp[1][d][i-1];
        }
        dp[1][10][i] += dp[1][d][i];
      }
      //final estritamente maior que o de b
      if (i <= b.size()) {
        if (d > (b[b.size()-i] - '0')) {
          dp[2][d][i] = dp[0][10][i-1] - dp[0][d][i-1];
       } else if (d == (b[b.size()-i] - '0')) {
          dp[2][d][i] = dp[2][10][i-1] - dp[2][d][i-1];
        }
        dp[2][10][i] += dp[2][d][i];
      }
    }
    if (i >= a.size() and i <= b.size())
      ans += dp[0][10][i] - dp[0][0][i];
  }
  
  ans -= dp[1][10][a.size()] - dp[1][0][a.size()];
  ans -= dp[2][10][b.size()] - dp[2][0][b.size()];

  if (a == "0") ans += 1;

  cout << ans << '\n';
  return 0;
}

