INF = 10**9+7

n = int(input())

l = [0, 1, 2, 4, 8, 16, 32]

for i in range(7, n+1):
    l.append(sum(l[-6:])%INF)

print(l[n])
