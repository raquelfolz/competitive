#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int x[200100];
vector<int> lis;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> x[i];
    auto it = lower_bound(lis.begin(), lis.end(), x[i]);
    if (it == lis.end()) lis.pb(x[i]);
    else *it = x[i];
  }

  cout << lis.size() << '\n';
    
  return 0;
}

