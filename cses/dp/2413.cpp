#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;

ll dp[1000100][2]; //0=dois, 1=uns

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  dp[1][0] = 1;
  dp[1][1] = 1;
  for (int i = 2; i < 1000010; i++) {
    dp[i][0] = (2*dp[i-1][0] + dp[i-1][1]) % INF;
    dp[i][1] = (dp[i-1][0] + 4*dp[i-1][1]) % INF;
  }

  int t;
  cin >> t;
  while (t--) {
    cin >> n; cout << (dp[n][0] + dp[n][1]) % INF << '\n';
  }

  return 0;
}
