#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int s;
ll dp[100100];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  s = (n+1)*n/2;
  if (s % 2 != 0) {
    cout << "0\n";
    return 0;
  }
  
  dp[0] = 1;
  cerr << s << '\n';
  for (int k = 1; k < n; k++) {
    for (int i = s/2; i >= k; i--) {
      dp[i] += dp[i-k];
      dp[i] %= INF;
    }
  }
  cout << dp[s/2] << '\n';

  return 0;
}

