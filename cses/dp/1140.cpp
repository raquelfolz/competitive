#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

struct projeto {
  int a, b, p;
};

int n;
projeto pro[200100];
vector<int> d;
ll dp[400100];

bool cmp(projeto a, projeto b) {
  return a.b < b.b;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> pro[i].a >> pro[i].b >> pro[i].p;
    d.pb(pro[i].a);
    d.pb(pro[i].b);
  }
  sort(pro+1, pro+n+1, cmp);
  sort(d.begin(), d.end());
  d.erase(unique(d.begin(), d.end()), d.end());

  int h = 0;
  for (int i = 1; i <= n; i++) {
    while (d[h] < pro[i].b) {
      h++;
      dp[h+1] = dp[h];
    }
    int aux = lower_bound(d.begin(), d.end(), pro[i].a)-d.begin();
    dp[h+1] = max(dp[aux]+pro[i].p, dp[h+1]);
  }
  
  cout << dp[h+1] << '\n';
  
  return 0;
}

