#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
string tab[1010];
int dp[1010][1010];
int x;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) cin >> tab[i];

  if (tab[0][0] == '*') {cout << 0 << '\n'; return 0;}
  if (n == 1) {cout << 1 << '\n'; return 0;}

  for (int i = 1; i < n; i++) {
    if (tab[0][i] == '*') break;
    dp[0][i] = 1;
  }
  for (int i = 1; i < n; i++) {
    if (tab[i][0] == '*') break;
    dp[i][0] = 1;
  }

  for (int i = 1; i < n; i++) {
    for (int j = 1; j < n; j++) {
      if (tab[i][j] == '*') continue;
      if (tab[i-1][j] != '*') dp[i][j] += dp[i-1][j];
      dp[i][j] %= INF;
      if (tab[i][j-1] != '*') dp[i][j] += dp[i][j-1];
      dp[i][j] %= INF;
    }
  }

  cout << dp[n-1][n-1] << '\n';

  return 0;
}

