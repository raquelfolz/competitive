#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, x;
int c[110];
int dp[1000100];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> x;
  for (int i = 0; i < n; i++) cin >> c[i];

  sort(c, c+n);

  for (int i = 1; i <= x; i++) {
    dp[i] = INF;
    for (int j = 0; j < n; j++) {
      if (c[j] > i) break;
      dp[i] = min(dp[i], dp[i-c[j]]+1);
    }
  }
  
  if (dp[x] == INF) dp[x] = -1;
  cout << dp[x] << '\n';
  
  return 0;
}

