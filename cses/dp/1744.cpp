#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int a, b;
int dp[510][510];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> a >> b;
  if (a < b) swap(a, b);

  for (int i = 1; i <= a; i++) {
    dp[i][1] = dp[1][i] = i-1;
    dp[i][i] = 0;
    for (int j = 2; j < i; j++) {
      dp[i][j] = (i-1)*(j-1);
      for (int k = 1; k < i; k++) {
        dp[i][j] = min(dp[i][j], dp[k][j] + dp[i-k][j]+1);
      }
      for (int k = 1; k < j; k++) {
        dp[i][j] = min(dp[i][j], dp[i][k] + dp[i][j-k]+1);
      }
      dp[j][i] = dp[i][j];
    }
  }

  cout << dp[a][b] << '\n';

  return 0;
}

