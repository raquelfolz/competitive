#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int x[110];
int dp[100100];
int maior, cnt;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  
  for (int k = 1; k < 100100; k++) dp[k] = INF;
  for (int i = 1; i <= n; i++) {
    cin >> x[i];
    for (int k = 0; k <= maior; k++) {
      if (dp[k] < i) dp[k+x[i]] = min(dp[k+x[i]], i);
    }
    maior += x[i];
  }

  for (int k = 1; k < maior; k++) if(dp[k] != INF) cnt++;
  cout << cnt + 1 << '\n';
  for (int k = 1; k < maior; k++) if(dp[k] != INF) cout << k << ' ';
  cout << maior << '\n';
    
  return 0;
}

