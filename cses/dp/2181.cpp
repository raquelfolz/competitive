#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
unsigned int dp[1100][1100];
int cnt;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> m >> n;

  for (int j = 0; j < (1 << m); j++) {
    cnt = 0;
    for (int x = 0; x < m; x++) {
      if (j & (1 << x)) {
        if (cnt % 2 != 0) break;
      } else cnt++;
    }
    if (cnt % 2 == 0) dp[0][j] = 1;
  }

  for (int i = 1; i < n; i++) {
    for (int k = 0; k < (1 << m); k++) {
      if (dp[i-1][k] == 0) continue;
      for (int j = 0; j < (1 << m); j++) {
        cnt = 0;
        for (int x = m-1; x >= 0; x--) {
          //o de cima estava aberto, o de baixo eh fechado
          if (k & (1 << x)) {
            if (j & (1 << x) or cnt % 2 != 0) {
              cnt = 1;
              j += (1 << x)-1;
              break;
            }
          } else if (j & (1 << x)) {
            if (cnt % 2 != 0) {
              j += (1 << x)-1;
              break;
            }
          } else cnt++;
        }
        if (cnt % 2 == 0) {
          dp[i][j] += dp[i-1][k];
          dp[i][j] %= INF;
        }
      }
    }
  }

  cout << dp[n-1][0] << '\n';
  
  return 0;
}

