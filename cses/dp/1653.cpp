#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
ll x;
ll w[30];
pair<int, long long> dp[1100100];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> x;
  for (int i = 0; i < n; i++) cin >> w[i];
  sort(w, w+n);
  
  dp[0].s = INF;

  for (int i = 1; i < (1 << n); i++) {
    dp[i].f = INF;
    for (int j = 0; j < n; j++) {
      if (not (i & (1<<j))) continue;
      int k = i ^ (1<<j);
      if (w[j]+dp[k].s <= x) {
        dp[i] = min(dp[i], {dp[k].f, dp[k].s + w[j]});
      } else {
        dp[i] = min(dp[i], {dp[k].f+1, w[j]});
      }
    }
  }
  
  cout << dp[(1<<n)-1].f << '\n';
    
  return 0;
}

