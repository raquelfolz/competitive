#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, x;
int h[1010];
int s[1010];

int dp[100100][2];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> x;
  for (int i = 1; i <= n; i++) cin >> h[i];
  for (int i = 1; i <= n; i++) cin >> s[i];

  //se eu pegar o livro i, posso gastar x-p[i] ate o livro i-1
  //se eu nao pegar o livro i, posso gastar x ate o livro i-1
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= x; j++) {
      int a=0, b=0;
      if (h[i] <= j) a = dp[j-h[i]][(i-1)%2] + s[i];
      b = dp[j][(i-1)%2];
      dp[j][i%2] = max(a, b);
    }
  }

  cout << dp[x][n%2] << '\n';

  return 0;
}

