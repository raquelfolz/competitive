#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int k;
int a, b;
vector<int> g[550];
int to[550];
int cnt;

bool vis[1100];
bool kuhn(int v) {
  vis[v] = true;
  int x;
  for (auto u: g[v]) {
    if (vis[x=to[u]]) continue;
    to[u] = v;
    if (x == 0 or kuhn(x)) return true;
    to[u] = x;
  }
  return false;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m >> k;
  for (int i = 0; i < k; i++) {
    cin >> a >> b;
    g[a].pb(b);
  }

  for (int i = 1; i <= n; i++) {
    memset(vis, 0, sizeof(vis));
    if (kuhn(i)) cnt++;
  }

  cout << cnt << '\n';
  for (int i = 1; i <= m; i++) {
    if (to[i] != 0) cout << to[i] << ' ' << i << '\n';
  }
  
  return 0;
}

