#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[200100];
bool vis[200100];
bool stk[200100];
vector<int> ans;

bool dfs(int v) {
  stk[v] = true;
  for (auto u: g[v]) {
    if (ans.size() > 0) break;
    if (not vis[u]) {
      vis[u] = true;
      dfs(u);
    } else if (stk[u]) ans.pb(u);
  }
  stk[v] = false;
  if (ans.size() > 0 and not (ans.size() > 1 and ans.back() == ans.front())) ans.pb(v);
  if (ans.size() > 0) return true;
  return false;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
  }

  for (int i = 1; i <= n; i++) {
    if (not vis[i]) {
      vis[i] = true;
      if (dfs(i)) goto resp;
    }
  }

  cout << "IMPOSSIBLE\n";
  return 0;

resp:
  cout << ans.size() << '\n';
  for (int i = ans.size()-1; i > 0; i--) cout << ans[i] << ' ';
  cout << ans.front() << '\n';
  
  return 0;
}

