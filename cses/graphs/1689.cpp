#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int x, y;
int tab[8][8];
vector<pii> pos;
vector<int> mov;
vector<int> num;
int a, b, c, d;

int cnt(int a, int b) {
  int c = 0;
  if (a-2 >= 0 and b-1 >= 0 and tab[a-2][b-1] == 0) c++;
  if (a-2 >= 0 and b+1 < 8 and tab[a-2][b+1] == 0) c++;
  if (a+2 < 8 and b-1 >= 0 and tab[a+2][b-1] == 0) c++;
  if (a+2 < 8 and b+1 < 8 and tab[a+2][b+1] == 0) c++;
  if (a-1 >= 0 and b-2 >= 0 and tab[a-1][b-2] == 0) c++;
  if (a-1 >= 0 and b+2 < 8 and tab[a-1][b+2] == 0) c++;
  if (a+1 < 8 and b-2 >= 0 and tab[a+1][b-2] == 0) c++;
  if (a+1 < 8 and b+2 < 8 and tab[a+1][b+2] == 0) c++;
  return c;
}

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> y >> x;
  x--; y--;
  pos.pb({x, y});
  mov.pb(0);
  /*num.pb(0);*/

  while (true) {
    /*for (auto [xx, yy]: pos) cerr << xx << ',' << yy << ' ';*/
    /*cerr << '\n';*/
    /*for (auto xx: mov) cerr << xx << ' ';*/
    /*cerr << '\n';*/
    /*for (auto xx: num) cerr << xx << ' ';*/
    /*cerr << '\n';*/
    /*for (int i = 0; i < 8; i++) {*/
    /*  for (int j = 0; j < 7; j++) cerr << tab[i][j] << ' ';*/
    /*  cerr << tab[i][7] << '\n';*/
    /*}*/
    x = pos.back().f;
    y = pos.back().s;
    tab[x][y] = pos.size();
    if (pos.size() == 64) break;
    a = x; b = y; c = 9;
    if (a-2 >= 0 and b-1 >= 0 and tab[a-2][b-1] == 0) c = min(cnt(a-2, b-1), c);
    if (a-2 >= 0 and b+1 < 8 and tab[a-2][b+1] == 0) c = min(cnt(a-2, b+1), c);
    if (a+2 < 8 and b-1 >= 0 and tab[a+2][b-1] == 0) c = min(cnt(a+2, b-1), c);
    if (a+2 < 8 and b+1 < 8 and tab[a+2][b+1] == 0) c = min(cnt(a+2, b+1), c);
    if (a-1 >= 0 and b-2 >= 0 and tab[a-1][b-2] == 0) c = min(cnt(a-1, b-2), c);
    if (a-1 >= 0 and b+2 < 8 and tab[a-1][b+2] == 0) c = min(cnt(a-1, b+2), c);
    if (a+1 < 8 and b-2 >= 0 and tab[a+1][b-2] == 0) c = min(cnt(a+1, b-2), c);
    if (a+1 < 8 and b+2 < 8 and tab[a+1][b+2] == 0) c = min(cnt(a+1, b+2), c);
    
    if (c != 9) num.pb(c);
    else {
      tab[x][y] = 0;
      pos.pop_back();
      mov.pop_back();
      while (mov.back() == 8) {
        x = pos.back().f;
        y = pos.back().s;
        tab[x][y] = 0;
        pos.pop_back();
        mov.pop_back();
        num.pop_back();
      }
      a = pos.back().f;
      b = pos.back().s;
      c = num.back();
    }
    
    lab:
    /*cerr << a << ' ' << b << ' ' << pos.size() << ' ' << mov.back() << '\n';*/
    if (mov.back() < 1 and a-2 >= 0 and b-1 >= 0 and tab[a-2][b-1] == 0) {
      if (cnt(a-2, b-1) == c) {
        pos.pb({a-2, b-1});
        mov.back() = 1;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 1);
    if (mov.back() < 2 and a-2 >= 0 and b+1 < 8 and tab[a-2][b+1] == 0) {
      if (cnt(a-2, b+1) == c) {
        pos.pb({a-2, b+1});
        mov.back() = 2;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 2);
    if (mov.back() < 3 and a+2 < 8 and b-1 >= 0 and tab[a+2][b-1] == 0) {
      if (cnt(a+2, b-1) == c) {
        pos.pb({a+2, b-1});
        mov.back() = 3;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 3);
    if (mov.back() < 4 and a+2 < 8 and b+1 < 8 and tab[a+2][b+1] == 0) {
      if (cnt(a+2, b+1) == c) {
        pos.pb({a+2, b+1});
        mov.back() = 4;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 4);
    if (mov.back() < 5 and a-1 >= 0 and b-2 >= 0 and tab[a-1][b-2] == 0) {
      if (cnt(a-1, b-2) == c) {
        pos.pb({a-1, b-2});
        mov.back() = 5;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 5);
    if (mov.back() < 6 and a-1 >= 0 and b+2 < 8 and tab[a-1][b+2] == 0) {
      if (cnt(a-1, b+2) == c) {
        pos.pb({a-1, b+2});
        mov.back() = 6;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 6);
    if (mov.back() < 7 and a+1 < 8 and b-2 >= 0 and tab[a+1][b-2] == 0) {
      if (cnt(a+1, b-2) == c) {
        pos.pb({a+1, b-2});
        mov.back() = 7;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 7);
    if (mov.back() < 8 and a+1 < 8 and b+2 < 8 and tab[a+1][b+2] == 0) {
      if (cnt(a+1, b+2) == c) {
        pos.pb({a+1, b+2});
        mov.back() = 8;
        mov.pb(0);
        continue;
      }
    }
    mov.back() = max(mov.back(), 8);

    while (mov.back() == 8) {
      x = pos.back().f;
      y = pos.back().s;
      tab[x][y] = 0;
      pos.pop_back();
      mov.pop_back();
      num.pop_back();
    }
    a = pos.back().f;
    b = pos.back().s;
    c = num.back();
    goto lab;

    /*cerr << x+1 << ' ' << y+1 << '\n';*/
    
  }

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 7; j++) cout << tab[i][j] << ' ';
    cout << tab[i][7] << '\n';
  }
  
  return 0;
}

