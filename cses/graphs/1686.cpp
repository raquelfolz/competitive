#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
ll k[100100];
int a, b;
vector<int> g[100100];
vector<int> rg[100100];

int comp[100100];
int cnum;
ll ck[100100];
set<int> cg[100100];
ll d[100100];

bool vis[100100];
stack<int> s;

void dfs(int v) {
  for (auto u: g[v]) {
    if (! vis[u]) {
      vis[u] = true;
      dfs(u);
    }
  }
  s.push(v);
}

void rdfs(int v) {
  for (auto u: rg[v]) {
    if (comp[u] == 0) {
      comp[u] = cnum;
      rdfs(u);
    }
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 1; i <= n; i++) cin >> k[i];
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    rg[b].pb(a);
  }

  for (int i = 1; i <= n; i++) {
    if (! vis[i]) dfs(i);
  }

  while (! s.empty()) {
    a = s.top();
    s.pop();
    if (comp[a] == 0) {
      comp[a] = ++cnum;
      rdfs(a);
    }
  }

  for (int i = 1; i <= n; i++) {
    ck[comp[i]] += k[i];
    for (auto v: g[i]) {
      if (comp[v] != comp[i]) cg[comp[v]].insert(comp[i]);
    }
  }

  ll ans = 0;
  for (int i = 1; i <= cnum; i++) {
    d[i] = ck[i];
    for (auto x: cg[i]) d[i] = max(d[i], ck[i]+d[x]);
    ans = max(ans, d[i]);
  }
  
  cout << ans << '\n';
  
  return 0;
}

