#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
multiset<int> g[100100], rg[100100];
vector<int> ans;
stack<int> s;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].insert(b);
    rg[b].insert(a);
  }

  g[n].insert(1);
  rg[1].insert(n);

  for (int i = 1; i <= n; i++) {
    if (g[i].size() != rg[i].size()) {
      cout << "IMPOSSIBLE\n";
      return 0;
    }
  }

  s.push(n);
  s.push(1);

  g[n].erase(g[n].find(1));
  rg[1].erase(rg[1].find(n));
  
  for (int i = 0; i < m; i++) {
    while (g[a=s.top()].size() == 0) {
      ans.pb(a);
      s.pop();
      if (s.size() == 1) { //isso eh antes de percorrer a ultima aresta
                           //nao posso jamais ficar com um so vertice se ainda tem alguma aresta
        cout << "IMPOSSIBLE\n";
        return 0;
      }
    }
    b = *(g[a].begin());
    s.push(b);
    g[a].erase(b);
    rg[b].erase(a);
  }

  while (not s.empty()) {
    ans.pb(s.top());
    s.pop();
  }

  for (int i = ans.size()-2; i > 0; i--) cout << ans[i] << ' ';
  cout << ans.front() << '\n';
  
  return 0;
}

