#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll c;
vector<pair<pii, ll>> e;
ll dist[2510];
int pre[2510];
bool vis[2510];
vector<int> ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    e.pb({{a,b},c});
  }

  for (int i = 1; i < n; i++) {
    for (auto [p, x] : e) {
      if (dist[p.s] > dist[p.f] + x) {
        dist[p.s] = dist[(pre[p.s]=p.f)]+x;
      }
    }
  }
  for (auto [p, x] : e) {
    if (dist[p.s] > dist[p.f] + x) {
      dist[p.s] = dist[(pre[p.s]=p.f)]+x;
      a = p.s;
      goto ciclo;
    }
  }

  cout << "NO\n";
  return 0;

ciclo:

  cout << "YES\n";
  
  while(not vis[a]) { 
    vis[a] = true; a = pre[a];
  }

  ans.pb(a);
  while (pre[a] != ans.front()) ans.pb(a=pre[a]);
  cout << ans.front();
  for (int i = ans.size()-1; i >= 0; i--) cout << ' ' << ans[i];
  cout << '\n';
  
  return 0;
}

