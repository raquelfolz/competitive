#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll c;
int k;
vector<pair<int, ll>> g[100100];

vector<ll> d[100100];
priority_queue<pair<ll, int>> q;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m >> k;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    g[a].pb({b, c});
  }
  
  q.push({0, 1});

  while (!q.empty()) {
    c = -q.top().f;
    a = q.top().s;
    q.pop();
    if (d[a].size() == k) continue;
    d[a].pb(c);
    for (auto [v, x]: g[a]) {
      if (d[v].size() != k) q.push({-c-x, v});
    }
  }

  for (int i = 0; i+1 < d[n].size(); i++) cout << d[n][i] << ' ';
  cout << d[n].back() << '\n';
  
  return 0;
}

