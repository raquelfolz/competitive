#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll c;
vector<pair<int, ll>> g[100100];
ll dist[100100];
priority_queue<pair<ll, int>> q;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    g[a].pb({b, c});
  }
  
  memset(dist, -1, sizeof(dist));
  q.push({0, 1});

  while (! q.empty()) {
    a = q.top().s;
    c = -q.top().f;
    q.pop();
    if (dist[a] != -1) continue;
    dist[a] = c;
    for (auto [v, d]: g[a]) {
      if (dist[v] == -1) q.push({-c-d, v});
    }
  }

  for (int i = 1; i < n; i++) cout << dist[i] << ' ';
  cout << dist[n] << '\n';
  
  return 0;
}

