#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int m;
int a, b;
ll c;

vector<int> g[510];
ll w[510][510];
ll t, f;

bool vis[510];
bool dfs(int v) {
  if (v == n) return true;
  for (auto u: g[v]) {
    if (vis[u] or w[v][u] < t) continue;
    vis[u] = true;
    w[v][u] -= t;
    w[u][v] += t;
    if (dfs(u)) return true;
    w[v][u] += t;
    w[u][v] -= t;
  }
  return false;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    g[a].pb(b);
    g[b].pb(a);
    w[a][b] += c;
    t = max(t, c);
  }

  while (t > 0) {
    memset(vis, 0, sizeof(vis));
    if (dfs(1)) f += t;
    else t /= 2;
  }

  cout << f << '\n';
  
  return 0;
}

