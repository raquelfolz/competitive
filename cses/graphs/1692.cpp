#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b, t;
multiset<pii> g[100100], rg[100100];
string ans;
stack<pii> s;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n;
  if (n == 1) { cout << "01\n"; return 0; }

  m = 1 << (n-1);
  for (int i = 0; i < m; i++) {
    a = (i << 1) & (m-1);
    b = a | 1;
    rg[i].insert({a, '0'});
    g[a].insert({i, '0'});
    rg[i].insert({b, '1'});
    g[b].insert({i, '1'});
  }

  s.push({0, 0});
  for (int i = 1; i < n; i++) ans += '0';
  
  for (int i = 0; i < 2*m; i++) {
    while (g[a=s.top().f].size() == 0) {
      ans.pb(s.top().s);
      s.pop();
      if (s.size() == 0) { //isso eh antes de percorrer a ultima aresta
        cout << "IMPOSSIBLE\n";
        return 0;
      }
    }
    b = (*(g[a].begin())).f;
    t = (*(g[a].begin())).s;
    s.push({b, t});
    g[a].erase({b, t});
    rg[b].erase({a, t});
  }

  while (s.size() > 1) {
    ans.pb(s.top().s);
    s.pop();
  }

  cout << ans << '\n';
  
  return 0;
}

