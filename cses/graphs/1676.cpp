#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
int c[200100], s[200100];
int cnum, ms;

int find(int v) {
  if (c[v] == v) return v;
  return c[v] = find(c[v]);
}

void join(int v, int u) {
  v = find(v);
  u = find(u);
  if (v == u) return;
  if (v > u) swap(u, v);
  c[u] = v;
  s[v] += s[u];
  ms = max(ms, s[v]);
  cnum--;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;

  for (int i = 1; i <= n; i++) {
    c[i] = i;
    s[i] = 1;
  }
  cnum = n;

  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    join(a, b);
    cout << cnum << ' ' << ms << '\n';
  }
  
  return 0;
}

