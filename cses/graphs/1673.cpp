#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll x;
vector<pair<pii, ll>> e;
ll dist[2510];
ll distr[2510];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> x;
    e.pb({{a, b}, -x});
  }

  for (int i = 2; i <= n; i++) dist[i] = LINF;
  for (int i = 1; i < n; i++) distr[i] = LINF;

  for (int i = 1; i < n; i++) {
    for (auto [p, w]: e) if (dist[p.f] != LINF) dist[p.s] = min(dist[p.s], dist[p.f] + w);
    for (auto [p, w]: e) if (distr[p.s] != LINF) distr[p.f] = min(distr[p.f], distr[p.s] + w);
  }
  for (auto [p, w]: e) {
    if (dist[p.f] != LINF and dist[p.f] + w < dist[p.s] and distr[p.s] != LINF) {
      cout << "-1\n";
      return 0;
    }
    if (dist[p.f] != LINF) dist[p.s] = min(dist[p.s], dist[p.f] + w);
  }
  cout << -dist[n] << '\n';
  
  return 0;
}

