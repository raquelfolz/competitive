#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
string tab[1010];

bool vis[1010][1010];

void dfs(int x, int y) {
  if (vis[x][y]) return;
  if (tab[x][y] == '#') return;
  vis[x][y] = true;
  if (x > 0) dfs(x-1, y);
  if (x < n-1) dfs(x+1, y);
  if (y > 0) dfs(x, y-1);
  if (y < m-1) dfs(x, y+1);
}

int ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < n; i++) cin >> tab[i];

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      if (tab[i][j] == '.' and !vis[i][j]) {
        dfs(i, j);
        ans++;
      }
    }
  }

  cout << ans << '\n';
  
  return 0;
}

