#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll c;
vector<pair<int,ll>> g[100100];

ll d[100100];
bool vis[100100];
ll nr[100100];
int minf[100100], maxf[100100];
priority_queue<pair<ll, int>> q;
pair<ll, pii> prox;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    g[a].pb({b, c});
  }
  
  for (int i = 1; i <= n; i++) d[i] = LINF;
  q.push({0, 1});
  nr[1] = 1;

  while (!q.empty()) {
    a = q.top().s;
    c = -q.top().f;
    q.pop();
    if (vis[a]) continue;
    vis[a] = true;
    for (auto [v, x]: g[a]) {
      if (c+x < d[v]) {
        d[v] = c+x;
        nr[v] = nr[a];
        minf[v] = minf[a] + 1;
        maxf[v] = maxf[a] + 1;
        q.push({-d[v], v});
      } else if (c+x == d[v]) {
        nr[v] += nr[a];
        nr[v] %= INF;
        minf[v] = min(minf[v], minf[a] + 1);
        maxf[v] = max(maxf[v], maxf[a] + 1);
      }
    }
  }

  cout << d[n] << ' ' << nr[n] << ' ' << minf[n] << ' ' << maxf[n] << '\n';
  
  return 0;
}

