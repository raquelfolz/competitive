#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll c;
vector<pair<int, ll>> g[100100];
ll dist[2][100100];
priority_queue<pair<ll, int>> q;
ll aux;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    g[a].pb({b, c});
  }
  
  memset(dist, -1, sizeof(dist));
  q.push({0, 1});
  while (!q.empty()) {
    c = -q.top().f;
    a = q.top().s;
    q.pop();
    if (dist[0][a] != -1) continue;
    dist[0][a] = c;
    for (auto [v, x]: g[a]) {
      if (dist[0][v] == -1) q.push({-c-x, v});
    }
  }
  q.push({0, 1});
  while (!q.empty()) {
    c = -q.top().f;
    a = q.top().s;
    q.pop();
    if (dist[1][a] != -1) continue;
    dist[1][a] = c;
    for (auto [v, x]: g[a]) {
      if (dist[1][v] == -1) {
        aux = min(c+x, dist[0][a]+x/2);
        q.push({-aux, v});
      }
    }
  }

  cout << dist[1][n] << '\n';
  
  return 0;
}

