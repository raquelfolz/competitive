#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[100100], rg[100100];
int comp[100100];
int cnum;

bool vis[100100];
stack<int> s;

void dfs(int v) {
  for (auto x: g[v]) {
    if (not vis[x]) {
      vis[x] = true;
      dfs(x);
    }
  }
  s.push(v);
}

void rdfs(int v) {
  for (auto x: rg[v]) {
    if (comp[x] == 0) {
      comp[x] = cnum;
      rdfs(x);
    }
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;

  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    rg[b].pb(a);
  }

  for (int i = 1; i <= n; i++) {
    if (not vis[i]) {
      vis[i] = true;
      dfs(i);
    }
  }

  while (s.size() > 0) {
    a = s.top();
    s.pop();
    if (comp[a] == 0) {
      comp[a] = ++cnum;
      rdfs(a);
    }
  }

  cout << cnum << '\n';
  for (int i = 1; i < n; i++) cout << comp[i] << ' ';
  cout << comp[n] << '\n';
  
  return 0;
}

