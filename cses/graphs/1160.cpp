#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, q;
int t[200100];
int a, b;

int vis[200100]; //1 visitado 2 processado
int bs[200100][31];
vector<int> ciclo;
int p[200100], d[200100], idx[200100], cs[200100];

vector<int> g[200100];

int search(int v, int k) {
  for (int i = 30; i >= 0; i--) {
    if ((1 << i) & k) v = bs[v][i];
  }
  return v;
}

void dfs(int v) {
  if (t[v] == v) {
    for (int i = 0; i <= 30; i++) bs[v][i] = t[v];
    vis[v] = 2;
    p[v] = v;
    cs[v] = 1;
    return;
  }
  if (vis[t[v]] == 1) { //cheguei num ciclo!
    ciclo.pb(t[v]);
    ciclo.pb(v);
    return;
  }
  if (vis[t[v]] == 0) { //preciso descobrir o que tem pela frente
    vis[v] = 1;
    dfs(t[v]);
    if (ciclo.size() > 0) { //eu estou num ciclo :D
      ciclo.pb(v);
      if (ciclo.front() == v) {//eu sou o comeco
        reverse(ciclo.begin(), ciclo.end());
        for (int x = 0; x < ciclo.size()-1; x++) {
          for (int i = 0; i <= 30; i++) bs[ciclo[x]][i] = ciclo[((1 << i)+x) % (ciclo.size()-1)];
          vis[ciclo[x]] = 2;
          p[ciclo[x]] = v;
          idx[ciclo[x]] = x;
          cs[ciclo[x]] = ciclo.size()-1;
        }
        ciclo.clear();
      }
      return;
    }
  }
  if (vis[t[v]] == 2) {
    for (int i = 0; i <= 30; i++) bs[v][i] = search(t[v], (1<<i)-1);
    vis[v] = 2;
    p[v] = p[t[v]];
    d[v] = d[t[v]]+1;
    cs[v] = cs[t[v]];
    idx[v] = idx[t[v]];
  }
}

/*int find(int u, int v) {
  int ans=0;
  if (p[u] == p[v]) {
    if (d[v] != 0) { //destino nao esta no ciclo
      if (bf[u] == bf[v] and d[u] > d[v]) return d[u] - d[v];
      return -1;
    }
    if (d[u] != 0) { //origem nao esta no ciclo, destino esta
      if (p[v] == v) return d[u]; //se destino eh a cabeca no ciclo, eu posso ir direto
      return find(p[u], v) + d[u]; //senao eu tenho que depois ainda andar no ciclo
    }
    //origem e destino no ciclo
    return (idx[v]-idx[u]+cs[v])%cs[v];
  }
  while (true) {
    if (p[u] == u) return -1;
    ans += d[u];
    u = p[u];
    if (p[u] == p[v]) break;
  }
  if (bf[u] != bf[v]) return -1;
  return ans + find(p[u], v);
}*/

int entryt[200100], exitt[200100];
int tempo;
void rdfs(int v) {
  tempo++;
  entryt[v] = tempo;
  vis[v] = true;
  for (auto u: g[v]) {
    if (vis[u]) continue; //nao eh pra acontecer, mas por via
    vis[u] = true;
    rdfs(u);
  }
  exitt[v] = tempo;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> q;
  for (int i = 1; i <= n; i++) {
    cin >> t[i];
  }
  for (int i = 1; i <= n; i++) {
    if (vis[i] == 0) dfs(i);
  }

  for (int i = 1; i <= n; i++) { //crio o grafo reverso como se todo mundo chegasse no mesmo cara do ciclo
    if (d[i] == 0) continue;
    if (d[t[i]] == 0) g[p[t[i]]].pb(i);
    else g[t[i]].pb(i);
  }
  memset(vis, 0, sizeof(vis));
  for (int i = 1; i <= n; i++) { //calculo os tempos de entrada e saida de todo mundo :3
    if (vis[i] == 0 and d[i] == 0 and g[i].size() > 0) rdfs(i);
  }

  for (int i = 0; i < q; i++) {
    cin >> a >> b;
    /*cerr << p[a] << ' ' << d[a] << ' ' << entryt[a] << ' ' << exitt[a] << '\n';*/
    /*cerr << p[b] << ' ' << d[b] << ' ' << entryt[b] << ' ' << exitt[b] << '\n';*/
    //nao da pra chegar se
    //1) nao tem o mesmo ciclo base ou
    //2) destino nao esta no ciclo e tempos da origem nao tao entre os tempos do destino
    if (p[a] != p[b] or (d[b] != 0 and (entryt[a] < entryt[b] or exitt[a] > exitt[b]))) cout << -1 << '\n';
    //se b nao esta no ciclo, so preciso da diferenca de distancias
    else if (d[b] != 0) cout << d[a] - d[b] << '\n';
    //se b esta no ciclo, ando ate o ciclo depois ando no ciclo
    else cout << d[a] + (idx[b]-idx[a]+cs[b])%cs[b] << '\n';
  }
  
  return 0;
}

