#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int m;
int a, b;
int comp[100100];
int num;

int find(int u) {
  if (comp[u] == u) return u;
  return comp[u] = find(comp[u]);
}

void unite(int u, int v) {
  int x = find(u);
  int y = find(v);
  if (x == y) return;
  if (x > y) swap(x, y);
  comp[y] = x;
  num--;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  num = n;
  for (int i = 1; i <= n; i++) comp[i] = i;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    unite(a, b);
  }
  
  cout << num - 1 << '\n';
  for (int i = 2; i <= n; i++) {
    if (find(i) == i) cout << "1 " << i << '\n';
  }
  
  return 0;
}

