#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[100100];
int p[100100];
vector<int> ans;
queue<int> q;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    p[b]++;
  }

  for (int i = 1; i <= n; i++) {
    if (p[i] == 0) q.push(i);
  }

  while (not q.empty()) {
    a = q.front();
    q.pop();
    ans.pb(a);
    for (auto x: g[a]) {
      if (--p[x] == 0) q.push(x);
    }
  }
  
  if (ans.size() < n) {
    cout << "IMPOSSIBLE\n";
    return 0;
  }

  for (int i = 0; i+1 < ans.size(); i++) cout << ans[i] << ' ';
  cout << ans.back() << '\n';
  
  return 0;
}

