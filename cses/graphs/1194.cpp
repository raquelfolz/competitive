#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;


int n, m;
string tab[1010];
queue<pair<pii, int>> p, q;
int path[1010][1010]; //0 -i 1 +i 2 -j 3 +j
string mov = "UDLR";
vector<int> ans;
pii a, b;
int cnt;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;

  for (int i = 0; i < n; i++) {
    cin >> tab[i];
    for (int j = 0; j < m; j++) {
      if (tab[i][j] == 'A') {
        q.push({{i, j}, 0});
        tab[i][j] = '.';
      }
      else if (tab[i][j] == 'M') p.push({{i, j}, 0});
    }
  }

  memset(path, -1, sizeof(path));
  path[q.front().f.f][q.front().f.s] = -2;

  while (not q.empty()) {
    a = q.front().f;
    cnt = q.front().s;
    if (a.f == 0 or a.f == n-1 or a.s == 0 or a.s == m-1) goto path;
    
    while (not p.empty()) {
      if (p.front().s != cnt) break;
      b = p.front().f;
      if (b.f > 0 and tab[b.f-1][b.s] == '.') {
        tab[b.f-1][b.s] = 'M';
        p.push({{b.f-1, b.s}, cnt+1});
      }
      if (b.f < n-1 and tab[b.f+1][b.s] == '.') {
        tab[b.f+1][b.s] = 'M';
        p.push({{b.f+1, b.s}, cnt+1});
      }
      if (b.s > 0 and tab[b.f][b.s-1] == '.') {
        tab[b.f][b.s-1] = 'M';
        p.push({{b.f, b.s-1}, cnt+1});
      }
      if (b.s < m-1 and tab[b.f][b.s+1] == '.') {
        tab[b.f][b.s+1] = 'M';
        p.push({{b.f, b.s+1}, cnt+1});
      }
      p.pop();
    }

    if (tab[a.f-1][a.s] == '.' and path[a.f-1][a.s] == -1) {
      q.push({{a.f-1, a.s}, cnt+1});
      path[a.f-1][a.s] = 0;
    }
    if (tab[a.f+1][a.s] == '.' and path[a.f+1][a.s] == -1) {
      q.push({{a.f+1, a.s}, cnt+1});
      path[a.f+1][a.s] = 1;
    }
    if (tab[a.f][a.s-1] == '.' and path[a.f][a.s-1] == -1) {
      q.push({{a.f, a.s-1}, cnt+1});
      path[a.f][a.s-1] = 2;
    }
    if (tab[a.f][a.s+1] == '.' and path[a.f][a.s+1] == -1) {
      q.push({{a.f, a.s+1}, cnt+1});
      path[a.f][a.s+1] = 3;
    }

    q.pop();
  }
  
  cout << "NO\n";
  return 0;

path:

  cout << "YES\n";

  while (path[a.f][a.s] != -2) {
    ans.pb(path[a.f][a.s]);
    if (path[a.f][a.s] == 0) a.f++;
    else if (path[a.f][a.s] == 1) a.f--;
    else if (path[a.f][a.s] == 2) a.s++;
    else if (path[a.f][a.s] == 3) a.s--;
  }

  cout << ans.size() << '\n';
  for (int i = ans.size()-1; i >= 0; i--) cout << mov[ans[i]];
  cout << '\n';
  
  return 0;
}

