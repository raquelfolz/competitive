#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
set<int> g[100100];
vector<int> ans;
stack<int> s;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].insert(b);
    g[b].insert(a);
  }

  for (int i = 1; i <= n; i++) {
    if (g[i].size() % 2 == 1) {
      cout << "IMPOSSIBLE\n";
      return 0;
    }
  }

  if (g[1].size() == 0) {
    cout << "IMPOSSIBLE\n";
    return 0;
  }

  s.push(1);
  for (int i = 0; i < m; i++) {
    while (g[a=s.top()].size() == 0) {
      ans.pb(a);
      s.pop();
      if (s.size() == 0) { //isso eh antes de percorrer a ultima aresta
        cout << "IMPOSSIBLE\n";
        return 0;
      }
    }
    b = *(g[a].begin());
    s.push(b);
    g[a].erase(b);
    g[b].erase(a);
  }

  while (not s.empty()) {
    ans.pb(s.top());
    s.pop();
  }

  for (int i = 0; i < ans.size()-1; i++) cout << ans[i] << ' ';
  cout << ans.back() << '\n';
  
  return 0;
}

