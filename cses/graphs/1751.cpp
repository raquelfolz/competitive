#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, q;
int t[200100];
int a, b;

int vis[200100]; //1 visitado 2 processado
int bs[200100][31];
vector<int> ciclo;
int d[200100], cs[200100];

int search(int v, int k) {
  for (int i = 30; i >= 0; i--) {
    if ((1 << i) & k) v = bs[v][i];
  }
  return v;
}

void dfs(int v) {
  if (t[v] == v) {
    for (int i = 0; i <= 30; i++) bs[v][i] = t[v];
    vis[v] = 2;
    cs[v] = 1;
    return;
  }
  if (vis[t[v]] == 1) { //cheguei num ciclo!
    ciclo.pb(t[v]);
    ciclo.pb(v);
    return;
  }
  if (vis[t[v]] == 0) { //preciso descobrir o que tem pela frente
    vis[v] = 1;
    dfs(t[v]);
    if (ciclo.size() > 0) { //eu estou num ciclo :D
      ciclo.pb(v);
      if (ciclo.front() == v) {//eu sou o comeco
        reverse(ciclo.begin(), ciclo.end());
        for (int x = 0; x < ciclo.size()-1; x++) {
          for (int i = 0; i <= 30; i++) bs[ciclo[x]][i] = ciclo[((1 << i)+x) % (ciclo.size()-1)];
          vis[ciclo[x]] = 2;
          cs[ciclo[x]] = ciclo.size()-1;
        }
        ciclo.clear();
      }
      return;
    }
  }
  if (vis[t[v]] == 2) {
    for (int i = 0; i <= 30; i++) bs[v][i] = search(t[v], (1<<i)-1);
    vis[v] = 2;
    d[v] = d[t[v]]+1;
    cs[v] = cs[t[v]];
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> t[i];
  }
  for (int i = 1; i <= n; i++) {
    if (vis[i] == 0) dfs(i);
  }
  for (int i = 1; i < n; i++) {
    cout << d[i] + cs[i] << ' ';
  }
  cout << d[n] + cs[n] << '\n';
  
  return 0;
}

