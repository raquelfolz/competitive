#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[200100];
pii d[200100];
queue<int> q;
int aux;
vector<int> ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    g[b].pb(a);
  }

  for (int i = 2; i <= n; i++) d[i] = {-1, -1};
  d[1] = {0, -1};
  q.push(1);

  while (!q.empty()) {
    aux = q.front();
    if (aux == n) break;
    q.pop();

    for (auto v: g[aux]) {
      if (d[v].f == -1) {
        d[v] = {d[aux].f+1, aux};
        q.push(v);
      }
    }
  }

  if (d[n].f == -1) {
    cout << "IMPOSSIBLE\n";
    return 0;
  }

  aux = n;
  while (aux != 1) {
    ans.pb(d[aux].s);
    aux = d[aux].s;
  }

  cout << ans.size()+1 << '\n';
  for (int i = ans.size()-1; i >= 0; i--) cout << ans[i] << ' ';
  cout << n << '\n';
  
  return 0;
}

