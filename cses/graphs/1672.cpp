#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m, q;
int a, b;
ll c;
ll dist[510][510];
bool proc[510][510];

void dijkstra(int og) {

}

int main() {

  scanf("%d %d %d", &n, &m, &q);
  
  for (int i = 1; i <= n; i++) {
    for (int j = 0; j <= n; j++) dist[i][j] = LINF;
    dist[i][i] = 0;
  }
  
  for (int i = 0; i < m; i++) {
    scanf("%d %d %lld", &a, &b, &c);
    dist[a][b] = min(dist[a][b], c);
    dist[b][a] = dist[a][b];
  }
  
  for (int i = 1; i <= n; i++) {
    while (true) {
      a = 0;
      for (int k = 1; k <= n; k++) {
        if (not proc[i][k] and dist[i][k] < dist[i][a]) a = k;
      }
      if (a == 0) break;
      proc[i][a] = true;
      for (int k = 1; k <= n; k++) {
        dist[i][k] = min(dist[i][k], dist[i][a]+dist[a][k]);
      }
      /*for (auto [v, x]: g[a]) {*/
      /*  dist[i][v] = min(dist[i][v], dist[i][a] + x);*/
      /*}*/
    }
  }
  
  for (int i = 0; i < q; i++) {
    scanf("%d %d", &a, &b);
    if (dist[a][b] == LINF) printf("-1\n");
    else printf("%lld\n", dist[a][b]);
  }

  return 0;
}

