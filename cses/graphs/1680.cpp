#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[100100];
int p[100100];
pii r[100100];
vector<int> ans;
queue<int> q;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    if (b == 1) continue;
    g[a].pb(b);
    p[b]++;
  }

  q.push(1);
  r[1].f = 1;
  for (int i = 2; i <= n; i++) {
    if (p[i] == 0) q.push(i);
    r[i].f = -INF;
  }

  while (not q.empty()) {
    a = q.front();
    q.pop();
    for (auto x: g[a]) {
      r[x] = max(r[x], {r[a].f+1, a});
      if (--p[x] == 0) q.push(x);
    }
  }
  
  if (r[n].f < 1) {
    cout << "IMPOSSIBLE\n";
    return 0;
  }

  cout << r[n].f << '\n';
  a = n;
  while (a != 1) {
    ans.pb(a);
    a = r[a].s;
  }
  cout << 1;
  for (int i = ans.size()-1; i >= 0; i--) cout << ' ' << ans[i];
  cout << '\n';
  
  return 0;
}

