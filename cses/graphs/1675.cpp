#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
ll c;
pair<ll, pii> e[200100];
int comp[100100];
ll ans;
int cnum;

int find(int v) {
  if (comp[v] == v) return v;
  return comp[v] = find(comp[v]);
}

bool unite(int u, int v) {
  if (find(u) == find(v)) return false;
  if (comp[u] > comp[v]) swap(u, v);
  comp[comp[v]] = comp[u];
  cnum--;
  return true;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 1; i <= n; i++) comp[i] = i;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    e[i] = {c, {a, b}};
  }

  sort(e, e+m);
  cnum = n;
  for (int i = 0; i < m and cnum != 1; i++) {
    if (unite(e[i].s.f, e[i].s.s)) {
      ans += e[i].f;
    }
  }

  if (cnum != 1) cout << "IMPOSSIBLE\n";
  else cout << ans << '\n';
  
  return 0;
}

