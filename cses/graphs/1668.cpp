#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[200100];
int t[200100];

bool dfs(int v) {
  for (auto u: g[v]) {
    if (t[u] == 0) {
      t[u] = t[v]==1 ? 2 : 1;
      if (not dfs(u)) return false;
    } else if (t[u] == t[v]) return false;
  }
  return true;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    g[b].pb(a);
  }

  for (int i = 1; i <= n; i++) {
    if (t[i] == 0) {
      t[i] = 1;
      if (not dfs(i)) goto falha;
    }
  }
  for (int i = 1; i < n; i++) cout << t[i] << ' ';
  cout << t[n] << '\n';
  return 0;

falha:
  
  cout << "IMPOSSIBLE\n";
  
  return 0;
}

