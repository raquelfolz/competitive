#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int m;
int a, b;

vector<int> g[510];
ll w[510][510];
ll f;

bool vis[510];
bool dfs(int v) {
  if (v == n) return true;
  for (auto u: g[v]) {
    if (vis[u] or w[v][u] < 1) continue;
    vis[u] = true;
    w[v][u] -= 1;
    w[u][v] += 1;
    if (dfs(u)) return true;
    w[v][u] += 1;
    w[u][v] -= 1;
  }
  return false;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    g[b].pb(a);
    w[a][b] += 1;
    w[b][a] += 1;
  }
  
  vis[1] = true;
  while (dfs(1)) {
    f++;
    memset(vis, 0, sizeof(vis));
    vis[1] = true;
  }

  cout << f << '\n';
  for (int v = 1; v <= n; v++) {
    if (not vis[v]) continue;
    for (auto u: g[v]) {
      if (not vis[u]) cout << u << ' ' << v << '\n';
    }
  }
  
  return 0;
}

