#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int m;
int a, b;
ll c;

set<pii> edges;

vector<int> g[510];
ll w[510][510];
ll f;

bool vis[510];
bool dfs(int v) {
  if (v == n) return true;
  for (auto u: g[v]) {
    if (vis[u] or w[v][u] < 1) continue;
    vis[u] = true;
    w[v][u] -= 1;
    w[u][v] += 1;
    if (dfs(u)) return true;
    w[v][u] += 1;
    w[u][v] -= 1;
  }
  return false;
}

vector<int> path;
bool flow[510][510];
bool dfs2(int v) {
  /*cerr << "V " << v << '\n';*/
  path.pb(v);
  /*for (auto x: path) cerr << x << ' ';*/
  /*cerr << '\n';*/
  if (v == n) return true;
  for (auto u: g[v]) {
    /*cerr << v << ' ' << u << ' ' << w[u][v] << '\n';*/
    if (edges.count({v, u})==0 or vis[u] or w[u][v] < 1 or flow[v][u]) continue; //tem que ter a rota de volta
    vis[u] = true;
    flow[v][u] = true;
    if (dfs2(u)) return true;
    flow[v][u] = false;
  }
  path.pop_back();
  return false;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    g[a].pb(b);
    g[b].pb(a);
    edges.insert({a, b});
    w[a][b] += 1;
  }
  
  vis[1] = true;
  while (dfs(1)) {
    memset(vis, 0, sizeof(vis));
    vis[1] = true;
    f++;
  }

  cout << f << '\n';
  while (f--) {
    memset(vis, 0, sizeof(vis));
    vis[1] = true;
    path.clear();
    dfs2(1);
    cout << path.size() << '\n';
    for (int i = 0; i < path.size()-1; i++) cout << path[i] << ' ';
    cout << path.back() << '\n';
  }
  
  return 0;
}

