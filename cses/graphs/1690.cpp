#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int a, b;
vector<int> g[20];
ll dp[1050500][20];

ll calc(int i, int v) {
  if (dp[i][v] != -1) return dp[i][v];
  dp[i][v] = 0;
  for (auto u: g[v]) {
    if ((i & (1 << u))) {
      dp[i][v] += calc(i^(1<<v), u);
      dp[i][v] %= INF;
    }
  }
  return dp[i][v];
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    if (a == b) continue;
    g[b-1].pb(a-1);
  }
  
  memset(dp, -1, sizeof(dp));
  for (int i = 0; i < (1<<n); i++) dp[i][0] = 0;
  for (int i = 1; i < n; i++) dp[(1<<i)][i] = 0;
  dp[1][0] = 1;

  /*for (int i = 2; i < (1<<n); i++) {*/
  /*  for (int v = 0; v < n; v++) {*/
  /*    if (not (i & (1 << v))) continue;*/
  /*    dp[i][v] = 0;*/
  /*    for (auto u: g[v]) {*/
  /*      if ((i & (1 << u))) dp[i][v] += dp[i^(1<<v)][u];*/
  /*      dp[i][v] %= INF;*/
  /*    }*/
  /*  }*/
  /*}*/
  /**/
  /*cout << dp[(1<<n)-1][n-1] << '\n';*/
  cout << calc((1<<n)-1,n-1) << '\n';
  
  return 0;
}

