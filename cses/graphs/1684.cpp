#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
char c, d;
int x, y;

vector<int> g[200100];
vector<int> rg[200100];
int comp[200100];
int cnum;
char ans[100100];

bool vis[200100];
stack<int> s;

string str = "+-";

void dfs(int v) {
  for (auto u: rg[v]) {
    if (! vis[u]) {
      vis[u] = true;
      dfs(u);
    }
  }
  s.push(v);
}

void rdfs(int v) {
  /*cerr << v << ' ' << (v>>1) << '\n';*/
  if (ans[v>>1] == 0) ans[v>>1] = str[v&1];
  for (auto u: g[v]) {
    if (comp[u] == 0) {
      comp[u] = cnum;
      rdfs(u);
    }
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < n; i++) {
    cin >> c >> x >> d >> y;
    x <<= 1; y <<= 1;
    if (c == '-') x ^= 1;
    if (d == '-') y ^= 1;
    g[x^1].pb(y);
    rg[y].pb(x^1);
    g[y^1].pb(x);
    rg[x].pb(y^1);
  }

  for (int i = 1; i <= m; i++) {
    if (! vis[i<<1]) {
      vis[i<<1] = true;
      dfs(i<<1);
    }
    if (! vis[(i<<1)^1]) {
      vis[(i<<1)^1] = true;
      dfs((i<<1)^1);
    }
  }

  while (! s.empty()) {
    x = s.top();
    s.pop();
    if (comp[x] == 0) {
      comp[x] = ++cnum;
      rdfs(x);
    }
  }

  for (int i = 1; i <= m; i++) {
    if (comp[i<<1] == comp[(i<<1)^1]) {
      cout << "IMPOSSIBLE\n";
      return 0;
    }
  }
  for (int i = 1; i < m; i++) cout << ans[i] << ' ';
  cout << ans[m] << '\n';

  return 0;
}

