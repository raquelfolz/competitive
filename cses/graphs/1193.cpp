#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
string tab[1010];

string mv = "UDLR";
pii d[1010][1010]; //movimento foi: 0 -i 1 +i 2 -j 3 +j (tem que fazer o reverso)
queue<pii> q;
int x, y;
vector<int> ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < n; i++) {
    cin >> tab[i];
    for (int j = 0; j < m; j++) {
      if (tab[i][j] == 'A') {
        d[i][j] = {0, -1};
        q.push({i, j});
      } else d[i][j] = {-1, -1};
    }
  }

  while (! q.empty()) {
    x = q.front().f;
    y = q.front().s;
    if (tab[x][y] == 'B') break;
    q.pop();

    if (x > 0 and tab[x-1][y] != '#' and d[x-1][y].f == -1) {
      d[x-1][y] = {d[x][y].f+1, 0};
      q.push({x-1, y});
    }
    if (x < n-1 and tab[x+1][y] != '#' and d[x+1][y].f == -1) {
      d[x+1][y] = {d[x][y].f+1, 1};
      q.push({x+1, y});
    }
    if (y > 0 and tab[x][y-1] != '#' and d[x][y-1].f == -1) {
      d[x][y-1] = {d[x][y].f+1, 2};
      q.push({x, y-1});
    }
    if (y < m-1 and tab[x][y+1] != '#' and d[x][y+1].f == -1) {
      d[x][y+1] = {d[x][y].f+1, 3};
      q.push({x, y+1});
    }
  }

  if (tab[x][y] != 'B') {
    cout << "NO\n";
    return 0;
  }

  while (tab[x][y] != 'A') {
    ans.pb(d[x][y].s);
    if (d[x][y].s == 0) x++;
    else if (d[x][y].s == 1) x--;
    else if (d[x][y].s == 2) y++;
    else if (d[x][y].s == 3) y--;
    else break;
  }

  cout << "YES\n";
  cout << ans.size() << '\n';
  for (int i = ans.size()-1; i >= 0; i--) cout << mv[ans[i]];
  cout << '\n';
  
  return 0;
}

