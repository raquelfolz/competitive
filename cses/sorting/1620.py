n, t = map(int, input().split())
k = [int(x) for x in input().split()]

def teste(tempo):
    cnt = 0
    for x in k:
        cnt += tempo // x
    if cnt >= t:
        return True
    return False

i = 1
j = k[0]*t
ans = j
while i <= j:
    m = (i+j) // 2
    if teste(m):
        ans = m
        j = m-1
    else:
        i = m+1

print(ans)
