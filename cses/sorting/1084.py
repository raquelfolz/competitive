n, m, k = map(int, input().split())
a = sorted([int(x) for x in input().split()])
b = sorted([int(x) for x in input().split()])

i = 0
cnt = 0
for x in a:
    while i < m and b[i] < x-k:
        i += 1
    if i >= m:
        break
    if b[i] <= x+k:
        cnt += 1
        i += 1

print(cnt)
