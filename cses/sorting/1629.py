n = int(input())
l = []
for i in range(n):
    a, b = map(int, input().split())
    l.append((b, a))

t = 0
cnt = 0
l.sort()

for x in l:
    if x[1] >= t:
        t = x[0]
        cnt += 1

print(cnt)
