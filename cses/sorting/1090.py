n, x = map(int, input().split())
p = sorted([int(x) for x in input().split()])

cnt = 0
i = 0
j = n-1

while i < j:
    if p[i] + p[j] <= x:
        i += 1
    j -= 1
    cnt += 1

if i == j:
    cnt += 1

print(cnt)
