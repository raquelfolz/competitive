#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int x;
int n;
int p;
set<int> prox, ante;
multiset<int> dist;

vector<int> v;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> x >> n;

  prox.insert(0);
  prox.insert(x);
  ante.insert(0);
  ante.insert(-x);
  dist.insert(x);

  for (int i = 0; i < n; i++) {
    cin >> p;
    int a = - (* ante.upper_bound(-p));
    int b = *prox.upper_bound(p);
    auto it = dist.lower_bound(b-a);
    dist.erase(it);
    dist.insert(p-a);
    dist.insert(b-p);
    ante.insert(-p);
    prox.insert(p);
    v.pb(*(--dist.end()));
  }

  for (int i = 0; i < n-1; i++) cout << v[i] << ' ';
  cout << v.back() << '\n';

  
  return 0;
}

