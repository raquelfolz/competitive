n = int(input())

def foo(x):
    x -= 1
    d = 1
    t = n
    while x % 2 == 0:
        if t == 1:
            return (d, x)
        d += 1
        x = (x // 2) + t%2
        t = (t+1) // 2
        x %= t
    return (d, x)  

l = [(x+1) for x in range(n)]

print(*sorted(l, key=foo))
