n = int(input())
a = []
d = []
for i in range(n):
    x, y = map(int, input().split())
    a.append(x)
    d.append(y)

a.sort()
d.sort()

cnt = 0
ans = 0
i = 0
for x in a:
    while d[i] < x:
        cnt -= 1
        i += 1
    cnt += 1
    ans = max(ans, cnt)

print(ans)

