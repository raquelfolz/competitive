n, x = map(int, input().split())
a = [int(x) for x in input().split()]

l = 0
r = 0
s = 0
cnt = 0

while l <= r and r <= n:
    if s > x:
        s -= a[l]
        l += 1
    else:
        if s == x:
            cnt += 1
        if r < n:
            s += a[r]
        r += 1

print(cnt)
