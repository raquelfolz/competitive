#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define x first
#define y second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<pii, null_type, less<pii>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
pair<pii, int> a[200100];
indexed_set s;
indexed_set ys;
int contem[200100];
int contido[200100];

bool cmp(pair<pii, int> a, pair<pii, int> b) {
  if (a.x.x == b.x.x) return a.x.y > b.x.y;
  return a.x.x < b.x.x;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a[i].x.x >> a[i].x.y;
    ys.insert({a[i].x.y, i});
    a[i].y = i;
  }

  sort(a, a+n, cmp);

  for (int i = 0; i < n; i++) {
    //se estiver contido, vai ter alguem no set que seja maior ou igual
    auto it = s.lower_bound({a[i].x.y, 0});
    if (it != s.end()) contido[a[i].y] = s.size() - s.order_of_key(*it);
    s.insert({a[i].x.y, i});
    //se contiver, vai ter alguem no set de finais que eh menor ou igual
    //__depois__ de eu apagar meu final de la
    ys.erase({a[i].x.y, a[i].y});
    it = ys.upper_bound({a[i].x.y, INF});
    if (it != ys.begin()) contem[a[i].y] = (it == ys.end()) ? ys.size() : ys.order_of_key(*it);
  }

  for (int i = 0; i < n-1; i++) cout << contem[i] << ' ';
  cout << contem[n-1] << '\n';
  for (int i = 0; i < n-1; i++) cout << contido[i] << ' ';
  cout << contido[n-1] << '\n';

  return 0;
}

