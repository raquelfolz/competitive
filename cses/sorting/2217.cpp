#include <bits/stdc++.h>
using namespace std;
 
#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
 
#define f first
#define s second
#define pb push_back
 
typedef long long ll;
typedef pair<int, int> pii;
 
typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;
 
const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;
 
int n, m;
pii x[200100];
int y[200100];
int a, b;
int u, v;
 
int cnt=1;
 
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m;
  for (int i = 0; i < n; i++) {
    cin >> y[i];
    x[i] = {y[i], i};
  }

  sort(x, x+n);
  for (int i = 1; i < n; i++) {
    if (x[i].s < x[i-1].s) cnt++;
  }

  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    if (a == b) { cout << cnt << '\n'; continue; }
    //o cara na posicao a eh y[a-1]
    u = y[a-1]-1;
    v = y[b-1]-1;
    if (u > v) swap(u, v);
    //quero trocar o indice de u = x[u].s
    //quando eu faco essa troca
    //o que pode ter mudado era so:
    //x[u-1]x[u], x[u]x[u+1], x[v-1]x[v], x[v]x[v+1]
    if (u > 0 and x[u].s < x[u-1].s) cnt--;
    if (x[u+1].s < x[u].s) cnt--;
    if (v > u+1 and x[v].s < x[v-1].s) cnt--;
    if (v < n-1 and x[v+1].s < x[v].s) cnt--;
    swap(x[u].s, x[v].s);
    swap(y[a-1], y[b-1]);
    if (u > 0 and x[u].s < x[u-1].s) cnt++;
    if (x[u+1].s < x[u].s) cnt++;
    if (v > u+1 and x[v].s < x[v-1].s) cnt++;
    if (v < n-1 and x[v+1].s < x[v].s) cnt++;
    
    cout << cnt << '\n';
  }
  
  return 0;
}
