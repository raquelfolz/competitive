n = int(input())
l = [int(x) for x in input().split()]

s = 0
ans = max(l)

for x in l:
    if s + x < 0:
        s = 0
    else:
        s += x
        ans = max(ans, s)

print(ans)
