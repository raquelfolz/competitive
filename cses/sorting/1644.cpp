#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int a, b;
ll x[200100];
ll ps[200100];

ll ans;

int l, r;
int low[200100];

//preciso saber pra cada cara, quem eh o primeiro com ps menor ou igual a minha
//(pq eh onde vale a pena avancar o ponteiro da esquerda)
vector<pair<ll, int>> s;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  cin >> a >> b;
  for (int i = 1; i <= n; i++) {
    cin >> x[i];
    ps[i] = ps[i-1] + x[i];
  }

  for (int i = n; i > 0; i--) {
    while (s.size() > 0 and s.back().f > ps[i-1]) s.pop_back();
    if (s.size() > 0) low[i] = s.back().s;
    s.pb({ps[i-1], i});
  }
  
  /*for (int i = 1; i <= n; i++) cerr << ps[i-1] << ' ';*/
  /*cerr << '\n';*/
  /*for (int i = 1; i <= n; i++) cerr << low[i] << ' ';*/
  /*cerr << '\n';*/

  l = 1; r = a;
  ans = ps[a];
  for (; r <= n; r++) {
    if (r-l+1 > b) l++;
    while (low[l] != 0 and r-low[l]+1 >= a) l = low[l];
    /*cerr << l << ' ' << r << '\n';*/
    ans = max(ans, ps[r]-ps[l-1]);
  }

  cout << ans << '\n';
  
  return 0;
}

