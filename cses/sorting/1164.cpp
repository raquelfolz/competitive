#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
pair<pii, int> l[200100];
map<int, vector<int>> m;
int cnt;
int v[200100];
int aux;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> l[i].f.f >> l[i].f.s;
    l[i].s = i;
  }

  sort(l, l+n);

  for (int j = 0; j < n; j++) {
    int i = l[j].s;
    auto [a, b] = l[j].f;
    auto it = m.upper_bound(-a);
    if (it == m.end()) {
      v[i] = ++cnt;
    }
    else {
      v[i] = it->s.back();
      it->s.pop_back();
      if (it->s.size() == 0) m.erase(it);
    }
    if (m.count(-b) == 0) m[-b] = {v[i]};
    else m[-b].pb(v[i]);
  }

  cout << cnt << '\n';
  for (int i = 0; i < n-1; i++) cout << v[i] << ' ';
  cout << v[n-1] << '\n';

  return 0;
}

