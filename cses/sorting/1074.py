n = int(input())
p = [int(x) for x in input().split()]
p.sort()


s = p[n//2]
if n % 2 == 0:
    s = (s + p[(n-1)//2])/2

print(int(sum([abs(x-s) for x in p])))
