n = int(input())
t = [int(x) for x in input().split()]
s = sum(t)
m = max(t)

if s-m < m:
    print(2*m)
else:
    print(s)
