#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, m;
int h;
int t[200100];

multiset<int> s;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m;
  for (int i = 0; i < n; i++) {
    cin >> h;
    s.insert(h);
  }
  
  for (int i = 0; i < m; i++) {
    cin >> t[i];
    //cerr << '?' << s.size() << '\n';
    if (s.size() == 0) { cout << "-1\n"; continue; }
    auto it = s.lower_bound(t[i]);
    if (it == s.begin() and *it > t[i]) cout << "-1\n";
    else {
      //cerr << s.size() << ' ';
      //if (it == s.end()) cerr << "END ";
      if (it == s.end()) it--;
      //if (*it > t[i]) cerr << "BIG ";
      if (*it > t[i]) it--;
      //cerr <<'\n';
      cout << *it << '\n';
      s.erase(it);
    }
  }

  return 0;
}

