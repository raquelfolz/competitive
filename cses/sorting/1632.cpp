#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, k;
pii f[200100];
multiset<int> m;
int ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> k;
  for (int i = 0; i < n; i++) cin >> f[i].s >> f[i].f;
  for (int i = 0; i < k; i++) m.insert(0);
  
  sort(f, f+n);

  for (int i = 0; i < n; i++) {
    auto it = m.lower_bound(-f[i].s);
    if (it == m.end()) continue;
    ans++;
    m.erase(it);
    m.insert(-f[i].f);
  }

  cout << ans << '\n';
  
  return 0;
}

