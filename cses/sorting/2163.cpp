#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

unsigned int n, k;
indexed_set s;
unsigned int pos;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> k;
  for (int i = 1; i <= n; i++) s.insert(i);
  
  for (auto x: s) cerr << x << ' ';
  cerr << '\n';

  for (int i = 1; i < n; i++) {
    pos = (pos + k) % s.size();
    auto x = s.find_by_order(pos);
    cout << *x << ' ';
    s.erase(x);
  }

  cout << *s.begin() << '\n';

  return 0;
}

