n, x = map(int, input().split())
l = [int(x) for x in input().split()]
l = list(zip(l, range(1, n+1)))
l.sort()

i = 0
j = n-1

while i < j:
    if l[i][0] + l[j][0] == x:
        print(l[i][1], l[j][1])
        break
    elif l[i][0] + l[j][0] < x:
        i += 1
    else:
        j -= 1
else:
    print("IMPOSSIBLE")
