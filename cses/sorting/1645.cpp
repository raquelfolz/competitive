#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int x[200100];
vector<pii> v;
int ans[200100];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> x[i];
    while (v.size() > 0 and x[i] <= v.back().f) v.pop_back();
    if (v.size() != 0) ans[i] = v.back().s;
    v.pb({x[i], i+1});
  }

  for (int i = 0; i < n-1; i++) cout << ans[i] << ' ';
  cout << ans[n-1] << '\n';
  
  return 0;
}

