#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
int k[200100];
set<int> s;

int ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  int j = 0;
  for (int i =0; i < n; i++) {
    cin >> k[i];
    /*cerr << i << ' ' << j << ' ' << s.count(k[i]) << '\n';*/
    if (s.count(k[i])) {
      while (k[j] != k[i]) {
        s.erase(k[j++]);
      }
      j++;
    }
    else s.insert(k[i]);
    ans = max(ans, i-j+1);
  }
  
  cout << ans << '\n';

  return 0;
}

