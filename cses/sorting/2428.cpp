#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, k;
int x[200100];
map<int, int> m;
int l, r, rr;
ll ans, aux;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> k;
  for (int i = 0; i < n; i++) cin >> x[i];

  rr = -1;
  while (r <= n) {
    if (m.size() <= k) {
      if (r < n) {
        m[x[r]]++;
        r++;
      }
      if (m.size() > k or r == n) {
        /*cerr << r <<  ' ' << l << '\n';*/
        aux = r - l;
        if (m.size() > k) aux--;
        ans += aux*(aux+1)/2;
        if (rr > l) {
          aux = rr - l;
          ans -= aux*(aux+1)/2;
        }
        rr = r-1;
        if (m.size() <= k) break;
      }
    } else {
      m[x[l]]--;
      if (m[x[l]] == 0) m.erase(x[l]);
      l++;
    }
  }

  cout << ans << '\n';
  
  return 0;
}

