#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
ll x[200100];
ll soma;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  for (int i = 0; i < n; i++) cin >> x[i];

  sort(x, x+n);
  x[n] = LINF;

  for (int i = 0; i <= n; i++) {
    if (x[i] > soma+1) {
      cout << soma+1 << '\n';
      break;
    }
    soma += x[i];
  }

  return 0;
}

