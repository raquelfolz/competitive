#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, k;
int x[200100];
multiset<int> l, r;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> k;
  for (int i = 0; i < n; i++) cin >> x[i];

  if (k == 1) {
    for (int i = 0; i < n-1; i++) cout << x[i] << ' ';
    cout << x[n-1] << '\n';
    return 0;
  }

  for (int i = 0; i < (k+1)/2; i++) l.insert(x[i]);
  for (int i = (k+1)/2; i < k; i++) {
    l.insert(x[i]);
    auto it = l.end();
    it--;
    r.insert(*it);
    l.erase(it);
  }
  cout << *l.rbegin();
  for (int i = k; i < n; i++) {
    if (l.find(x[i-k]) != l.end()) {
      l.erase(l.find(x[i-k]));
      l.insert(*r.begin());
      r.erase(r.begin());
    } else r.erase(r.find(x[i-k]));

    l.insert(x[i]);
    auto it = l.end();
    it--;
    r.insert(*it);
    l.erase(it);
    cout << ' ' << *l.rbegin();
  }
  cout << '\n';
  
  return 0;
}

