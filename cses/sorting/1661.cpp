#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
ll x;
ll a[200100];
map<ll, ll> s;
ll soma, cnt;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  s[0] = 1;

  cin >> n >> x;
  for (int i = 1; i <= n; i++) {
    cin >> a[i];
    soma += a[i];
    cnt += s[soma - x];
    s[soma]++;
  }

  cout << cnt << '\n';
  
  return 0;
}

