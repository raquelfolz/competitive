#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n;
ll x;
pair<ll, int> a[1010];
int l, r;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> x;
  for (int i = 0; i < n; i++) {
    cin >> a[i].f;
    a[i].s = i+1;
  }
  sort(a, a+n);

  for (int i = 0; i < n-3; i++) {
    for (int j = i+1; j < n-2; j++) {
      if (a[i].f + a[j].f + a[n-1].f + a[n-2].f < x) continue;
      if (a[i].f + a[j].f + a[j+1].f + a[j+2].f > x) continue;
      l = j+1; r = n-1;
      while (l < r) {
        if (a[i].f + a[j].f + a[l].f + a[r].f == x) {
          cout << a[i].s << ' ' << a[j].s << ' ' << a[l].s << ' ' << a[r].s << '\n';
          return 0;
        } else if (a[i].f + a[j].f + a[l].f + a[r].f < x) l++;
        else r--;
      }
    }
  }

  cout << "IMPOSSIBLE\n";
  
  return 0;
}

