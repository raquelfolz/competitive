#include <bits/stdc++.h>
using namespace std;

#include<ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int, int> pii;

typedef tree<int, null_type, less<int>, rb_tree_tag,
            tree_order_statistics_node_update> indexed_set;

const int INF = 1e9+7;
const ll LINF = ((ll)1e18) + 9;

int n, k;
ll x[200100];

bool test(ll s) {
  ll acc=0;
  int cnt=1;

  for (int i = 0; i < n; i++) {
    acc += x[i];
    if (acc > s) {
      cnt++;
      acc = x[i];
    }
  }

  return cnt <= k;
}

ll l = 0, r = 0;
ll meio, ans;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> k;
  for (int i = 0; i < n; i++) {
    cin >> x[i];
    l = max(l, x[i]);
    r += x[i];
  }

  while (l <= r) {
    meio = (l + r) / 2;
    if (test(meio)) {
      ans = meio;
      r = meio-1;
    } else l = meio+1;
  }

  cout << ans << '\n';
  
  return 0;
}

