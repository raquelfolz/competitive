#include <bits/stdc++.h>

using namespace std;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  long long n;
  long long a, x;
  long long c;
  long long m, p, cnt=0;

  cin >> n;
  cin >> a >> x;

  //x = abs(a-m);
  p = a+x;
  m = a-x;

  for (int i=0; i<n; i++) {
    cin >> c;
    if (c <= p and c >= m) cnt++;
  }

  cout << cnt << '\n';

  return 0;
}
