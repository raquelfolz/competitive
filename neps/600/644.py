teste = 0

while True:
    pos = [int(x) for x in input().split()]
    if pos == [0,0,0,0]:
        break

    n = int(input())

    cnt = 0
    for i in range(n):
        x, y = map(int, input().split())
        if x >= pos[0] and x <= pos[2] and \
           y <= pos[1] and y >= pos[3]:
            cnt += 1

    teste += 1
    print("Teste", teste)
    print(cnt)
    print()
