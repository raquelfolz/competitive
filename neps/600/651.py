class retangulo:
    def __init__(self, x, y, u, v):
        self.x = x
        self.y = y
        self.u = u
        self.v = v

    def intersect(self, other):
        self.x = max(self.x, other.x)
        self.y = min(self.y, other.y)
        self.u = min(self.u, other.u)
        self.v = max(self.v, other.v)


teste = 0

while True:

    n = int(input())
    if n == 0:
        break

    teste += 1

    x,y,u,v = map(int, input().split())
    ret = retangulo(x, y, u, v)

    for i in range(1, n):
        x,y,u,v = map(int, input().split())
        r = retangulo(x, y, u, v)
        ret.intersect(r)

    print("Teste", teste)
    if ret.x >= ret.u or ret.y <= ret.v:
        print("nenhum")
    else:
        print(ret.x, ret.y, ret.u, ret.v)
    print()
