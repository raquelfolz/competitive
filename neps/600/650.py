teste = 0

while True:

    n = int(input())
    if n == 0:
        break

    x = [0]
    y = [0]

    for i in range(n):
        a, b = map(int, input().split())
        x.append(a)
        y.append(b)

    pc = [(-1,0)]
    for i in range(1, n+1):
        if pc[-1][0] >= 0:
            k = x[i] - y[i] + pc[-1][0]
            pc.append((k, pc[-1][1]))
        else:
            k = x[i] - y[i]
            pc.append((k,i))

    m = 0

    for i in range(1, n+1):
        if (pc[i][0], i-pc[i][1]) > (pc[m][0], m-pc[m][1]):
            m = i

    teste += 1
    print("Teste", teste)
    if pc[m][0] <= 0:
        print("nenhum")
    else:
        print(pc[m][1], m)
    #print(pc)
    print()
