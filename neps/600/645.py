def find(link, x):
    if link[x] == x:
        return
    find(link, link[x])
    link[x] = link[link[x]]

def union(link, a, b):
    find(link, a)
    find(link, b)
    link[link[a]] = link[link[b]]

teste = 0

while True:
    n = int(input())
    if n == 0:
        break

    link = [0, 1, 2, 3, 4, 5, 6]
    g = [0 for i in range(7)]

    for i in range(n):
        x, y = map(int, input().split())
        g[x] += 1
        g[y] += 1
        union(link, x, y)
        #print(link)

    cnt = 0
    for i in range(7):
        if g[i] % 2 == 1:
            cnt += 1

    #for i in range(7):
    #    find(link, i)
    #print(link)

    ok = True
    comp = -1
    for i in range(7):
        #print(i, g[i], comp, link[i])
        if g[i] == 0:
            continue
        find(link, i)
        if comp == -1:
            comp = link[i]
        else:
            if link[i] != comp:
                ok = False
                break

    teste += 1
    print("Teste", teste)

    if ok and (cnt == 0 or cnt == 2):
        print("sim")
    else:
        print("nao")

    print()
