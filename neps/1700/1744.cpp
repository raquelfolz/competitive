#include <bits/stdc++.h>

using namespace std;


int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n;
  string s;
  vector<int> v[100100];
  vector<long long> u;
  long long cnt=0;

  cin >> n;

  for (int i = 0; i < n; i++) {
    cin >> s;

    sort(s.begin(), s.end());
    v[i].push_back(1);
    for (int j = 1; j < s.size(); j++) {
      if (s[j] != s[j-1]) v[i].push_back(1);
      else v[i].back()++;
    }
    sort(v[i].begin(), v[i].end());
  }

  sort(v, v+n);

  u.push_back(1);
  for (int i = 1; i < n; i++) {
    if (v[i] != v[i-1]) u.push_back(1);
    else u.back()++;
  }

  for (int i = 0; i < u.size(); i++) {
    cnt += (u[i]*(u[i]-1))/2;
  }

  cout << cnt << '\n';

  return 0;
}
