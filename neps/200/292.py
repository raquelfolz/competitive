while True:

    n, d = map(int, input().split())
    if n == 0:
        break

    s = input()

    l = []

    for c in s:
        while len(l) > 0 and d > 0 and int(c) > int(l[-1]):
            l.pop()
            d -= 1

        l.append(c)

    while d != 0:
        l.pop()
        d -= 1

    print("".join(l))
