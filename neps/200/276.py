l = list("ABCDEFGHIJKLMNOP")
p = []

for i in range(8):
    m, n = map(int, input().split())
    if m > n:
        p.append(l[2*i])
    else:
        p.append(l[2*i+1])

l = p
p = []

for i in range(4):
    m, n = map(int, input().split())
    if m > n:
        p.append(l[2*i])
    else:
        p.append(l[2*i+1])

l = p
p = []

for i in range(2):
    m, n = map(int, input().split())
    if m > n:
        p.append(l[2*i])
    else:
        p.append(l[2*i+1])

l = p

m, n = map(int, input().split())
if m > n:
    print(l[0])
else:
    print(l[1])
