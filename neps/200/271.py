t = int(input())

for i in range(t):

    s = input()

    l = []
    ok = True

    for c in s:
        if c in "([{":
            l.append(c)
        elif len(l) == 0:
            ok = False
            break
        else:
            if (c == ")" and l[-1] == "(" or
              c == "]" and l[-1] == "["  or
              c == "}" and l[-1] == "{"):
                l.pop()
                continue
            ok = False
            break

    if len(l) > 0:
        ok = False

    if not ok:
        print("N")
    else:
        print("S")
