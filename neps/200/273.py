import heapq as hq

c, n = map(int, input().split())

cx = [0]*c
cnt = 0

for i in range(n):
    t, d = map(int, input().split())
    if cx[0] > t + 20:
        cnt += 1
    hq.heappush(cx, max(t, hq.heappop(cx))+d)

print(cnt)
