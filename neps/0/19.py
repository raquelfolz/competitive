n = int(input())
botas = [input().split() for x in range(n)]

e = [0]*70
d = [0]*70

for b in botas:
    if b[1] == 'D':
        d[int(b[0])] += 1
    else:
        e[int(b[0])] += 1

cnt = 0
for i in range(70):
    cnt += min(e[i], d[i])

print(cnt)
