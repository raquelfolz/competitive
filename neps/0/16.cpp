#include <bits/stdc++.h>

using namespace std;


int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  int n, m;
  int b[100100];
  int ps[10][100100];
  int ans[10]; //resposta
  int a, d; //antes e depois
  int x, y;

  cin >> n >> m;
  for (int i=1; i <= n; i++) cin >> b[i];

  memset(ps, 0, sizeof(ps));

  for (int i=1; i <= n; i++) {
    for (int j = 0; j < 10; j++) ps[j][i] = ps[j][i-1];
    ps[b[i]][i]++;
  }


  memset(ans, 0, sizeof(ans));

  cin >> a >> d;
  for (int j = 0; j < 10; j++) ans[j] = ps[j][d];

  for (int i = 2; i < m; i++) {
    a = d;
    cin >> d;

    x = a; y = d;
    if (x > y) swap(x, y);

    for (int j = 0; j < 10; j++) {
      ans[j] += ps[j][y] - ps[j][x-1];
    }
    ans[b[a]]--;
  }

  cout << ans[0];
  for (int j = 1; j < 10; j++) cout << ' ' << ans[j];
  cout << '\n';

  return 0;

}
