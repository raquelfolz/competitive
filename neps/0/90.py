class VectorR2(object):

    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def __repr__(self):
        return "V2({}, {})".format(self.x, self.y)

    def __add__(self, other):
        if type(other) is VectorR3:
            raise TypeError("Cannot add 'VectorR2' and 'VectorR3' objects")
        return VectorR2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        if type(other) is VectorR3:
            raise TypeError("Cannot subtract 'VectorR2' and 'VectorR3' objects")
        return VectorR2(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        if type(other) is VectorR2:
            return self.x * other.x + self.y * other.y
        if type(other) is VectorR3:
            raise TypeError("Cannot multiply 'VectorR2' and 'VectorR3' objects")
        return VectorR2(other * self.x, other * self.y)

    def __rmul__(self, other):
        if type(other) is VectorR2:
            return self.x * other.x + self.y * other.y
        if type(other) is VectorR3:
            raise TypeError("Cannot multiply 'VectorR2' and 'VectorR3' objects")
        return VectorR2(other * self.x, other * self.y)

    def __div__(self, other):
        return VectorR2(self.x / other, self.y / other)

    def __truediv__(self, other):
        return VectorR2(self.x / other, self.y / other)


    def __iadd__(self, other):
        if type(other) is VectorR3:
            raise TypeError("Cannot add 'VectorR2' and 'VectorR3' objects")
        self.x += other.x
        self.y += other.y

    def __isub__(self, other):
        if type(other) is VectorR3:
            raise TypeError("Cannot subtract 'VectorR2' and 'VectorR3' objects")
        self.x -= other.x
        self.y -= other.y

    def __idiv__(self, other):
        self.x /= other
        self.y /= other

    def __itruediv__(self, other):
        self.x /= other
        self.y /= other


    def norm(self):
        return sqrt(self.x ** 2 + self.y ** 2)

    def distance(self, other):
        return (self - other).norm()

    def angle(self, other):
        return acos((self * other) / (self.norm() * other.norm()))

    def oriented_angle(self):
        if self.x >= 0:
            return self.angle(VectorR2(1,0))
        else:
            return - self.angle(VectorR2(1,0))

    def pseudo_angle(self, other):
        return 1 - (self * other) / (self.norm() * other.norm())

    def square_angle(self, other=None):
        if self.x == 0 and self.y == 0:
            raise ValueError("Cannot compute the angle of the zero vector")
        if other is not None:
            r = self.square_angle() - other.square_angle()
            if r < 0:
                r += 8
            return r

        if self.x == 0:
            if self.y > 0:
                return 2.
            else:
                return 6.
        elif self.y == 0:
            if self.x > 0:
                return 0.
            else:
                return 4.

        if self.y > 0:
            if self.x > 0:
                if self.x > self.y:
                    return self.y/self.x
                return 2 - self.x/self.y

            if -self.x < self.y:
                return 2 - self.x/self.y
            return 4 + self.y/self.x

        if self.x < 0:
            if self.x < self.y:
                return 4 + self.y/self.x
            return 6 - self.x/self.y

        if self.x < -self.y:
            return 6 - self.x/self.y
        return 8 + self.y/self.x

    def vector_product(self, other):
        return self.x * other.y - self.y * other.x

def area(*points):
    #assumes the points form a simple polygon
    #assumes all points are VectorR2 or all points are VectorR3
    if type(points[0]) is tuple:
        p = [VectorR2(*x) for x in points]
    else:
        p = [x for x in points]
    a = 0
    for i in range(len(p)-1):
        a += p[i].vector_product(p[i+1])
    a += p[-1].vector_product(p[0])
    return a/2

def barycentric_coordinates(point, a, b, c):
    #assumes points in R2
    k = area(a, b, c)
    l1 = area(point, b, c) / k
    l2 = area(a, point, c) / k
    l3 = area(a, b, point) / k
    return l1, l2, l3

def is_in_triangle(point, a, b, c):
    #returns 1 if point is in the triangle
    #returns -1 if point is not in the triangle
    #returns 0 if point is on a side or vertex
    l1, l2, l3 = barycentric_coordinates(point, a, b, c)
    if l1 > 0 and l2 > 0 and l3 > 0:
        return 1
    if l1 == 0 and l2 >= 0 and l3 >= 0:
        return 0
    if l1 >= 0 and l2 == 0 and l3 >= 0:
        return 0
    if l1 >= 0 and l2 >= 0 and l3 == 0:
        return 0
    else:
        return -1


n, xa, xb = map(int, input().split())

a = VectorR2(xa, 0)
b = VectorR2(xb, 0)

p = []
for i in range(n):
    x, y = map(int, input().split())
    p.append(VectorR2(x, y))

p.sort(key=lambda x: x.y)

cnt = [0] * 110

for i in range(n):
    melhor = 0
    for j in range(i):
        #print(i, j)
        #print(p[i], p[j], is_in_triangle(p[j], p[i], a, b))
        if is_in_triangle(p[j], p[i], a, b) == 1 and cnt[j] >= melhor:
            melhor = cnt[j] + 1
    cnt[i] = melhor

#print(p[:10])
#print(cnt[:10])

print(max(cnt)+1)
