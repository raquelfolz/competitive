#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

#define ptot 78498

using namespace std;

long long n, aux;
int primos[80100] = {2, 3, 5, 7};

long long pcnt, ans;
bool ok;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  //calcula primos
  aux = 4;
  for (int i = 11; i < 1000000; i+=2) {
    ok = true;
    for (int j = 0; j < aux; j++) {
      if (i / primos[j] < primos[j]) break;
      if (i % primos[j] == 0) {
        ok = false;
        break;
      }
    }
    if (ok) primos[aux++] = i;
  }

  cin >> n;

  aux = n;
  for (int j = 0; j < ptot; j++) {
    if (aux < primos[j]) break;

    if (aux % primos[j] == 0) pcnt++;
    while (aux % primos[j] == 0) {
      aux /= primos[j];
    }
  }
  if (aux != 1) pcnt++;

  //cerr << pcnt << '\n';
  //if (pcnt > 1)
  cout << (1 << pcnt) - pcnt - 1  << '\n';

  return 0;
}
