#include <bits/stdc++.h>

#define f first
#define s second
#define pb push_back
#define INF 1000000007

using namespace std;

long long t, m, n;
long long memo[100100][55];
long long soma;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> t >> m >> n;

  for (int i = m; i <= n; i++) memo[i][1] = 1;

  for (int j = 2; j <= t; j++) {
    memo[m][j] = memo[m+1][j-1];
    for (int i = m+1; i < n; i++) {
      memo[i][j] = memo[i+1][j-1] + memo[i-1][j-1];
      memo[i][j] %= INF;
    }
    memo[n][j] = memo[n-1][j-1];
  }

  for (int i = m; i <= n; i++) {
    soma += memo[i][t];
    soma %= INF;
  }

  cout << soma << "\n";

  return 0;
}
