#include <bits/stdc++.h>

using namespace std;

int p, r;

int main() {

  ios::sync_with_stdio(0);
  cin.tie(0);

  cin >> p >> r;

  // se P estiver no 0, a bolinha vai automaticamente para C
  // caso contrario, a bolinha vai passar por R
  //   nesse caso se R estiver em 0, a bolinha vai para B
  //   caso contrario, a bolinha vai para A

  if (p == 0) {
    cout << "C\n";
  }
  else {
    if (r == 0) cout << "B\n";
    else cout << "A\n";
  }
  return 0;

}
