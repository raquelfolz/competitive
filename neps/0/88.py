l = [int(x) for x in input().split()]
i = None
if sum(l) == 1:
    i = l.index(1)
elif sum(l) == 2:
    i = l.index(0)

if i is None:
    print("*")
else:
    print("ABC"[i])
